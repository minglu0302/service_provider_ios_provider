//
//  ProfileViewController.m
//  iServePro
//
//  Created by -Tony Lu on 28/03/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ProfileViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "CustomNavigationBar.h"
#import "User.h"
#import "LocationTracker.h"
#import "profileTableViewCell.h"
#import "iServeSplashController.h"
#import "ProfileCollectionViewCell.h"
#import "profileTableViewCell.h"
#import "collectionTableViewCell.h"
#import "ReviewTableViewCell.h"
#import "ChatSocketIOClient.h"
#import "AmazonTransfer.h"
#import "CategoryDetailsUpdate.h"
#import "LiveChatViewController.h"

@interface ProfileViewController () <CustomNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate, UserDelegate,UIImagePickerControllerDelegate,UIGestureRecognizerDelegate>
{
    CGRect screenSize;
    UIImagePickerController *imagePicker;
    UIImage *pickedImage;
    UIImage *pickedImage1;
    
    NSMutableArray *jobImagesArray;
    collectionTableViewCell *jobPhotosCell;
    ChatSocketIOClient *socket;
      UIActionSheet *actionSheet;
    CategoryDetailsUpdate *updateCategoryDetails;
}
@property(nonatomic,strong)profile *user;

@property(nonatomic,weak) IBOutlet UITableView *tableView;
@property(nonatomic,strong) UIImageView *profileImage;
@property(nonatomic,assign)BOOL isEditingModeOn;
@property(nonatomic,strong) NSArray *categories;
@property(nonatomic,strong) NSArray *reviewsArray;
@property(nonatomic,assign)BOOL fromProfile;
@property(nonatomic,assign)BOOL fromJobImages;
@property(nonatomic, strong) NSString *imageURl;
@property(nonatomic,assign)BOOL isKeyboardIsShown;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end




@implementation ProfileViewController



#pragma mark - ViewController LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
     _proName.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.ratingView.highlightColor = UIColorFromRGB(0xffd200);
    self.ratingView.baseColor = UIColorFromRGB(0xcccccc);
    screenSize = [[UIScreen mainScreen]bounds];
    self.ratingView.markFont = [UIFont systemFontOfSize:18];
    _logoutButton.layer.borderWidth = 2;
    _logoutButton.layer.borderColor= [UIColorFromRGB(0x2598ED) CGColor];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    _versionLabel.text = [NSString stringWithFormat:@"Version :%@",version];
    _user = [[profile alloc]init];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x2598ED)};
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
    _liveChatButton.layer.borderWidth = 2;
    _liveChatButton.layer.borderColor = [UIColorFromRGB(0x2598ED) CGColor];
}


- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    
}

//**** If session expires ********//
-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userSessionTokenExpire) name:@"HandleAcceptAndRejectFromAdmin" object:nil];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];

    socket =[ChatSocketIOClient sharedInstance];
    jobImagesArray = [[NSMutableArray alloc]init];
    [self getProfileData];
  
 self.tableView.rowHeight = UITableViewAutomaticDimension;
}

///Gesture Deledate method *****//
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (self.tableView && !_isEditingModeOn) {
        return NO;
    }else{
        return YES;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountDeactivated) name:@"accountDeactivated" object:nil];
    
}

-(void)dismissKeyboard{
    [self moveViewDown];
    [self.view endEditing:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//********* Provider Profile Data*********//
-(void)getProfileData
{
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:NSLocalizedString(@"Loading...",@"Loading...")];

    NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:MethodGetMasterProfile
                              paramas:queryParams
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 
                                 [self getProfileResponse:response];
                             }
                         }];
    TELogInfo(@"param%@",queryParams);
}

///*******handling the profile data**********//
-(void)getProfileResponse:(NSDictionary *)response      // errNum=21 for getting profile data.
{
    [_profileArray removeAllObjects];
    _profileArray=[[NSMutableArray alloc]init];
    _jobImagesMutableArray = [[NSMutableArray alloc]init];
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    
    if ([response[@"errFlag"] intValue] ==0) {
        _categories =response[@"catArray"];
        _reviewsArray =response[@"reviewsArr"];
        _noOfReviews.text=[NSString stringWithFormat:@"%@ Reviews",response[@"totRats"]];
        [_profileArray addObject:response[@"about"]];
        [_profileArray addObject:response[@"expertise"]];
        [_profileArray addObject:response[@"languages"]];
        [_profileArray addObject:response[@"email"]];
        [_jobImagesMutableArray addObjectsFromArray:response[@"job_images"]];
        [_profileArray addObject:response[@"mobile"]];
        [[NSUserDefaults standardUserDefaults] setObject:response[@"mobile"] forKey:@"mobileNO"];
        
        _user.fName =[NSString stringWithFormat:@"%@ %@",response[@"fName"],response[@"lName"]];
        [[NSUserDefaults standardUserDefaults]setObject:_user.fName forKey:@"providerName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //  [self.tableView reloadData];
        
        _user.aboutMe = response[@"about"];
        _user.language = response[@"languages"];
        _user.expertises = response[@"expertise"];
        
        [_ratingView setUserInteractionEnabled:NO];
        _ratingView.value=[response[@"avgRate"] floatValue];
        _proName.tag = 199;
        _proName.text=[NSString stringWithFormat:@"%@ %@",response[@"fName"],response[@"lName"]];
        NSString *strImageUrl = response[@"pPic"];
        _imageURl = strImageUrl;
        [_activityIndicator startAnimating];
        [_profileImgView sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                           placeholderImage:[UIImage imageNamed:@"my_profile_profile_default_image.png"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      [_activityIndicator stopAnimating];
                                  }];
        [self.tableView reloadData];
        self.heightOftable.constant = self.tableView.contentSize.height;
        [self.view layoutIfNeeded];
        [progressIndicator hideProgressIndicator];
        
    }
    else if ([response[@"errFlag"] intValue] == 1)
    {
        [progressIndicator hideProgressIndicator];
        if ([response[@"errNum"] integerValue] == 7) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else if ([response[@"errNum"] integerValue] == 96 || [response[@"errNum"] integerValue] == 94|| [response[@"errNum"] integerValue] == 83) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
    else
    {
        [progressIndicator hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*-------------------------*/
#pragma mark - UserDelegate
/*-------------------------*/
-(void)userDidLogoutSucessfully:(BOOL)sucess {
    if (sucess){
        // Logged it out Successfully
        NSLog(@"Logged it out Successfully");
    }
    else{
        // Session is Expired
        NSLog(@"Session is Expired");
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)userDidFailedToLogout:(NSError *)error
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}

-(void)accountDeactivated {
    [socket disconnectSocket];
    User *user = [User sharedInstance];
    user.delegate = self;
    [user logout];
}

- (IBAction)addPhotosButton:(id)sender {
    _fromProfile = NO;
    _fromJobImages = YES;
    
    [self.view endEditing:YES];
    
    
    actionSheet.tag = 1;
    
    actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Add Job Photos",@"Add Job Photos")
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")
                                destructiveButtonTitle:nil
                                     otherButtonTitles:NSLocalizedString(@"Take Photo",@"Take Photo"),NSLocalizedString(@"Choose From Library",@"Choose From Library"),nil];
    
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
    
}

- (IBAction)logoutAction:(id)sender {
    [self logoutMethod];
}

-(void)logoutMethod
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirm", @"Confirm")
                                                        message:NSLocalizedString(@"Are you sure you want to logout?", @"Are you sure you want to logout?")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"No", @"No")
                                              otherButtonTitles:NSLocalizedString(@"Yes", @"Yes"), nil];
    [alertView show];
}

/*-------------------------------*/
#pragma mark - UIAlertViewDelegate
/*-------------------------------*/
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view
             withMessage:NSLocalizedString(@"Logging out..", @"Logging out..")];
        
        User *user = [[User alloc] init];
        user.delegate = self;
        [user logout];
    }
}

#pragma mark UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    
    switch (indexPath.section)
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        {
            cellIdentifier = @"categories";
            profileTableViewCell    *detailsCell = (profileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if(detailsCell == nil)
            {
                detailsCell =[[profileTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            if(indexPath.section == 0)
            {
                 detailsCell.ContentTextView.scrollEnabled = NO;
                detailsCell.ContentTextView.text = _categories[indexPath.row][@"cat_name"];
            }
            else if(indexPath.section == 1)
            {
                detailsCell.ContentTextView.scrollEnabled = YES;
                if (_isEditingModeOn && ![_user.aboutMe isEqualToString:@""]) {
                    detailsCell.ContentTextView.text = _user.aboutMe;
                    
                }else{
                    detailsCell.ContentTextView.text = _profileArray[indexPath.section-1];
                }
                
            }
            else if(indexPath.section == 2)
            {
                 detailsCell.ContentTextView.scrollEnabled = NO;
                if (_isEditingModeOn && ![_user.aboutMe isEqualToString:@""]) {
                    
                    detailsCell.ContentTextView.text = _user.expertises;
                    
                }else{
                    detailsCell.ContentTextView.text = _profileArray[indexPath.section-1];
                }

            }
            else if(indexPath.section == 3)
            {
                 detailsCell.ContentTextView.scrollEnabled = NO;
                if (_isEditingModeOn && ![_user.aboutMe isEqualToString:@""]) {
                    
                    detailsCell.ContentTextView.text = _user.language;
                    
                }else{
                    detailsCell.ContentTextView.text = _profileArray[indexPath.section-1];
                }
            }
            else
            {
                 detailsCell.ContentTextView.scrollEnabled = NO;
                detailsCell.ContentTextView.text = _profileArray[indexPath.section-1];
            }
            float height = [self measureHeightLabel:detailsCell.ContentTextView];
            return 20+height;
        }
            break;
            
        case 5:
        {
            return 70;
        }
        case 6:
        {
            cellIdentifier = @"reviews";
            ReviewTableViewCell *reviewcell = (ReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(reviewcell == nil)
            {
                reviewcell =[[ReviewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            reviewcell.review.text=_reviewsArray[indexPath.row][@"review"];
            float height = [self measureHeiLabel:reviewcell.review];
            return 46+height;
        }
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

#pragma mark UITableViewDelegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_profileArray.count) {
         return 6;
    }else{
        return 0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return _categories.count;
    }
    else
    {
        return 1;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    
    switch (indexPath.section)
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        {
            cellIdentifier = @"categories";
            profileTableViewCell *detailsCell= (profileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if(detailsCell == nil)
            {
                detailsCell =[[profileTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            if (_isEditingModeOn) {
                [detailsCell.ContentTextView setScrollEnabled:YES];
                for(UIView * cellSubviews in detailsCell.subviews)
                {
                    cellSubviews.userInteractionEnabled = YES;
                }
            }else
            {
                 [detailsCell.ContentTextView setScrollEnabled:YES];
                for(UIView * cellSubviews in detailsCell.subviews)
                {
                    cellSubviews.userInteractionEnabled = NO;
                }
            }
            switch (indexPath.section) {
                case 0:
                {
                    detailsCell.ContentTextView.text =  _categories[indexPath.row][@"cat_name"];
                    detailsCell.selectionStyle = UITableViewCellSelectionStyleDefault;
                    if (_isEditingModeOn) {
                        [detailsCell.arrowImages setHidden:YES];
                    }else{
                        [detailsCell.arrowImages setHidden:NO];
                    }
                    return detailsCell;
                    break;
                }
                case 1:
                {
                    [detailsCell.arrowImages setHidden:YES];
                    detailsCell.ContentTextView.tag = 200;
                    if (_isEditingModeOn && ![_user.aboutMe isEqualToString:@""]) {
                        detailsCell.ContentTextView.text = _user.aboutMe;
                        
                    }else{
                        detailsCell.ContentTextView.text = _profileArray[indexPath.section-1];
                    }
                    detailsCell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return detailsCell;
                    break;
                }
                case 2:
                {
                    [detailsCell.arrowImages setHidden:YES];
                    detailsCell.ContentTextView.tag = 201;
                    if (_isEditingModeOn && ![_user.aboutMe isEqualToString:@""]) {
                        
                        detailsCell.ContentTextView.text = _user.expertises;
                        
                    }else{
                        detailsCell.ContentTextView.text = _profileArray[indexPath.section-1];
                    }
                    detailsCell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return detailsCell;
                    break;
                }
                case 3:
                {
                    [detailsCell.arrowImages setHidden:YES];
                    detailsCell.ContentTextView.tag = 202;
                    if (_isEditingModeOn && ![_user.aboutMe isEqualToString:@""]) {
                        
                        detailsCell.ContentTextView.text = _user.language;
                        
                    }else{
                        detailsCell.ContentTextView.text = _profileArray[indexPath.section-1];
                    }
                    detailsCell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return detailsCell;
                    break;
                }
                default:
                {
                    [detailsCell.arrowImages setHidden:YES];
                    detailsCell.ContentTextView.text =_profileArray[indexPath.section-1];
                    detailsCell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                    return detailsCell;
                    break;
                }
            }
        }
            break;
            
        default:
        {
            cellIdentifier = @"jobimages";
            jobPhotosCell = (collectionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(jobPhotosCell == nil)
            {
                jobPhotosCell =[[collectionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            jobPhotosCell.UpdatedImages=[[NSMutableArray alloc]init];
            [jobPhotosCell reloadCollection:_jobImagesMutableArray];
            return jobPhotosCell;
        }
            break;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:_categories[indexPath.row][@"cat_id"] forKey:@"Cat_ID"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self performSegueWithIdentifier:@"toCategoryDetails" sender:nil];
    }
    NSLog(@"Successfully tapped");
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView * labelview  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 480, 26)];
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(18, 3, 480, 22)];
    UIView * divider  = [[UIView alloc]initWithFrame:CGRectMake(18, 25, 480, 1)];
    labelview.backgroundColor=UIColorFromRGB(0XFFFFFF);
    divider.backgroundColor=UIColorFromRGB(0XEEEEEE);
    labelHeader.minimumScaleFactor=0.5;
    [labelview addSubview:labelHeader];
    [labelview addSubview:divider];
    
    switch (section) {
        case 0:
        {
            [Helper setToLabel:labelHeader Text:NSLocalizedString(@"APPROVED CATEGORIES",@"APPROVED CATEGORIES") WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
            return labelview;
            break;
        }
        case 1:
        {
            
            [Helper setToLabel:labelHeader Text:NSLocalizedString(@"ABOUT ME",@"ABOUT ME") WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
            return labelview;
            break;
        }
        case 2:
        {
            [Helper setToLabel:labelHeader Text:NSLocalizedString(@"AREA OF EXPERTISE",@"AREA OF EXPERTISE") WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
            return labelview;
            break;
        }
        case 3:
        {
            [Helper setToLabel:labelHeader Text:NSLocalizedString(@"LANGUAGE",@"LANGUAGE") WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
            return labelview;
            break;
        }
        case 4:
        {
            [Helper setToLabel:labelHeader Text:NSLocalizedString(@"EMAIL ID",@"EMAIL ID") WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
            return labelview;
            break;
        }
        case 5:
        {
            [Helper setToLabel:labelHeader Text:NSLocalizedString(@"JOB PHOTOS",@"JOB PHOTOS") WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
            return labelview;
            break;
        }
        default:
            return nil;
            break;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view;
    
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,5)];
    view.backgroundColor = UIColorFromRGB(0xEDEDED);
    
    return view;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 5;
}

#pragma mark - Custom Methods -

- (CGFloat)measureHeightLabel: (UITextView *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-26 , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:flStrForObj(label.text)attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return  requiredHeight.size.height;
}

#pragma mark - Custom Methods -

- (CGFloat)measureHeiLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-80 , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:flStrForObj(label.text)attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return  requiredHeight.size.height;
}


#pragma mark - UIImagePickerDelegate -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            [self cameraButtonClicked];
            break;
        }
        case 1:
        {
            [self libraryButtonClicked];
            break;
        }
        case 2:{
            [self removePhoto];
            return;
        }
        default:
            break;
    }
}


-(void)cameraButtonClicked
{
    imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message: NSLocalizedString(@"Camera is not available",@"Camera is not available") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)libraryButtonClicked
{
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePicker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    if (actionSheet.tag == 99) {
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        _profileImgView.image = image;
        CGSize size = CGSizeMake(125, 125);
        pickedImage = [self scaleImage:image toSize:size];
    }else if (actionSheet.tag == 1){
        pickedImage1 = [info objectForKey:UIImagePickerControllerEditedImage];
        pickedImage1 = [self imageWithImage:pickedImage1 scaledToSize:CGSizeMake(300,300)];
        [jobImagesArray addObject:pickedImage1];
        [jobPhotosCell reloadCollectionView:jobImagesArray];
    }
}

- (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)size
{
    
    UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (IBAction)editProfile:(id)sender {
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    if (_editProfileButton.selected == YES) {
        [self.view endEditing:YES];
        _isEditingModeOn = NO;
        _editProfileButton.selected = NO;
        if ((pickedImage != nil)||
            (![_user.fName isEqualToString:[ud objectForKey:@"providerName"]]
             ||![_user.aboutMe isEqualToString:_profileArray[0]]
             ||![_user.expertises isEqualToString:_profileArray[1]]
             ||![_user.language isEqualToString:_profileArray[2]]) ) {
                [self uploadImage];
              //   [self getProfileData];
            }else if ([_imageURl isEqualToString:@""]){
                 [self uploadImage];
            }
        [self.tableView reloadData];
    }else{
        _editProfileButton.selected = YES;
        _isEditingModeOn = YES;
        pickedImage = nil;
        [self getProfileData];
        [jobImagesArray removeAllObjects];
    }
    [jobPhotosCell addImagesSelected];
}

-(void)sectionTapped{
    if ([_addButton.titleLabel.text isEqual:@"ADD IMAGES"]) {
        [Helper setButton:_addButton Text:NSLocalizedString(@"DONE",@"DONE") WithFont:@"opensans" FSize:10 TitleColor:UIColorFromRGB(0XCCCCCC) ShadowColor:UIColorFromRGB(0XCCCCCC)];
       
    }else{
        [Helper setButton:_addButton Text:NSLocalizedString(@"ADD IMAGES",@"ADD IMAGES") WithFont:@"opensans" FSize:10 TitleColor:UIColorFromRGB(0XCCCCCC) ShadowColor:UIColorFromRGB(0XCCCCCC)];
    }
}

- (IBAction)editImage:(id)sender {
    
    _fromProfile = YES;
    _fromJobImages = NO;
    [self.view endEditing:YES];
    actionSheet.tag = 99;
    if (_isEditingModeOn) {
        if (!pickedImage && ([_imageURl isEqualToString:@"http://www.goclean-service.com/pics/default.png"] ||
                             [_imageURl isEqualToString:@""])) {
            actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"), nil];
        }else{
            actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"),NSLocalizedString(@"Remove Photo", @"Remove Photo"), nil];
        }
        actionSheet.tag = 99;
        [actionSheet showInView:self.view];
    }
    
}

- (void)removePhoto{
    pickedImage = nil;
    _profileImgView.image = [UIImage imageNamed:@"my_profile_profile_default_image.png"];
    _imageURl = @"";
}

-(NSString *)getCurrentDateTime
{
    NSString *languageID = [[NSBundle mainBundle] preferredLocalizations].firstObject;//
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:languageID];//
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
    [dateFormatter setLocale:locale]; //
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:now];
    
    NSLog(@"date in string %@ ",dateInStringFormated);
    
    //  NSDate *wer = [dateFormatter dateFromString:returnDate];
    // NSLog(@"date from string %@ ",wer);
    
    return dateInStringFormated;
    
}

/**
 *  upload the image in amazon
 *
 *  @param profile image of received person
 */

-(void)uploadImage
{
    if (pickedImage) {
        UIWindow *window = [[UIApplication sharedApplication]keyWindow];
        [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:NSLocalizedString(@"Updating profile..",@"Updating profile..")];
  //  [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Updating profile.."];
           NSString *name;
        if (_imageURl) {
            
            name = [NSString stringWithFormat:@"%@%@.jpg",_profileArray[3],[self getCurrentDateTime]];
        }else{
            name = [NSString stringWithFormat:@"%@.jpg",_profileArray[3]];
        }
    
    NSString *fullImageName = [NSString stringWithFormat:@"ProfileImages/%@",name];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmssa"];
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
    
    NSData *data = UIImageJPEGRepresentation(pickedImage,0.8);
    [data writeToFile:getImagePath atomically:YES];
    
        
        [AmazonTransfer upload:getImagePath
                       fileKey:fullImageName
                      toBucket:Bucket
                      mimeType:@"image/jpeg"
               completionBlock:^(AWSS3TransferUtilityUploadTask *task, NSError *error){
                   
                   if (!error) {
                       if([[SDImageCache sharedImageCache]diskImageExistsWithKey:[NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName]])
                       {
                           [[SDImageCache sharedImageCache] removeImageForKey:[NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName] fromDisk:YES];
                           NSLog(@"Removed job image SDCache");
                       }
                       
                       NSLog(@"Uploaded Profile Image:%@",[NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName]);
                       pickedImage = nil;
                       [self updateProfileData:[NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName]];
                   }else{
                       NSLog(@"Photo Upload Failed");
                   }
               }
         ];
    }else{
        pickedImage = nil;
        UIWindow *window = [[UIApplication sharedApplication]keyWindow];
        [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:NSLocalizedString(@"Updating profile..",@"Updating profile..")];

//        [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Updating profile.."];
        [self updateProfileData:_imageURl];
    }
}

-(void)updateProfileData:(NSString*)profileImg
{

    
    NSArray *name = [_user.fName componentsSeparatedByString:@" "];
    NSString *lname;
    if ([name count]>1)
    {
        lname = name[1];
    }else
    {
        lname = @" ";
    }

    _user.language = [_user.language stringByReplacingOccurrencesOfString:@"  "
                                                               withString:@""];
    _user.language = [_user.language stringByReplacingOccurrencesOfString:@", "
                                                               withString:@","];
    _user.language = [_user.language stringByReplacingOccurrencesOfString:@" "
                                                               withString:@","];
    
    _user.expertises = [_user.expertises stringByReplacingOccurrencesOfString:@"  "
                                                                   withString:@""];
    _user.expertises = [_user.expertises stringByReplacingOccurrencesOfString:@", "
                                                                   withString:@","];
    _user.expertises = [_user.expertises stringByReplacingOccurrencesOfString:@" "
                                                               withString:@","];

    NSDictionary *dict =@{
                          @"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id":[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                          @"ent_date_time":[Helper getCurrentDateTime],
                          @"ent_about" :flStrForObj( _user.aboutMe),
                          @"ent_name" : flStrForObj(name[0]),
                          @"ent_lang" : flStrForObj(_user.language),
                          @"ent_expertise":flStrForObj(_user.expertises),
                          @"ent_profile_img":flStrForObj(profileImg),
                          @"ent_last_name":flStrForObj(lname)
                          };

    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"updateMasterProfile"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 if ([response[@"errFlag"] integerValue]==1) {
                                     _editProfileButton.selected = YES;
                                     _isEditingModeOn = YES;
                                     [Helper showAlertWithTitle:@"Message" Message:@"Please Enter Provider Name"];
                                     [self.proName becomeFirstResponder];
                                     [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                     if ([response[@"errNum"] integerValue]==83) {
                                         User *logout = [User sharedInstance];
                                         logout.delegate = self;
                                         [logout logout];
                                     }
                                 }else{
                                    
                                     NSLog(@"succeeded");
                                     [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                     [self getProfileData];
                                 }
                             }else{
                                  [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 _editProfileButton.selected = YES;
                                 _isEditingModeOn = YES;
                             }
                         }];
}

#pragma TextView Delegate methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if (_isEditingModeOn) {
        if (textView.tag == 200) {
            [self moveViewUp:-114];
             return YES;
        }else if (textView.tag == 201){
            [self moveViewUp:-204];
             return YES;
        }
        else if (textView.tag == 202){
            [self moveViewUp:-254];
             return YES;
        }
        if (textView.tag >= 199 && textView.tag < 203) {
            return YES;
        }
        return NO;
    }
    return NO;

}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.tag == 200) {
        _user.aboutMe = textView.text;
    }
    else if (textView.tag == 201) {
        _user.expertises = textView.text;
    }
    else if (textView.tag == 202) {
        _user.language = textView.text;
    }
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if([text isEqualToString:@"\n"]) {
        [self.view endEditing:YES];
        [self moveViewDown];
        return NO;
    }

    if (textView.tag == 200) {
        _user.aboutMe = textView.text;

    }
    return YES;
}

#pragma mark - UITextFeildDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if (_isEditingModeOn) {

        if (textField.tag >= 199 && textField.tag < 203) {
            return YES;
        }
              return NO;
    }
    return NO;
}

-(void)moveViewDown{
       [UIView animateWithDuration:0.4
                     animations:^{
                         CGRect frame = _mainScrollView.frame;
                         frame.origin.y = 0;
                         _mainScrollView.frame = frame;
                     }];
}

-(void)moveViewUp:(NSInteger)val{
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         CGRect frame = _mainScrollView.frame;
                         frame.origin.y = val;
                         _mainScrollView.frame = frame;
                         
                     }];
  
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    return YES;
 }

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _proName) {
        _user.fName = textField.text;
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

- (IBAction)forgotPassword:(id)sender {
    [self performSegueWithIdentifier:@"fromProfile" sender:nil];
}

- (IBAction)liveChatAction:(id)sender {
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    LiveChatViewController  *chatVc = [mainstoryboard instantiateViewControllerWithIdentifier:@"liveChatVC"];

    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:chatVc];
    navBar.navigationBar.translucent = NO;
    [self presentViewController:navBar animated:YES completion:nil];

}

@end
