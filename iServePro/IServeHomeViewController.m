//
//  IServeHomeViewController.m
//  iServePro
//
//  Created by -Tony Lu on 25/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "IServeHomeViewController.h"
#import "UploadFiles.h"
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "CustomMarkerView.h"
#import "Helper.h"
#import "UIImageView+WebCache.h"
#import "LocationTracker.h"
#import "iServeSplashController.h"
#import "HomeCell.h"
#import "AppointmentDetailController.h"
#import "ChatSocketIOClient.h"
#import "BookingViewController.h"
#import "InvoiceViewController.h"
#import "TimerViewController.h"
#import "LocationServicesViewController.h"
#import <EventKit/EventKit.h>
#import "AddRemindersAndEvents.h"
#import "ChatSocketIOClient.h"
#import "ChatSIOClient.h"
#import "PMDReachabilityWrapper.h"
#import "LiveChatViewController.h"



#define mapZoomLevel 17
#define BottomShadowImageTag 18

@interface IServeHomeViewController ()<CLLocationManagerDelegate,GMSMapViewDelegate,HomeCellDelegate>

{
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    GMSMapView *mapView_;
    NSArray *bookingArray;
    NSArray *newAppointments;
    UIView *customMapView;
    Boolean isMapViewOpened;
    NSArray *seguesDict;
     CGRect screenSize;
    
     NSArray *laterBookingArray;
    ChatSocketIOClient *socket;
    ChatSIOClient *soketIOClient;
    IServeAppDelegate *tmpDelegate;
    
}

@property (strong, nonatomic) IBOutlet UILabel *noAppoointmentLabel;
@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;
@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;
@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;
@property (nonatomic, strong) UIRefreshControl *refreshControl;


@end

@implementation IServeHomeViewController

- (void)viewDidLoad {
    
    if (!socket || !soketIOClient) {
        socket  =[ChatSocketIOClient sharedInstance];
        soketIOClient =[ChatSIOClient sharedInstance];
    }

    [super viewDidLoad];
    [self getCustomerAppointment];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    _counter = 0;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    CLLocationCoordinate2D coordinate = [self getLocation];
    self.currentLatitude = coordinate.latitude;
    self.currentLongitude = coordinate.longitude;
    [[NSUserDefaults standardUserDefaults] synchronize];
    waypoints_ = [[NSMutableArray alloc]init];
    waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
    seguesDict =@[@"appointmentDetail",@"ToMapController",@"ToTimerVC",@"ToInvoice"];
    _locationManager = [[CLLocationManager alloc]init];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)viewWillAppear:(BOOL)animated
{
    if (!soketIOClient.isSocketConnected) {
        [socket connectSocket];
    }
    
    self.tabBarController.tabBar.hidden = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
    });
    //NEW BOOKING
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCustomerAppointment) name:@"DismissNewBookingVC" object:nil];
    
    NSString *bookingDate = [Helper getCurrentDateTime:[NSDate date] dateFormat:@"MM/dd/YY"];
    NSLog(@"viewWillAppear%@",bookingDate);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCustomerAppointment) name:@"AppointmentCancel" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userSessionTokenExpire) name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:@"AppointmentAcceptAndRejectFromAdmin" object:nil];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"versionAvail"]) {
        [self updateAppVersion];
    }
    if (!tmpDelegate) {
         tmpDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    [tmpDelegate checkDriverStatus];

}

/**
 *  app version updates to server once new version downloaded
 */
-(void)updateAppVersion{
    NSDictionary *dict = @{
                           @"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                           @"ent_dev_id": [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                           @"ent_app_version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                           @"ent_user_type":@"1"
                           };
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"UpdateAppVersion"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"versionAvail"];
                                 NSLog(@"updated to server version");
                             }
                         }];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (!soketIOClient.isSocketConnected) {
        [socket connectSocket];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCustomerAppointment) name:@"DismissNewBookingVC" object:nil];
    [self getCustomerAppointment];
    if (IS_SIMULATOR)
    {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord
                                                         zoom:14];
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        marker.title = NSLocalizedString(@"It's Me", @"It's Me");
        marker.snippet = NSLocalizedString(@"Current Location", @"Current Location");
        marker.map = mapView_;
    }
    else
    {
        NSLog(@"latitude %f",_currentLatitude);
        NSLog(@"logitude %f",_currentLongitude);
    }
    _previouCoord = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
    // static dispatch_once_t onceToken;
    // dispatch_once(&onceToken, ^{
    [socket heartBeat:@"1"];
    //});
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
    });
}
/**
 *  Action for on the job and off the job
 */
-(void)onOffTheJob:(NSString *)status{
    
    if([status intValue] == 3 || [status intValue] == 5)
    {
        _onTheJobOffTheJobButton.selected = YES;
        [self.locationUpdateTimer invalidate];
        self.locationUpdateTimer = nil;
        [_locationManager startUpdatingLocation];
        [self startUpdatingLocationToServer];
    }
    else
    {
        _onTheJobOffTheJobButton.selected = NO;
        [self.locationUpdateTimer invalidate];
        self.locationUpdateTimer = nil;
        [_locationManager stopUpdatingLocation];
        [self.locationTracker stopLocationTracking];
    }
}


/**
 *  button action to represent to off or on the job
 */
- (IBAction)onOffJobButton:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    if (_onTheJobOffTheJobButton.selected) {
        _onTheJobOffTheJobButton.selected = NO;
        [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:NSLocalizedString(@"Going Off the job..",@"Going Off the job..")];
        _status = [NSString stringWithFormat:@"%d",kDriverStatusOffline];
        [ud setValue:@"4" forKey:@"IserveStatus"];
    }
    else {
        _onTheJobOffTheJobButton.selected = YES;
        [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:NSLocalizedString(@"Going On the job..",@"Going On the job..")];
        _status = [NSString stringWithFormat:@"%d",KDriverStatusOnline];
        [ud setValue:@"3" forKey:@"IserveStatus"];
    }
    [self requestUpdateIServeStatus:_status];
    });
    
}
//*******Presenting the live chat view controller**************//
- (IBAction)liveChat:(id)sender {
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    LiveChatViewController  *chatVc = [mainstoryboard instantiateViewControllerWithIdentifier:@"liveChatVC"];
    
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:chatVc];
    navBar.navigationBar.translucent = NO;
    [self presentViewController:navBar animated:YES completion:nil];
}
/**
 *  Service Call for updating the driver status
 *
 *  @param statusType //statusType 3-->Active ,4-->InActive
 */
- (void)requestUpdateIServeStatus:(NSString *)statusType{
    
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    if([[PMDReachabilityWrapper sharedInstance] isNetworkAvailable])
    {
        NSDictionary *parameters = @{
                                     kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                     kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                     KSMPMasterStatus:statusType,
                                     @"ent_date_time":[Helper getCurrentDateTime]
                                     };
        
        NetworkHandler *handler = [NetworkHandler sharedInstance];
        
        [handler composeRequestWithMethod:MethodUpdateMasterStaus
                                  paramas:parameters
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 
                                 if (succeeded) {
                                     [self getIServeResponse:response];
                                     if ([statusType  isEqual: @"3"]) {
                                         _onTheJobOffTheJobButton.selected = YES;
                                         [self.locationUpdateTimer invalidate];
                                         self.locationUpdateTimer = nil;
                                         [_locationManager startUpdatingLocation];
                                         [self startUpdatingLocationToServer];
                                         
                                     }else if([statusType  isEqual: @"4"]){
                                         _onTheJobOffTheJobButton.selected = NO;
                                         [self.locationUpdateTimer invalidate];
                                         self.locationUpdateTimer = nil;
                                         [_locationManager stopUpdatingLocation];
                                         [self.locationTracker stopLocationTracking];
                                     }
                                 }
                                 else{
                                     [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                     if ([statusType  isEqual: @"3"]) {
                                         _onTheJobOffTheJobButton.selected = NO;
                                         [self.locationUpdateTimer invalidate];
                                         self.locationUpdateTimer = nil;
                                         [_locationManager stopUpdatingLocation];
                                         [self.locationTracker stopLocationTracking];
                                         [ud setValue:@"4" forKey:@"IserveStatus"];
                                     }else if([statusType  isEqual: @"4"]){
                                         _onTheJobOffTheJobButton.selected = YES;
                                         [self.locationUpdateTimer invalidate];
                                         self.locationUpdateTimer = nil;
                                         [_locationManager startUpdatingLocation];
                                         [self startUpdatingLocationToServer];
                                         
                                         [ud setValue:@"3" forKey:@"IserveStatus"];
                                     }
                                 }
                             }];
        
    }else{
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        if ([statusType  isEqual: @"3"]) {
            _onTheJobOffTheJobButton.selected = NO;
            [self.locationUpdateTimer invalidate];
            self.locationUpdateTimer = nil;
            [_locationManager stopUpdatingLocation];
            [self.locationTracker stopLocationTracking];
            [ud setValue:@"4" forKey:@"IserveStatus"];
        }else if([statusType  isEqual: @"4"]){
            _onTheJobOffTheJobButton.selected = YES;
            [self.locationUpdateTimer invalidate];
            self.locationUpdateTimer = nil;
            [_locationManager startUpdatingLocation];
            [self startUpdatingLocationToServer];
            [ud setValue:@"3" forKey:@"IserveStatus"];
        }
    }
}

/**
 *  Responding according to the response
 *
 *  @param response describes updated status
 */
- (void)getIServeResponse:(NSDictionary *)response
{
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK" )otherButtonTitles:nil];
        [alertView show];
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Error",@"Error") Message:[response objectForKey:@"Error"]];
        
    }else if ([[response objectForKey:@"errFlag"] intValue] == 1 && [[response objectForKey:@"errNum"] intValue] == 6) {   // session token expire
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        return;
    }
    else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 7||[[response objectForKey:@"errNum"] intValue] == 83) ) {   // session token expire
        [self performSelector:@selector(userSessionTokenExpire) withObject:nil afterDelay:1];
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        return;
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
        }
        else
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
}


/**
 *  update the lat longs for every 2 second to socket
 */
-(void)startUpdatingLocationToServer
{
    self.locationTracker = [LocationTracker sharedInstance];
    [self.locationTracker startLocationTracking];
    NSTimeInterval time = 2.0;
    self.locationUpdateTimer =
    [NSTimer scheduledTimerWithTimeInterval:time
                                     target:self
                                   selector:@selector(updateLocation)
                                   userInfo:nil
                                    repeats:YES];
}

/**
 *  location tracker method will called
 */
-(void)updateLocation {
    [self.locationTracker updateLocationToServer];
}




- (void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AppointmentCancel" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DismissNewBookingVC" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AppointmentAcceptAndRejectFromAdmin" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [mapView_ removeObserver:self forKeyPath:@"myLocation" context:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma Refresh

- (void)refreshTable{
    if ([Helper isConnected]) {
       [self getCustomerAppointment];
        
    }else{
        [Helper showAlertViewForInternetCheck];
    }
}

/**
 *  present the new view to navigate to enable the location services
 */
-(void)checkLocationServices  {
    
    LocationServicesViewController *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
    
}
/**
 *  location manager delegate method
 *
 *  @param manager check the location enabled r not
 *  @param status  always allow, only when app in use
 */
- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    
    if ([CLLocationManager locationServicesEnabled]) {
        switch ([CLLocationManager authorizationStatus]) {
            case kCLAuthorizationStatusAuthorizedAlways:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationServices" object:nil];
                break;
            case kCLAuthorizationStatusDenied:
                [self checkLocationServices];
                break;
            case kCLAuthorizationStatusAuthorizedWhenInUse:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationServices" object:nil];
                break;
            case kCLAuthorizationStatusNotDetermined:
                NSLog(@"Not Determined");
                break;
            case kCLAuthorizationStatusRestricted:
                NSLog(@"Restricted");
                break;
        }
        
    }else{
        LocationServicesViewController *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
        UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
        [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
    }
}


-(NSString *)getMonths
{
    NSDate *date = [NSDate date];
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    NSInteger Day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    //
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month]];
    NSString * dayString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:Day]];
    
    NSString *retMonth = [NSString stringWithFormat:@"%ld-%@-%@",(long)year,monthString,dayString];
    return retMonth;
}

#pragma mark Webservice Request -
-(void)getCustomerAppointment
{
    NSLog(@">>>>>>>>>>>>>>>>>>>>>>>>" );
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:NSLocalizedString(@"Loading...",@"Loading...")];

    
    NSDictionary *parameters = @{
                                 kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_date_time":[Helper getCurrentDateTime]
                                 };
    
    NSLog(@"getCustomerAppointment>>>>>>>>>> %@",parameters);
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:@"getMasterAppointmentsHome"
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (succeeded)
                             {
                                 [self getCustomerAppointmentResponse:response];
                             }
                             else{
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 NSLog(@"Error");
                             }
                         }];
}

/**
 *  main service response
 *
 *  @param response booking details response
 */
-(void)getCustomerAppointmentResponse:(NSDictionary *)response
{
    [self onOffTheJob:response[@"status"]];
    [[NSUserDefaults standardUserDefaults]setObject:response[@"status"] forKey:@"IserveStatus"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Badge"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ChangeStatus" object:nil];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [self.refreshControl endRefreshing];
    
    NSLog(@"response Appointment:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
        [alertView show];
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Error",@"Error") Message:[response objectForKey:@"Error"]];
        
    }
    else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 6|| [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 83 || [response[@"errNum"] intValue] == 76))
    {
        [self performSelector:@selector(userSessionTokenExpire) withObject:nil afterDelay:1];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Expired" message:@"Your session with Goclean Service is expired. Please Login again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
       [alert show];
    }
    
    else
    {
        NSDictionary *dictResponse  = response;
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            bookingArray = dictResponse[@"nowbooking"];
            laterBookingArray = dictResponse[@"laterBookings"];
            
            NSLog(@"tableView1:%@",self.tblView);
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
        }
        else{
            bookingArray=@[];
            laterBookingArray=@[];
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
        }
    }
    [self.tblView reloadData];
    self.heightOfContent.constant = self.tblView.contentSize.height;
    [self.view layoutIfNeeded];
}



#pragma mark Other Methods -

-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}


-(CLLocationCoordinate2D)getLocation
{
    
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [_locationManager requestWhenInUseAuthorization];
            }
        }
    }
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _locationManager.distanceFilter = 50;
    _locationManager.headingFilter = 1;
    [_locationManager startUpdatingLocation];
    [_locationManager startUpdatingHeading];
    CLLocation *location = [_locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    return coordinate;
}

#pragma get font Details
-(void)getFontFamily {
    
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    for (indFamily = 0; indFamily < [familyNames count]; ++indFamily)
    {
        NSLog(@"********Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:[UIFont fontNamesForFamilyName:[familyNames objectAtIndex:indFamily]]];
        for (indFont=0; indFont<[fontNames count]; ++indFont)
        {
            NSLog(@"----------Font name: %@", [fontNames objectAtIndex:indFont]);
        }
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
      self.tabBarController.tabBar.hidden = YES;
    NSInteger segueIndex =[seguesDict indexOfObject:[segue identifier]];
    switch (segueIndex) {
        case 0:
        {
            AppointmentDetailController *details = [segue destinationViewController];
            details.dictAppointmentDetails = sender;
        }
            break;
        case 1:
        {
            BookingViewController *details =[segue destinationViewController];
            details.dictBookingDetails =sender;
        }
            break;
        case 2:
        {
            TimerViewController *details =[segue destinationViewController];
            details.dictBookingDetails = sender;
        }
            break;
        case 3:
        {
            InvoiceViewController *details =[segue destinationViewController];
            details.dictBookingDetails = sender;
        }
            break;
    }
}

#pragma mark UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 123;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView * labelview  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 25)];
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 6, 320, 22)];
    
    [labelview addSubview:labelHeader];
    
    if (section == 0) {
        if (bookingArray.count) {
            [Helper setToLabel:labelHeader Text:@"CURRENT JOBS" WithFont:@"opensans" FSize:11 Color:UIColorFromRGB(0xaaaaaa)];
            labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
        }else if(laterBookingArray.count){
            [Helper setToLabel:labelHeader Text:@"SCHEDULED JOBS" WithFont:@"opensans" FSize:11 Color:UIColorFromRGB(0xaaaaaa)];
            labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
        }

    }
    else if (section == 1){
        
        [Helper setToLabel:labelHeader Text:@"SCHEDULED JOBS" WithFont:@"opensans" FSize:11 Color:UIColorFromRGB(0xaaaaaa)];
        labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
    }
    
    return labelview;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        if (bookingArray.count) {
            NSInteger keyStatus =[bookingArray[indexPath.row][@"status"] integerValue] ;
            switch (keyStatus) {
                case 2:
                    [self performSegueWithIdentifier:@"appointmentDetail" sender:bookingArray[indexPath.row]];
                    break;
                case 5:
                    [self performSegueWithIdentifier:@"ToMapController" sender:bookingArray[indexPath.row]];
                    break;
                case 6:
                    [self performSegueWithIdentifier:@"ToTimerVC" sender:bookingArray[indexPath.row]];
                    break;
                case 21:
                    [self performSegueWithIdentifier:@"ToTimerVC" sender:bookingArray[indexPath.row]];
                    break;
                case 22:
                    [self performSegueWithIdentifier:@"ToInvoice" sender:bookingArray[indexPath.row]];
                    break;
            }
            
        }else{
            NSInteger keyStatus =[laterBookingArray[indexPath.row][@"status"] integerValue] ;
            switch (keyStatus) {
                case 2:
                    [self performSegueWithIdentifier:@"appointmentDetail" sender:laterBookingArray[indexPath.row]];
                    break;
                case 5:
                    [self performSegueWithIdentifier:@"ToMapController" sender:laterBookingArray[indexPath.row]];
                    break;
                case 6:
                    [self performSegueWithIdentifier:@"ToTimerVC" sender:laterBookingArray[indexPath.row]];
                    break;
                case 21:
                    [self performSegueWithIdentifier:@"ToTimerVC" sender:laterBookingArray[indexPath.row]];
                    break;
                case 22:
                    [self performSegueWithIdentifier:@"ToInvoice" sender:laterBookingArray[indexPath.row]];
                    break;
            }
            
        }
    }else{
        
        if (!laterBookingArray.count) {
        }else{
            NSInteger keyStatus =[laterBookingArray[indexPath.row][@"status"] integerValue] ;
            switch (keyStatus) {
                case 2:
                    [self performSegueWithIdentifier:@"appointmentDetail" sender:laterBookingArray[indexPath.row]];
                    break;
                case 5:
                    [self performSegueWithIdentifier:@"ToMapController" sender:laterBookingArray[indexPath.row]];
                    break;
                case 6:
                    [self performSegueWithIdentifier:@"ToTimerVC" sender:laterBookingArray[indexPath.row]];
                    break;
                case 21:
                    [self performSegueWithIdentifier:@"ToTimerVC" sender:laterBookingArray[indexPath.row]];
                    break;
                case 22:
                    [self performSegueWithIdentifier:@"ToInvoice" sender:laterBookingArray[indexPath.row]];
                    break;
            }
        }
    }
    [self.tblView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark UITableview DataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (laterBookingArray.count && bookingArray.count)
    {
        return 2;
    }else if (laterBookingArray.count || bookingArray.count){
        return 1;
    }
    else {
        [self.view bringSubviewToFront:self.noAppoointmentLabel];
        [_noAppoointmentLabel setHidden:NO];
        return 0;
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    if (bookingArray.count == 0 && laterBookingArray.count == 0)
    {
        [self.view bringSubviewToFront:self.noAppoointmentLabel];
        [_noAppoointmentLabel setHidden:NO];
        return 0;
    }else{
        [_noAppoointmentLabel setHidden:YES];
        switch (section) {
            case 0:
            {
                if (bookingArray.count)
                {
                    return bookingArray.count;
                }else if (laterBookingArray.count)
                {
                    return laterBookingArray.count;
                }
                else{
                    return  0;
                }
            }
                break;
            default:
            {
                if (laterBookingArray.count) {
                    return laterBookingArray.count;
                }else{
                    return 0;
                }
            }
                break;
        }
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"HomeViewCell";
    HomeCell *cell;
    if (!cell) {
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    if (indexPath.section == 0) {
        if (bookingArray.count) {
            cell.custName.text = bookingArray[indexPath.row][@"fname"];
            cell.bookingaddress.text=bookingArray[indexPath.row][@"addrLine1"];
            cell.bookingStatus.text = bookingArray[indexPath.row][@"statusMsg"];
            NSString *distance =[NSString stringWithFormat:@"%@ KM Away",[NSNumber numberWithFloat:[bookingArray[indexPath.row][@"distance_met"]floatValue]/1000]];
            cell.DistanceAway.text=distance;
            cell.requestType.text=[NSString stringWithFormat:@" ASAP - %@ Request",bookingArray[indexPath.row][@"cat_name"]];
            cell.bid.text=[NSString stringWithFormat:@"JOB ID:%@",bookingArray[indexPath.row][@"bid"]];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }else if (laterBookingArray.count){
            cell.custName.text = laterBookingArray[indexPath.row][@"fname"];
            cell.bookingaddress.text=laterBookingArray[indexPath.row][@"addrLine1"];
            cell.bookingStatus.text = laterBookingArray[indexPath.row][@"statusMsg"];
            cell.DistanceAway.text=laterBookingArray[indexPath.row][@"apntDt"];
            cell.requestType.text=[NSString stringWithFormat:@" %@ Request",laterBookingArray[indexPath.row][@"cat_name"]];
            cell.bid.text=[NSString stringWithFormat:@"JOB ID:%@",laterBookingArray[indexPath.row][@"bid"]];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            
        }
    }
    else if(indexPath.section == 1){
        if (laterBookingArray.count != 0) {
            cell.custName.text = laterBookingArray[indexPath.row][@"fname"];
            cell.bookingaddress.text=laterBookingArray[indexPath.row][@"addrLine1"];
            cell.bookingStatus.text = laterBookingArray[indexPath.row][@"statusMsg"];
            cell.DistanceAway.text=laterBookingArray[indexPath.row][@"apntDt"];
            cell.requestType.text=[NSString stringWithFormat:@" %@ Request",laterBookingArray[indexPath.row][@"cat_name"]];
            cell.bid.text=[NSString stringWithFormat:@"JOB ID:%@",laterBookingArray[indexPath.row][@"bid"]];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
    }
    return cell;
}




- (NSDateFormatter *)DateFormatterProper
{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return formatter;
}



- (NSDateFormatter *)DateFormatterRequired
{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd MMM, HH:mm a";
    });
    return formatter;
}


#pragma mark - Custom Methods -

- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-38 , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:flStrForObj(label.text)attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return  requiredHeight.size.height;
}


@end
