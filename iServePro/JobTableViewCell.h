//
//  JobTableViewCell.h
//  iServePro
//
//  Created by -Tony Lu on 13/07/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *startTime;

@property (strong, nonatomic) IBOutlet UILabel *endDate;
@property (strong, nonatomic) IBOutlet UILabel *noofbookings;
@property (strong, nonatomic) IBOutlet UILabel *earnings;
@property (strong, nonatomic) IBOutlet UILabel *paidAmt;
@end
