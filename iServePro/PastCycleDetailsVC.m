//
//  PastCycleDetailsVC.m
//  iServeProvider
//
//  Created by -Tony Lu on 19/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "PastCycleDetailsVC.h"

@interface PastCycleDetailsVC ()

@end

@implementation PastCycleDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Past Cycle Details";
    [self pastCycleDetails];
    [self createNavLeftButton];
    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)pastCycleDetails{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    NSNumber *close =[NSNumber numberWithFloat:[flStrForObj(_pastCycleData[@"closing_balance"]) floatValue]];
    NSString *closeBal = [formatter stringFromNumber:close];
    _closedBal.text = closeBal;
    
    NSNumber *earn =[NSNumber numberWithFloat:[flStrForObj(_pastCycleData[@"booking_earn"]) floatValue]];
    NSString *earnBal = [formatter stringFromNumber:earn];
    _earnings.text = earnBal;
    
    NSNumber *open =[NSNumber numberWithFloat:[flStrForObj(_pastCycleData[@"opening_balance"]) floatValue]];
    NSString *openBal = [formatter stringFromNumber:open];
     _openingBal.text = openBal;
    
    NSNumber *paid =[NSNumber numberWithFloat:[flStrForObj(_pastCycleData[@"pay_amount"]) floatValue]];
    NSString *paidBal = [formatter stringFromNumber:paid];
     _paidAmount.text = paidBal;

    
    
_totalAcceptedBookings.text = _pastCycleData[@"total_accepted"];
    
    _startDate.text = _pastCycleData[@"start_date"];
    _endDate.text = _pastCycleData[@"pay_date"];
    _acceptedbookings.text = _pastCycleData[@"tot_accepted"];//completed
    _totalBooking.text = _pastCycleData[@"tot_bookings"];
    _ignoreBookings.text = _pastCycleData[@"total_ignore"];
    _cancelledBookings.text =_pastCycleData[@"tot_cancel"];
    _avgRating.text =_pastCycleData[@"rev_rate"];
    _acceptRate.text =[NSString stringWithFormat:@"%@%%",_pastCycleData[@"acpt_rate"]] ;
    _rejectedBookings.text = _pastCycleData[@"total_reject"];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backButtonPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
