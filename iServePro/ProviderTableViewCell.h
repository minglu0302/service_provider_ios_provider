//
//  ProviderTableViewCell.h
//  iServePro
//
//  Created by -Tony Lu on 31/03/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProviderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *providerCell;
@property (strong, nonatomic)  NSString *providerType;

@end
