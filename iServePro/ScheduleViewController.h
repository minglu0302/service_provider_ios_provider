//
//  ScheduleViewController.h
//  Onthewaypro
//
//  Created by -Tony Lu on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JTCalendar/JTCalendar.h>
@interface ScheduleViewController : UIViewController
@property (strong, nonatomic) IBOutlet JTCalendarMenuView *headerView;
- (IBAction)preCalendar:(id)sender;
@property (strong, nonatomic) JTCalendarManager *calendarManager;
- (IBAction)postCalendar:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *yearLabel;
@property (strong, nonatomic) IBOutlet JTHorizontalCalendarView *mainCalendarView;
@property (strong, nonatomic) IBOutlet UITableView *mainTableView;
- (IBAction)addSlot:(id)sender;
- (IBAction)liveChat:(id)sender;
@end
