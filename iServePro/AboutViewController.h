//
//  AboutViewController.h
//  iServePro
//
//  Created by -Tony Lu on 02/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PastCycleDetailsVC.h"

@interface AboutViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;

- (IBAction)paymentLogs:(id)sender;

- (IBAction)jobsLog:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *contentScrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *leadingContrain;
@property (strong, nonatomic) IBOutlet UIView *currentView;
@property (strong, nonatomic) IBOutlet UIView *pastView;
@property (strong, nonatomic) IBOutlet UIButton *bankDetails;
- (IBAction)bankDetails:(id)sender;
- (IBAction)liveChat:(id)sender;

@end
