//
//  profile.h
//  Roadyo
//
//  Created by -Tony Lu on 09/05/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface profile : NSObject
@property (nonatomic, strong) NSString *fName;
@property (nonatomic, strong) NSString *aboutMe;
@property (nonatomic, strong) NSString *expertises;
@property (nonatomic, strong) NSString *language;



@end
