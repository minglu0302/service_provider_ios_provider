//
//  ScheduleTableCell.h
//  Onthewaypro
//
//  Created by -Tony Lu on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *status;
@property (strong, nonatomic) IBOutlet UILabel *startTime;
@property (strong, nonatomic) IBOutlet UILabel *endTime;
@property (strong, nonatomic) IBOutlet UIView *slotHeaderView;
@end
