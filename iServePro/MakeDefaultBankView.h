//
//  MakeDefaultBankView.h
//  ServeusPro
//
//  Created by -Tony Lu on 17/03/17.
//  Copyright © 2017 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol updateBankDetails <NSObject>

-(void)updateBankDetails;

@end

@interface MakeDefaultBankView : UIView

@property(weak, nonatomic) id<updateBankDetails>delegate;

@property (strong, nonatomic) IBOutlet UIView *contentView;
- (IBAction)closeView:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *holderName;
@property (strong, nonatomic) IBOutlet UILabel *accountNumber;
@property (strong, nonatomic) IBOutlet UILabel *rountingNumber;

@property (strong, nonatomic) NSDictionary *dictDetails;
- (IBAction)makeDefaultAcc:(id)sender;
- (IBAction)defaultAcc:(id)sender;
+ (id)sharedInstance;
- (void)showPopUpWithDictionary:(UIWindow *)window data:(NSDictionary*)dict;

@end
