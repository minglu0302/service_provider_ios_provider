//
//  IServeAppConstant.h
//  iServePro
//
//  Created by -Tony Lu on 25/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#ifndef IServeAppConstant_h
#define IServeAppConstant_h
#endif /* IServeAppConstant_h */

#import "IServeAppConstant.h"


#pragma mark - enums
#define APP_NAME @"Goclean Service Provider"
#define APP_NAVIGATION_BAR @"navigationbar"

#define APP_WEBSITE @"http://www.goclean-service.com"
#define BASE_IP @"www.goclean-service.com/"

#define APP_DRIVER_ITUNES_LINK @"https://itunes.apple.com/us/app/goclean-service-provider/id1331352781?ls=1&mt=8"
#define APP_FACEBOOK_PAGE @"https://www.facebook.com/Go-Clean-1799689986953665/"
#define APP_PRIVACY_POLICY @"http://www.goclean-service.com/PrivacyPolicy.html"
#define APP_TERMS_AND_CONDITIONS @"http://www.goclean-service.com/termcondition.html"

typedef enum {
    
    kNotificationTypeBookingAccept1 = 2,       
    kNotificationTypeBookingReject1 = 3,
    kNotificationTypeBookingOnTheWay = 5,
    kNotificationTypeBookingArrived = 21,
    kNotificationTypeBookingStarted = 6,
    kNotificationTypeBookingCompleted = 22,
    kNotificationTypeBookingInvoice = 7,
    
}BookingNotificationType;

typedef enum {
    KDriverStatusOnline = 3,
    kDriverStatusOffline = 4
}DriverStatus;


#pragma mark - Constants
//eg: give prifix kPMD
extern  NSString *BASE_URL_RESTKIT;
extern  NSString *BASE_URL;

extern NSString *const kISPSocketChannel;
extern NSString *const kPMDPubNubPublisherKey;
extern NSString *const kPMDPubNubSubcriptionKey;
extern NSString *const kPMDGoogleMapsAPIKey;
extern NSString *const kPMDCrashLyticsAPIKey;
extern NSString *const KPMDServerKey;

extern NSString *const kPMDTestDeviceidKey;
extern NSString *const kPMDDeviceIdKey;

#pragma mark - mark URLs

//Base URL
extern NSString *const   baseUrlForXXHDPIImage;
extern NSString *const   baseUrlForOriginalImage;
extern NSString *const   baseUrlForThumbnailImage;
extern NSString *const   baseUrlForUploadImage;


#pragma mark - ServiceMethods

// eg : prifix kSM
extern NSString *const kSMLiveBooking;
extern NSString *const kSMGetAppointmentDetial;
extern NSString *const kSMUpdateSlaveReview;
extern NSString *const kSMGetMasters ;


//Request Params For Logout the user

extern NSString *KDALogoutSessionToken;
extern NSString *KDALogoutUserId;
extern NSString *KDALogoutDateTime;

//Parsms for checking user loged out or not

extern NSString *KDAcheckUserLogedOut;
extern NSString *KDAcheckUserSessionToken;
extern NSString *KDAgetPushToken;

//Params to store the Country & City.

extern NSString *KDACountry;
extern NSString *KDACity;
extern NSString *KDALatitude;
extern NSString *KDALongitude;

//params for firstname
extern NSString *KDAFirstName;
extern NSString *KDALastName;
extern NSString *KDAEmail;
extern NSString *KDAPhoneNo;
extern NSString *KDAPassword;


#pragma mark - NSUserDeafults Keys
//eg : give prefix kNSU

extern NSString *const kNSUIServePubNubChannelkey;
extern NSString *const kNSUAppoinmentIServeDetialKey;
extern NSString *const kNSUIServeEmailAddressKey;
extern NSString *const kNSUIServeProfilePicKey;
extern NSString *const kNSUMongoDataBaseAPIKey;
extern NSString *const kNSUIServeNameKey;
extern NSString *const kNSUPatientPubNubChannelkey;
extern NSString *const kNSUIServePhonekey;
extern NSString *const kNSUIServeTypekey;
extern NSString *const kNSUIServeSubscribeChanelKey;
extern NSString *const kNSUIServeServerChanelKey ;
extern NSString *const kNSUDoctorProfilePicKey;

#pragma mark - PushNotification Payload Keys
//eg : give prefix kPN
extern NSString *const kPNPayloadIServeNameKey;
extern NSString *const kPNPayloadAppoinmentTimeKey;
extern NSString *const kPNPayloadDistanceKey;
extern NSString *const kPNPayloadEstimatedTimeKey;
extern NSString *const kPNPayloadIServeEmailKey;
extern NSString *const kPNPayloadIServeContactNumberKey;
extern NSString *const kPNPayloadProfilePictureUrlKey;
extern NSString *const kPNPayloadAppoinmentDateStringKey;
extern NSString *const kPNPayloadAppoinmentLatitudeKey;



#pragma mark - Notification Name keys
extern NSString *const kNotificationNewCardAddedNameKey;
extern NSString *const kNotificationCardDeletedNameKey;

#pragma mark - Network Error
extern NSString *const kNetworkErrormessage;
extern NSString *const kNSURoadyoPubNubChannelkey;
extern NSString *const kNSUDoctorEmailAddressKey;
extern NSString *const kNSUDoctorPhonekey;
extern NSString *const kNSUDoctorNameKey;
extern NSString *const kNSUDoctorSubscribeChanelKey;
extern NSString *const kNSUDoctorServerChanelKey;




