//
//  TableViewController.h
//  Sup
//
//  Created by -Tony Lu on 3/5/15.
//  Copyright (c) 2015 3embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryNameTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate,UISearchDisplayDelegate>

@property(strong , nonatomic) NSArray *details;
@property (nonatomic,copy) void (^oncomplete)(NSString * code,UIImage *flagimg,NSString * countryCode);
@property (weak, nonatomic) IBOutlet UIButton *navigationBackButton;

- (IBAction)navigationBackButtonAction:(id)sender;


@end
