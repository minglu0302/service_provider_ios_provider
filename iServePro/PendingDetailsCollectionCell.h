//
//  PendingDetailsCollectionCell.h
//  MelikeyPro
//
//  Created by -Tony Lu on 03/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PendingDetailsCollectionCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *appointmentImage;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
