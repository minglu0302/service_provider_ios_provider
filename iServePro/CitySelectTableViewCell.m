
//
//  CitySelectTableViewCell.m
//  iServePro
//
//  Created by -Tony Lu on 26/04/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "CitySelectTableViewCell.h"

@implementation CitySelectTableViewCell

- (void)awakeFromNib {
     [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
