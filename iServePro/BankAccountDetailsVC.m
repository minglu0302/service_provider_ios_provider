//
//  BankAccountDetailsVC.m
//  ServeusPro
//
//  Created by -Tony Lu on 17/03/17.
//  Copyright © 2017 -Tony Lu. All rights reserved.
//

#import "BankAccountDetailsVC.h"
#import "BackDetailsTableViewCell.h"
#import "MakeDefaultBankView.h"

@interface BankAccountDetailsVC ()<updateBankDetails>

{
    MakeDefaultBankView *selectDefaultBank;
    NSMutableArray *mutableArray;
}
@end

@implementation BankAccountDetailsVC

- (void)viewDidLoad {
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0X2598ED)};
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Lato-Regular" size:18.0],NSFontAttributeName,
      nil]];
    self.navigationController.navigationBar.topItem.title =NSLocalizedString(@"Banking Details",@"Banking Details");
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewWillAppear:(BOOL)animated{
   [self getTheSubmmitedBankDetails];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getTheSubmmitedBankDetails{
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
    NSDictionary *dict = @{
                           @"ent_sess_token": [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                           @"ent_dev_id":  [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                           };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"getMasterBankData"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 if ([response[@"errFlag"] integerValue]==1) {
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                      [self.bankDetailsTableView bringSubviewToFront:self.labelText];
                                 }else{
                                     mutableArray = [response[@"data"] mutableCopy];
                                     if (mutableArray.count>0) {
                                         [_labelText setHidden:YES];
                                     }else{
                                         [self.bankDetailsTableView bringSubviewToFront:self.labelText];
                                     }
                                     NSLog(@"bankdetails %@",response);
                                     [_bankDetailsTableView reloadData];
                                 }
                             }
                         }];
    
    
}


- (IBAction)addBackDetails:(id)sender {
    [self performSegueWithIdentifier:@"addAccDetails" sender:nil];
}

- (IBAction)backButton:(id)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    dispatch_async(dispatch_get_main_queue(), ^{
        selectDefaultBank = [MakeDefaultBankView sharedInstance];
        selectDefaultBank.delegate=self;
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [selectDefaultBank showPopUpWithDictionary:window data:mutableArray[indexPath.row]];
    });
}

-(void)updateBankDetails{
    [self getTheSubmmitedBankDetails];
}

#pragma mark UITableview DataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return mutableArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    static NSString *CellIdentifier = @"bankDetails";
    BackDetailsTableViewCell *cell;
    if (!cell) {
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:cell.topView.bounds];
    cell.topView.layer.masksToBounds = NO;
    cell.topView.layer.shadowColor =[UIColor colorWithRed:0.000 green:0.000 blue:0.000 alpha:0.1].CGColor;
    cell.topView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    cell.topView.layer.shadowOpacity = 0.2f;
    cell.topView.layer.shadowPath = shadowPath.CGPath;
    
    cell.accountHolderName.text = mutableArray[indexPath.row][@"account_holder_name"];
    cell.accountNumber.text = mutableArray[indexPath.row][@"last4"];
    
    if ([mutableArray[indexPath.row][@"default_account"]integerValue]==1) {
        cell.selectedButton.selected = YES;
    }else{
        cell.selectedButton.selected = NO;
    }
    
    return cell;
}

@end
