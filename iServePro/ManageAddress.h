//
//  ManageAddress.h
//  Onthewaypro
//
//  Created by -Tony Lu on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ManageAddress : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "ManageAddress+CoreDataProperties.h"
