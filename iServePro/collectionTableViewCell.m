//
//  collectionTableViewCell.m
//  iServePro
//
//  Created by -Tony Lu on 22/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "collectionTableViewCell.h"
#import "ProfileCollectionViewCell.h"
#import "AddImagesCollectionViewCell.h"
#import "ImageViewCollection.h"
#import "AmazonTransfer.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfileViewController.h"

@implementation collectionTableViewCell
{
    ImageViewCollection *collectionImages;
    ProfileViewController *callService;
}

- (void)awakeFromNib {
     [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //_count =0;
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma uicollectionView delegates
-(void)addImagesSelected{
    if (_count==0) {
        _count=1;
        [self.jobCollection reloadData];
    }else{
        _count=0;
        if (_images.count) {
            [self uploadToAmazon];
        }
    }
}

-(void)uploadToAmazon{

    double timestamp = [[NSDate date] timeIntervalSince1970];
    int64_t timeInMilisInt64 = (int64_t)(timestamp*1000);
    _c = 0;

    [[ProgressIndicator sharedInstance] showPIOnView:self withMessage:@"Loading.."];

    dispatch_async(dispatch_get_main_queue(), ^{
        for (int i =(int)_UpdatedImages.count; i<_images.count+_UpdatedImages.count; i++) {
            
            NSString *name = [NSString stringWithFormat:@"%@_%lld.png",[[NSUserDefaults standardUserDefaults] objectForKey:@"proid"],timeInMilisInt64+i];
            
            NSString *fullImageName = [NSString stringWithFormat:@"jobImages/%@",name];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyyMMddhhmmssa"];
            
            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
            
            NSData *data = UIImageJPEGRepresentation(_images[i-_UpdatedImages.count],0.8);
            [data writeToFile:getImagePath atomically:YES];
            
            [AmazonTransfer upload:getImagePath
                           fileKey:fullImageName
                          toBucket:Bucket
                          mimeType:@"image/jpeg"
                   completionBlock:^(AWSS3TransferUtilityUploadTask *task, NSError *error){
                       
                       if (!error) {
                           NSLog(@"Uploaded Profile Image:%@",[NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName]);
                           _c = _c+1;
                           [_UpdatedImages addObject:[NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName]];
                           if (_c == _images.count) {
                               _images = nil;
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                     [self.jobCollection reloadData];
                               });
                               
                             
                           }
                           
                       }else{
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [[ProgressIndicator sharedInstance] hideProgressIndicator];
                           });
                           NSLog(@"Photo Upload Failed");
                       }
                   }
             ];
        }
       //  [[ProgressIndicator sharedInstance] hideProgressIndicator];
    });
}

-(NSString *)getCurrentDateTime
{
 
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:now];
    
    NSLog(@"date in string %@ ",dateInStringFormated);
    

    return dateInStringFormated;
    
}

-(void)updateTheCount{
       [[ProgressIndicator sharedInstance] showPIOnView:self withMessage:@"Loading.."];
    NSDictionary *dict =@{
                          @"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id":[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                          @"ent_job_img":_UpdatedImages,
                          };
    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"updateMasterJobImages"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 _images = nil;
                                 if ([response[@"errFlag"] integerValue]==0) {
                                     NSLog(@"succeeded");
                                     [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 }else{
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                     [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 }
                                 
                             }
                         }];
}


-(void)reloadCollectionView:(NSMutableArray*)jobImages{
    _images = jobImages ;
    [self.jobCollection reloadData];
}


-(void)reloadCollection:(NSMutableArray*)jobImages{
    _UpdatedImages=jobImages;;
    [self.jobCollection reloadData];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (_images.count == 0) {
         [self updateTheCount];
    }
    return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(section == 1){
        return _count;
    }else
    {
        if (!self.images.count){
            return  _UpdatedImages.count;
        }else{
            
            return self.images.count+_UpdatedImages.count;
        }
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    if(indexPath.section == 0){
        cellIdentifier = @"images";
        ProfileCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        if (_UpdatedImages.count>indexPath.row) {
            
            NSString *strImageUrl =_UpdatedImages[indexPath.row];
            [cell.activityIndicator startAnimating];
           
            [cell.jobImages sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                               placeholderImage:[UIImage imageNamed:@"user_image_default"]
                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                          [cell.activityIndicator stopAnimating];
                                      }];
        }else{
            if (_images.count) {
                cell.jobImages.image =self.images[indexPath.row-_UpdatedImages.count];
            }else{
                
                cell.jobImages.image =_UpdatedImages[indexPath.row];
            }
        }
        return cell;
    }else
    {
        cellIdentifier = @"addImages";
        
        AddImagesCollectionViewCell *addPhotosButtonCell = (AddImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        return addPhotosButtonCell;
        
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_count == 0) {
     collectionImages= [ImageViewCollection sharedInstance];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    NSInteger profileTag =1;
    [collectionImages showPopUpWithDictionary:window jobImages:_UpdatedImages.count index:indexPath tag:profileTag jobImages:_UpdatedImages];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                            action:@selector(activateDeletionMode:)];
    longPress.delegate = self;
    [collectionView addGestureRecognizer:longPress];
    }
}

- (void)activateDeletionMode:(UILongPressGestureRecognizer *)gr
{
    
    if (gr.state == UIGestureRecognizerStateBegan) {
        if (!_isDeleteActive) {
            NSIndexPath *indexPath = [_jobCollection indexPathForItemAtPoint:[gr locationInView:_jobCollection]];
            UICollectionViewCell *cell = [_jobCollection cellForItemAtIndexPath:indexPath];
            _deletedIndexpath = indexPath.row;
            [cell addSubview:_buttonDelete];
            [_buttonDelete bringSubviewToFront:_jobCollection];
            [self delete:_buttonDelete];
        }
    }
}
- (void)delete:(UIButton *)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirm", @"Confirm")
                                                        message:NSLocalizedString(@"Are you sure to delete the image?",@"Are you sure to delete the image?")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"No", @"No")
                                              otherButtonTitles:NSLocalizedString(@"Yes", @"Yes"), nil];
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [_UpdatedImages removeObjectAtIndex:_deletedIndexpath];
        [_buttonDelete removeFromSuperview];
        [self.jobCollection reloadData];
        [self updateTheCount];
    }
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(49,49);
}


@end
