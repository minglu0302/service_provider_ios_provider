//
//  ScheduleImagesCell.h
//  iServeProvider
//
//  Created by -Tony Lu on 16/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleImagesCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *scheduledImage;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
