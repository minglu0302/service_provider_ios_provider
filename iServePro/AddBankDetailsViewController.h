//
//  AddBankDetailsViewController.h
//  ServeusPro
//
//  Created by -Tony Lu on 17/03/17.
//  Copyright © 2017 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBankDetailsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *holderNameTF;

@property (strong, nonatomic) IBOutlet UITextField *accountNumberTF;

@property (strong, nonatomic) IBOutlet UITextField *dobTF;

@property (strong, nonatomic) IBOutlet UITextField *personalIDNo;

@property (strong, nonatomic) IBOutlet UITextField *routingNo;

@property (strong, nonatomic) IBOutlet UITextField *addressTF;

@property (strong, nonatomic) IBOutlet UITextField *city;

@property (strong, nonatomic) IBOutlet UITextField *state;

@property (strong, nonatomic) IBOutlet UITextField *postalTF;
@property (strong, nonatomic) IBOutlet UIButton *selectThePhotoID;
- (IBAction)selectThePhotoID:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *photoID;


- (IBAction)backButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;

- (IBAction)submitTheBankDetails:(id)sender;

@end
