//
//  CategoryDetailsUpdate.m
//  iServeProvider
//
//  Created by -Tony Lu on 18/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "CategoryDetailsUpdate.h"

@interface CategoryDetailsUpdate ()
{
    UIBarButtonItem *rightBarButtonItem;
}
@end

@implementation CategoryDetailsUpdate

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Category Details";
    [self getTheCategoryDetails];
    _categoryNameTF.userInteractionEnabled = NO;
    _curencyTF.userInteractionEnabled =NO;
    _priceMinTF.userInteractionEnabled=NO;
    _radiusTF.userInteractionEnabled=NO;
    _currencyLabel.text = @"Price Set By";
    [self createNavLeftButton];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    // Do any additional setup after loading the view.
}
-(void)dismissKeyboard{
    [self moveDownView];
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//******************** Get the category details using pro id, cat id*******************//

-(void)getTheCategoryDetails{
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
    NSDictionary *dict =@{
                          @"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id": [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                          @"ent_cat_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"Cat_ID"],
                          @"ent_pro_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"]
                          };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"GetCategoryDetail" paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 if ([response[@"errFlag"] integerValue]==0) {
                                     _dictCategory =  response[@"data"];
                                     [self categoryDetails:response[@"data"]];
                                     
                                 }else{
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 }
                             }
                         }];
}


-(void)categoryDetails:(NSDictionary *)dict
{
    _categoryNameTF.text = [NSString stringWithFormat:@"%@",dict[@"cat_name"]];
    _curencyTF.text = dict[@"price_set_by"];
    
    _radiusTF.text =[NSString stringWithFormat:@"%@ miles",dict[@"radius"]];
    _radius =[NSString stringWithFormat:@"%@", dict[@"radius"]];
    _price =[NSString stringWithFormat:@"%f", [dict[@"price_min"] floatValue]];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSNumber *priceMin =[NSNumber numberWithFloat:[dict[@"price_min"] floatValue]];
    NSString *priceMinFee = [formatter stringFromNumber:priceMin];
    _priceMinTF.text = priceMinFee;
    
    NSNumber *visitFee =[NSNumber numberWithFloat:[dict[@"visit_fees"] floatValue]];
    NSString *visitFees = [formatter stringFromNumber:visitFee];
    _visitFeeTF.text = visitFees;
    
    if ([dict[@"price_set_by"] isEqualToString:@"Provider"]) {
        
        rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit",@"Edit") style:UIBarButtonItemStylePlain
                                                             target:self action:@selector(updateCategoryData)];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        self.navigationItem.rightBarButtonItem.tintColor =UIColorFromRGB(0X559ED6);
        
    }
    
}

//********** creating left navigation button**********//
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backButtonPressed
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)updateCategoryData
{
    _priceMinTF.userInteractionEnabled=YES;
    _radiusTF.userInteractionEnabled=YES;
    
    [Helper showAlertWithTitle:@"Message" Message:NSLocalizedString(@"You Can Edit PricePerMin and Radius",@"You Can Edit PricePerMin and Radius")];
    rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save",@"Save") style:UIBarButtonItemStylePlain
                                                         target:self action:@selector(updateServiceCategory)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    self.navigationItem.rightBarButtonItem.tintColor =UIColorFromRGB(0X559ED6);
    [self dismissKeyboard];
    
}

/*********Updating the category price per minute and distance*************/

-(void)updateServiceCategory
{
    if (_priceMinTF.text.length <= 0) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter the Price Per Hour"];
    }else if(_radiusTF.text.length <= 0){
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter the Radius Per Hour"];
    }else{
        
        if ([_price isEqualToString:@""]) {
            _price = _priceMinTF.text;
        }
        if ([_radius isEqualToString:@""]) {
            _radius = _radiusTF.text;
        }
        [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
        NSDictionary *dict =@{
                              @"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                              @"ent_dev_id": [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                              @"ent_price_min":[NSString stringWithFormat:@"%f",[_price floatValue]],
                              @"ent_pro_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                              @"ent_cat_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"Cat_ID"],
                              @"ent_radius":_radius
                              };
        NetworkHandler *handler = [NetworkHandler sharedInstance];
        [handler composeRequestWithMethod:@"UpdateCategoryDetail"
                                  paramas:dict
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
                                 if (succeeded) {
                                     if([response[@"errFlag"] integerValue]==1)
                                     {
                                         [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                         [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                     }else{
                                         [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                         [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                         [self.navigationController popViewControllerAnimated:YES];
                                     }
                                 }
                             }];
    }
}
#pragma mark - UITextFeildDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _priceMinTF) {
        _priceMinTF.text = @"";
        _price =@"";
        
        
    }else if(textField == _radiusTF){
        _radiusTF.text = @"";
        _radius = @"";
        [self moveViewUp:100];
    }
    return YES;
}

-(void)moveViewUp:(NSInteger)moveUP{
    [UIView animateWithDuration:0.4
                     animations:^{
                         CGRect frame = self.view.frame;
                         frame.origin.y=-moveUP;
                         self.view.frame=frame;
                     }];
}
-(void)moveDownView{
    [UIView animateWithDuration:0.4
                     animations:^{
                         CGRect frame = self.view.frame;
                         frame.origin.y=0;
                         self.view.frame=frame;
                     }];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField==_priceMinTF || textField==_radiusTF){
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString    *regex     = @"\\d{0,5}(\\.\\d{0,2})?";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        return [predicate evaluateWithObject:newString];
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    if (textField ==  _priceMinTF && textField.text.length > 0) {
        _price = textField.text;
        NSNumber *priceMin =[NSNumber numberWithFloat:[textField.text floatValue]];
        NSString *priceMinFee = [formatter stringFromNumber:priceMin];
        _priceMinTF.text = priceMinFee;
        
    }else if(textField == _radiusTF && textField.text.length > 0){
        _radius = textField.text;
        _radiusTF.text = [NSString stringWithFormat:@"%@ miles",textField.text];;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self moveDownView];
    [self.view endEditing:YES];
    return YES;
}


@end
