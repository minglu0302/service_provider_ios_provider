//
//  main.m
//  iServePro
//
//  Created by -Tony Lu on 24/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IServeAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IServeAppDelegate class]));
    }
}
