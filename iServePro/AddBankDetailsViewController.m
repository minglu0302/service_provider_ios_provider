//
//  AddBankDetailsViewController.m
//  ServeusPro
//
//  Created by -Tony Lu on 17/03/17.
//  Copyright © 2017 -Tony Lu. All rights reserved.
//

#import "AddBankDetailsViewController.h"
#import "AmazonTransfer.h"

@interface AddBankDetailsViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate>
{
    UITextField *activeTextField;
}
@property (strong, nonatomic) UIImage *pickedImage;
@property (strong, nonatomic) NSString *imageUrl;

@end

@implementation AddBankDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _imageUrl = nil;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0X2598ED)};
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Lato-Regular" size:18.0],NSFontAttributeName,
      nil]];
    self.navigationController.navigationBar.topItem.title =NSLocalizedString(@"Add Account",@"Add Account");

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissView)];
    [self.view addGestureRecognizer:tapGesture];
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)dismissView{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.view.alpha=1.0f;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [self.view endEditing:YES];
}
-(void) keyboardWillShow:(NSNotification *)note
{
    CGSize keyboardSize = [[[note userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    float height = MIN(keyboardSize.height,keyboardSize.width);
    //float width  = MAX(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:height];
    
}


-(void) keyboardWillHide:(NSNotification *)note
{
    [self moveViewDown];
}

- (IBAction)selectThePhotoID:(id)sender {
    [self.view endEditing:YES];
    UIActionSheet *actionSheet;
    if (!_pickedImage) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"), nil];
    }else{
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"),NSLocalizedString(@"Remove Photo", @"Remove Photo"), nil];
    }
    
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            case 2:{
                [self removePhoto];
                return;
            }
            default:
                break;
        }
    }
}

-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)removePhoto{
    _pickedImage = nil;
    _photoID.image = [UIImage imageNamed:@"home_profile_default_image_frame.png"];
}

-(void)cameraButtonClicked:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message: NSLocalizedString(@"Camera is not available",@"Camera is not available") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil, nil];
        [alert show];
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    _photoID.image = image;
    
    CGSize size = CGSizeMake(125, 125);
    _pickedImage = [self scaleImage:image toSize:size];
}

- (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)size
{
    
    UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
    
}

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UIView *)textfield andKeyboardHeight:(float)height {
    
    float textfieldMaxY = CGRectGetMaxY(textfield.frame) + 10;
    UIView *view = [textfield superview];
    
    while (view != [self.view superview]) {
        textfieldMaxY += CGRectGetMinY(view.frame);
        view = [view superview];
    }
    
    float remainder = CGRectGetHeight(self.view.window.frame) - (textfieldMaxY + height);
    if (remainder >= 0) {
    }
    else {
        [UIView animateWithDuration:0.4
                         animations:^{
                             self.mainScrollView.contentOffset = CGPointMake(0, -remainder);
                         }];
    }
}
/**
 *  Scroll up when keyboard hides
 */
- (void)moveViewDown {
    self.mainScrollView.contentOffset = CGPointMake(0, 0);
}


#pragma mark - textfield


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
       return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _dobTF) {
        NSString *filter = @"##/##/####";
        if(!filter) return YES; // No filter provided, allow anything
        
        NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if(range.length == 1 && // Only do for single deletes
           string.length < range.length &&
           [[textField.text substringWithRange:range] rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location == NSNotFound)
        {
            // Something was deleted.  Delete past the previous number
            NSInteger location = changedString.length-1;
            if(location > 0)
            {
                for(; location > 0; location--)
                {
                    if(isdigit([changedString characterAtIndex:location]))
                    {
                        break;
                    }
                }
                changedString = [changedString substringToIndex:location];
            }
        }
        
        textField.text = filteredPhoneStringFromStringWithFilter(changedString, filter);
        
        return NO;
    }else{
        return YES;
    }
}

NSMutableString *filteredPhoneStringFromStringWithFilter(NSString *string, NSString *filter)
{
    NSUInteger onOriginal = 0, onFilter = 0, onOutput = 0;
    char outputString[([filter length])];
    BOOL done = NO;
    
    while(onFilter < [filter length] && !done)
    {
        char filterChar = [filter characterAtIndex:onFilter];
        char originalChar = onOriginal >= string.length ? '\0' : [string characterAtIndex:onOriginal];
        switch (filterChar) {
            case '#':
                if(originalChar=='\0')
                {
                    // We have no more input numbers for the filter.  We're done.
                    done = YES;
                    break;
                }
                if(isdigit(originalChar))
                {
                    outputString[onOutput] = originalChar;
                    onOriginal++;
                    onFilter++;
                    onOutput++;
                }
                else
                {
                    onOriginal++;
                }
                break;
            default:
                // Any other character will automatically be inserted for the user as they type (spaces, - etc..) or deleted as they delete if there are more numbers to come.
                outputString[onOutput] = filterChar;
                onOutput++;
                onFilter++;
                if(originalChar == filterChar)
                    onOriginal++;
                break;
        }
    }
    outputString[onOutput] = '\0'; // Cap the output string
    return [NSMutableString stringWithUTF8String:outputString];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == _holderNameTF)
    {
        [_accountNumberTF becomeFirstResponder];
    }
    else if(textField == _accountNumberTF)
        
    {
        [_dobTF becomeFirstResponder];
    }
    else if (textField == _dobTF)
    {
        [_personalIDNo becomeFirstResponder];
    }
    
    else if (textField == _personalIDNo)
    {
        [_routingNo becomeFirstResponder];
    }
    
    else if (textField == _routingNo)
    {
       [_addressTF becomeFirstResponder];
    }
    else if (textField == _addressTF)
    {
        [_city becomeFirstResponder];
    }
    else if (textField == _city)
    {
        [_state becomeFirstResponder];
    }

    else if (textField == _state)
    {
        [_postalTF becomeFirstResponder];
    }
    else
    {
        [self.view endEditing:YES];
    }
    return YES;
}

-(void)submitBankDetailsToServer{
    NSString *holderName = _holderNameTF.text;
    NSString *accountNo = _accountNumberTF.text;
    NSString *dob = _dobTF.text;
    NSString *personalID = _personalIDNo.text;
    NSString *rountingNo = _routingNo.text;
    NSString *address = _addressTF.text;
    NSString *city = _city.text;
    NSString *state = _state.text;
    NSString *postal = _postalTF.text;
    
    if (holderName.length < 1) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please Enter the Account Holder Name"];
    }
    else if (accountNo.length < 1){
         [Helper showAlertWithTitle:@"Message" Message:@"Please Enter the Account Number"];
    }
    else if (dob.length < 1){
        [Helper showAlertWithTitle:@"Message" Message:@"Please Enter the Date Of Birth"];
    }
    else if (personalID.length < 1){
        [Helper showAlertWithTitle:@"Message" Message:@"Please Enter the SSN Number"];
    }
    else if (rountingNo.length < 1){
        [Helper showAlertWithTitle:@"Message" Message:@"Please Enter the Routing Number"];
    }
    else if (address.length < 1){
        [Helper showAlertWithTitle:@"Message" Message:@"Please Enter the Address"];
    }
    else if (city.length < 1){
        [Helper showAlertWithTitle:@"Message" Message:@"Please Enter the City"];
    }
    else if (state.length < 1){
        [Helper showAlertWithTitle:@"Message" Message:@"Please Enter the State"];
    }
    else if (postal.length < 1){
        [Helper showAlertWithTitle:@"Message" Message:@"Please Enter the Postal"];
    }else{
        
        NSDictionary *dict = @{
                               @"ent_sess_token": [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                               @"ent_dev_id":  [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                               @"account_holder_name":holderName,
                               @"account_number":accountNo,
                               @"dob":dob,
                               @"personal_id":personalID,
                               @"routing_number":rountingNo,
                               @"address":address,
                               @"city":city,
                               @"state":state,
                               @"postal_code":postal,
                               @"IdProof":_imageUrl
                               };
        NetworkHandler *handler = [NetworkHandler sharedInstance];
        [handler composeRequestWithMethod:@"AddBankDetails" paramas:dict
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
                                 if (succeeded) {
                                     [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                     if ([response[@"errFlag"] integerValue]==1) {
                                         [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                     }else{
                                         [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                         [self.navigationController popViewControllerAnimated:YES];
                                     }
                                 }
                             }];
    }

}
/**
 *  get the current date
 *
 *  @return returns formatted time
 */
-(NSString *)getCurrentTimeDate
{
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:now];
    NSLog(@"date in string %@ ",dateInStringFormated);
    return dateInStringFormated;
    
}

/**
 *  upload the image in amazon
 *
 *  @param sign image of received person
 */
-(void)uploadImage:(UIImage *)signImage
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...",@"Loading...")];
    NSString *name = [NSString stringWithFormat:@"%@.jpg",[self getCurrentTimeDate]];
    
    NSString *fullImageName = [NSString stringWithFormat:@"invoice/%@",name];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmssa"];
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
    
    NSData *data = UIImageJPEGRepresentation(signImage,0.8);
    [data writeToFile:getImagePath atomically:YES];
    
    [AmazonTransfer upload:getImagePath
                   fileKey:fullImageName
                  toBucket:Bucket
                  mimeType:@"image/jpeg"
           completionBlock:^(AWSS3TransferUtilityUploadTask *task, NSError *error){
               
               if (!error) {
                   NSLog(@"Uploaded Profile Image:%@",[NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName]);
                   
                   _imageUrl = [NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName];
                   [self submitBankDetailsToServer];
               }else{
                   NSLog(@"Photo Upload Failed");
               }
               
           }
     ];
    
}


- (IBAction)submitTheBankDetails:(id)sender {
    if (_pickedImage) {
        if (_imageUrl) {
             [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...",@"Loading...")];
            [self submitBankDetailsToServer];
        }else{
            [self uploadImage:_pickedImage];
        }
    }else{
        [Helper showAlertWithTitle:@"Message" Message:@"Select the Photo ID"];
    }
}
@end
