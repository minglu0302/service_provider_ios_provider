//
//  CitySelectTableViewController.h
//  iServePro
//
//  Created by -Tony Lu on 25/04/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectedCityDelegate <NSObject>

- (void)SelectedCity:(NSString *)city;

@end

@interface CitySelectTableViewController : UITableViewController



@property (weak, nonatomic) id<SelectedCityDelegate>delegate;

- (IBAction)CancelAction:(id)sender;
- (IBAction)addActiom:(id)sender;

@end
