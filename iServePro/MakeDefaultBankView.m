//
//  MakeDefaultBankView.m
//  ServeusPro
//
//  Created by -Tony Lu on 17/03/17.
//  Copyright © 2017 -Tony Lu. All rights reserved.
//

#import "MakeDefaultBankView.h"

@implementation MakeDefaultBankView

static MakeDefaultBankView *share;

+(id)sharedInstance
{
    
    if (!share ) {
        
        share=[[self alloc]init];
        
    }
    return share;
    
}
- (instancetype)init{
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"SelectDefaultBank"
                                          owner:self
                                        options:nil] firstObject];
    
 
    
    return self;
}
- (void)showPopUpWithDictionary:(UIWindow *)window data:(NSDictionary*)dict{
    
    _dictDetails = dict;
    _accountNumber.text = dict[@"last4"];
    _holderName.text = dict[@"account_holder_name"];
    _rountingNumber.text = dict[@"routing_number"];
    self.frame = window.frame;
    [window addSubview:self];
    self.contentView.alpha = 0.3;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}
- (IBAction)closeView:(id)sender {
    self.contentView.alpha = 1;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 0.3;
                     }
                     completion:^(BOOL finished) {
                         [_delegate updateBankDetails];
                         [self removeFromSuperview];
                     }];

}
- (IBAction)makeDefaultAcc:(id)sender {

    [self makeDefaultAccount];
   
}
-(void)deleteAccount
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirm", @"Confirm")
                                                        message:NSLocalizedString(@"Are you sure you want to Delete Account?", @"Are you sure you want to Delete Account?")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"No", @"No")
                                              otherButtonTitles:NSLocalizedString(@"Yes", @"Yes"), nil];
    [alertView show];
}

/*-------------------------------*/
#pragma mark - UIAlertViewDelegate
/*-------------------------------*/
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self deleteTheAccount];
    }
}

- (IBAction)defaultAcc:(id)sender {  //Delete the account
    [self deleteAccount];
}

-(void)makeDefaultAccount{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self
         withMessage:NSLocalizedString(@"Loading..", @"Loading..")];
    NSDictionary *dict = @{
                           @"ent_sess_token": [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                           @"ent_dev_id":  [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                           @"bank_token":_dictDetails[@"bank_id"]
                           };
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"BankAccountDefault"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [pi hideProgressIndicator];
                                 if ([response[@"errFlag"] integerValue]==1) {
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 }else{
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                     [self closeView:nil];
                                 }
                                 
                             }
                         }];
}



-(void)deleteTheAccount{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self
         withMessage:NSLocalizedString(@"Deleting Account..", @"Deleting Account..")];
    NSDictionary *dict = @{
                           @"ent_sess_token": [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                           @"ent_dev_id":  [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                           @"bank_token":_dictDetails[@"bank_id"]
                           };
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"deleteMasterBank"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [pi hideProgressIndicator];
                                 if ([response[@"errFlag"] integerValue]==1) {
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 }else{
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                     [self closeView:nil];
                                 }
                             }
                         }];
    
}
@end
