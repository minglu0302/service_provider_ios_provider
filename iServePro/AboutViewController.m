//
//  AboutViewController.m
//  iServePro
//
//  Created by -Tony Lu on 02/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "AboutViewController.h"
#import "CustomNavigationBar.h"
#import "iServeSplashController.h"
#import "LocationTracker.h"
#import "User.h"
#import "PaymentTableViewCell.h"
#import "JobTableViewCell.h"
#import "LiveChatViewController.h"

@interface AboutViewController () <CustomNavigationBarDelegate,UserDelegate>
@property (strong, nonatomic) IBOutlet UIButton *jobLogs;
@property (strong, nonatomic) IBOutlet UIButton *paymentLogs;
@property (strong, nonatomic) NSMutableArray *pastCycle;;
@property (strong, nonatomic) NSDictionary *currentCycle;

@property (strong, nonatomic) IBOutlet UITableView *pastTableView;
@property (strong, nonatomic) IBOutlet UITableView *currentTableView;

@end

@implementation AboutViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
     _paymentLogs.selected = YES;
   self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x2598ED)};
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)getFinancialData{
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:NSLocalizedString(@"Loading...",@"Loading...")];
    _pastCycle = [[NSMutableArray alloc]init];
    NSDictionary *dict =@{
                          @"ent_sess_token": [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id":  [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                          @"ent_date_time":[Helper getCurrentDateTime],
                          @"ent_pro_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"]
                          };
    NetworkHandler *handler  =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"GetFinancialData"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 _pastCycle = [response[@"pastCycle"] mutableCopy];
                                 _currentCycle = response[@"currentCycle"];
                                 NSLog(@"financial data %@",response);
                                 [self.currentTableView reloadData];
                                 [self.pastTableView reloadData];
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             }
                         }];
}


-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userSessionTokenExpire) name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    
}


-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    
}

-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated {
    _pastCycle = [[NSMutableArray alloc]init];
    [self getFinancialData];
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountDeactivated) name:@"accountDeactivated" object:nil];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)userDidLogoutSucessfully:(BOOL)sucess {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}


-(void)userDidFailedToLogout:(NSError *)error {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)accountDeactivated {
    
    User *user = [User sharedInstance];
    user.delegate = self;
    [user logout];
}

- (IBAction)paymentLogs:(id)sender {
    CGRect frame = _mainScrollView.bounds;
    frame.origin.x = 0;
    [_mainScrollView scrollRectToVisible:frame animated:YES];
    [self.currentTableView reloadData];

}

- (IBAction)jobsLog:(id)sender {
    CGRect frame = _mainScrollView.bounds;
    frame.origin.x = self.view.frame.size.width;
    [_mainScrollView scrollRectToVisible:frame animated:YES];
    [self.pastTableView reloadData];

}
/*---------------------------------*/
#pragma mark - Scrollview Delegate
/*---------------------------------*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:_mainScrollView]) {
        
        CGPoint offset = scrollView.contentOffset;
        float widthOfView = CGRectGetWidth(self.view.frame);
        
        // Change Leading position of Seperator/ Selector
        self.leadingContrain.constant = offset.x / 2;
         if (offset.x < widthOfView / 2) {
             
        _paymentLogs.selected = YES;
            _jobLogs.selected = NO;
        }
        else {
            _paymentLogs.selected = NO;
            _jobLogs.selected = YES;
        }
        
        CGFloat minOffsetX = 0;
        CGFloat maxOffsetX = widthOfView * 2;
        
        if (offset.x < minOffsetX) offset.x = minOffsetX;
        if (offset.x > maxOffsetX) offset.x = maxOffsetX;
        
        scrollView.contentOffset = offset;
    }
}

#pragma mark UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1) {
        return 375;
    }else{
        return 153;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier  isEqual: @"pastCycleDetails"]) {
        PastCycleDetailsVC *details = [segue destinationViewController];
        details.pastCycleData = sender;
    }
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == 2) {
        if (_pastCycle.count) {
        [self performSegueWithIdentifier:@"pastCycleDetails" sender:_pastCycle[indexPath.row]];
        }
    }
}


#pragma mark UITableview DataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (tableView.tag == 1) {
        if (_currentCycle) {
             return 1;
        }else{
            return 0;
        }
    }else{
        if (_pastCycle.count) {
            return _pastCycle.count;
        }
        else{
            return 0;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    NSLog(@"cellForRowAtIndexPath");
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    static NSString *CellIdentifier = @"payment";
    PaymentTableViewCell *current;
    if (!current) {
        current=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    static NSString *CellIdentifier1 = @"past";
    JobTableViewCell *past;
    if (!past) {
        past=[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    }
    if (tableView.tag == 1) {
        //        current.layer.cornerRadius = 4;
        //        current.layer.masksToBounds = YES;
        //        current.layer.borderWidth = 2;
        //        current.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
        
        current.startTime.text =[NSString stringWithFormat:@"%@", _currentCycle[@"start_date"]];
        current.avgRatings.text =[NSString stringWithFormat:@"%@",_currentCycle[@"rev_rate"]];
        current.acceptRating.text=[NSString stringWithFormat:@"%@%%", _currentCycle[@"acpt_rate"]];
        current.noOfbookings.text =[NSString stringWithFormat:@"%@", _currentCycle[@"total_bookings"]];
        current.totAcceptBookings.text =[NSString stringWithFormat:@"%@", _currentCycle[@"total_accepted"]];
        
        current.acceptedBooking.text =[NSString stringWithFormat:@"%@",_currentCycle[@"total_completed"]];//completed
        current.rejectedbooking.text =[NSString stringWithFormat:@"%@",_currentCycle[@"total_reject"]];
        current.ignoreBooking.text =[NSString stringWithFormat:@"%@",_currentCycle[@"total_ignore"]];
        current.cancelledBookings.text = [NSString stringWithFormat:@"%@",_currentCycle[@"tot_cancel"]];
        
        
        NSNumber *bal =[NSNumber numberWithFloat:[flStrForObj(_currentCycle[@"opening_balance"]) floatValue]];
        NSString *openBal = [formatter stringFromNumber:bal];
        current.openbal.text =openBal;
        
        NSNumber *bal1 =[NSNumber numberWithFloat:[flStrForObj(_currentCycle[@"closing_balance"]) floatValue]];
        NSString *closBal = [formatter stringFromNumber:bal1];
        current.closedbal.text = closBal;
        
        
        NSNumber *earn =[NSNumber numberWithFloat:[flStrForObj(_currentCycle[@"booking_earn"]) floatValue]];
        NSString *earnBal = [formatter stringFromNumber:earn];
        current.earinings.text = earnBal;
        
        current.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = current;
    }else{
        past.layer.cornerRadius = 4;
        past.layer.masksToBounds = YES;
        past.layer.borderWidth = 2;
        past.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
        
        NSNumber *paid =[NSNumber numberWithFloat:[flStrForObj(_pastCycle[indexPath.row][@"pay_amount"]) floatValue]];
        NSString *paidAmt = [formatter stringFromNumber:paid];
        past.paidAmt.text = paidAmt;
        
        NSNumber *earn =[NSNumber numberWithFloat:[flStrForObj(_pastCycle[indexPath.row][@"booking_earn"]) floatValue]];
        NSString *earnBal = [formatter stringFromNumber:earn];
        past.earnings.text = earnBal;
        
        past.noofbookings.text = [NSString stringWithFormat:@"%@",_pastCycle[indexPath.row][@"no_of_bookings"]];
        past.startTime.text = [NSString stringWithFormat:@"%@",_pastCycle[indexPath.row][@"start_date"]];
        past.endDate.text = [NSString stringWithFormat:@"%@",_pastCycle[indexPath.row][@"pay_date"]];
        past.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = past;
    }
    
    return cell;
}

- (IBAction)bankDetails:(id)sender {
    [self performSegueWithIdentifier:@"toBankDetails" sender:nil];
}

- (IBAction)liveChat:(id)sender {
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    LiveChatViewController  *chatVc = [mainstoryboard instantiateViewControllerWithIdentifier:@"liveChatVC"];
    
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:chatVc];
    navBar.navigationBar.translucent = NO;
    [self presentViewController:navBar animated:YES completion:nil];
}
@end
