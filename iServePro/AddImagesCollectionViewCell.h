//
//  AddImagesCollectionViewCell.h
//  iServePro
//
//  Created by -Tony Lu on 05/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddImagesCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *addImages;

@end
