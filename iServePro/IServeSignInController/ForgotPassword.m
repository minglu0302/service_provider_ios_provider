//
//  ForgotPassword.m
//  iServePro
//
//  Created by -Tony Lu on 01/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ForgotPassword.h"
#import "CountryPicker.h"
#import "CountryNameTableViewController.h"

@interface ForgotPassword ()<UIGestureRecognizerDelegate>

@end

@implementation ForgotPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavLeftButton];
    self.title = NSLocalizedString(@"RETRIEVE PASSWORD",@"RETRIEVE PASSWORD");
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x2598ED)};
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"OpenSans-regular" size:18.0],NSFontAttributeName,
      nil]];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissView)];
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
   // [_forgotPassword becomeFirstResponder];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"getMobileOtp"]) {
        _forgotpasswordDescrip.text =@"Enter your Phone number, and you will get instructions to reset your password";
        _forgotPassword.placeholder = @"Enter Mobile no..";
        _forgotPassword.keyboardType = UIKeyboardTypePhonePad;
    }else{
        _forgotpasswordDescrip.text =@"Enter your Email ID, and you will get instructions to reset your password";
        _forgotPassword.placeholder = @"Enter Email ID";
        _forgotPassword.keyboardType = UIKeyboardTypeEmailAddress;
    }
}
-(void)dismissView{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_off"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_on"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backToController
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)retrievePassword:(NSString *)text
{
    NSString *forgotString;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"getMobileOtp"]) {
        forgotString = [NSString stringWithFormat:@"%@",_forgotPassword.text];
        [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..",@"Loading..")];
        NSDictionary *dict =@{
                              @"ent_mobile":forgotString,
                              @"ent_user_type":@"1",
                              @"ent_date_time":[Helper getCurrentDateTime]
                              };
        NetworkHandler *handler =[NetworkHandler sharedInstance];
        [handler composeRequestWithMethod:@"ForgotPasswordWithOtp"
                                  paramas:dict
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
                                 if (succeeded) {
                                     [[NSUserDefaults standardUserDefaults] setObject:forgotString forKey:@"mobileForOtp"];
                                     [[NSUserDefaults standardUserDefaults] setObject:response[@"code"] forKey:@"OTP"];
                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                     [self retrievePasswordResponse:response];
                                 }
                             }];
    }else{
        forgotString = _forgotPassword.text;
        [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..",@"Loading..")];
        NSDictionary *dict =@{
                              @"ent_email":forgotString,
                              @"ent_user_type":@"1"
                              };
        NetworkHandler *handler =[NetworkHandler sharedInstance];
        [handler composeRequestWithMethod:@"forgotPassword"
                                  paramas:dict
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
                                 if (succeeded) {
                                     [self retrievePasswordResponse:response];
                                 }
                             }];
    }
}

- (IBAction)submitPassword:(id)sender {
    [self.view endEditing:YES];
    NSLog(@"Email Name: %@", _forgotPassword.text);
    if (_forgotPassword.text.length) {
 
        if (((unsigned long)_forgotPassword.text.length ==0))
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Invalid Email ID",@"Invalid Email ID") Message:NSLocalizedString(@"Re enter your email ID",@"Re enter your email ID")];
        }
        else
        {
            [self retrievePassword:_forgotPassword.text];
        }
    }else{
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"Enter Email ID (or) Mobile Number",@"Enter Email ID (or) Mobile Number")];
    }
}


-(void)retrievePasswordResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error",@"Error") Message:[response objectForKey:@"Error"]];
        _forgotPassword.text =@"";
        
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] intValue] == 0)
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[response objectForKey:@"errMsg"]];
           if ([[NSUserDefaults standardUserDefaults] boolForKey:@"getMobileOtp"]) {
                [[NSUserDefaults standardUserDefaults] setObject:_forgotPassword.text forKey:@"mobileNO"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:@"toVerification" sender:nil];
            }else{
            [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[response objectForKey:@"errMsg"]];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self submitPassword:nil];
    return YES;
}


- (IBAction)getCountryCode:(id)sender {
    
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString  *code, UIImage *flagimg, NSString *countryName)
    {
        NSString *countryCode = [NSString stringWithFormat:@"+%@", code];
        self.countryCode.text = countryCode;
    };
    [self presentViewController:navBar animated:YES completion:nil];

}
@end
