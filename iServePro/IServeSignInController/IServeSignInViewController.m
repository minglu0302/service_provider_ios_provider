//
//  SignInViewController.m
//  IservePro
//
//  Created by -Tony Lu on 13/02/16.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import "IServeSignInViewController.h"
#import "ForgotPassword.h"
#import "HomeTabBarController.h"
#import <sys/sysctl.h>
@interface IServeSignInViewController ()<CLLocationManagerDelegate,UIActionSheetDelegate>
@property(nonatomic,assign)double currentLatitude;
@property(nonatomic,assign)double currentLongitude;
@property(nonatomic,assign)NSString *platform;

@property(nonatomic,strong)CLLocationManager *locationManager;
@end

@implementation IServeSignInViewController

@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize emailImageView;
@synthesize loginBlock;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
-(void)getCurrentLocation{
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
        }
        [_locationManager startUpdatingLocation];
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Service",@"Location Service") message:NSLocalizedString(@"Unable to find your location,Please enable location services.",@"Unable to find your location,Please enable location services.") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        [alertView show];
        
    }
}

#pragma mark UIViewLifeCycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton           = NO;
    self.navigationController.navigationBar.topItem.title =NSLocalizedString(@"LOGIN",@"LOGIN");
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x2598ED)};
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"OpenSans-regular" size:18.0],NSFontAttributeName,
      nil]];
    [self createNavLeftButton];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"]){
        _rememberMe.selected = YES;
    }else{
         _rememberMe.selected = NO;
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    _currentLongitude = 0.0;
    _currentLatitude = 0.0;
        
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
//        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }

}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewDidAppear:(BOOL)animated{
    emailTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"emailORphone"];
    passwordTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    if (emailTextField.text.length > 0 && passwordTextField.text.length > 0) {
        [self.view endEditing:YES];
       // [passwordTextField becomeFirstResponder];
    }else{
         [emailTextField becomeFirstResponder];
    }
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton           = NO;
    [self getCurrentLocation];
}

-(void)viewWillAppear:(BOOL)animated{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    _platform =[self platformType:platform];
    NSLog(@"iPhone Device%@",[self platformType:platform]);

}
- (NSString *) platformType:(NSString *)platform
{
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}


-(void)dismissKeyboard
{
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)signInUser
{
    NSString *email = emailTextField.text;
    NSString *password = passwordTextField.text;
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"] && email.length >0 && password.length > 0) {
        [ud setObject:email forKey:@"emailORphone"];
        [ud setObject:password forKey:@"password"];
        [ud synchronize];
    }
  
    if((unsigned long)email.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"Enter Email ID",@"Enter Email ID")];
        [emailTextField becomeFirstResponder];
    }
    
    else if((unsigned long)password.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"Enter Password",@"Enter Password")];
        [passwordTextField becomeFirstResponder];
    }
    else if([self emailValidationCheck:email] == 0)
    {
        //email is not valid
        [Helper showAlertWithTitle:@"Message" Message:@"Invalid Email ID"];
        emailTextField.text = @"";
        [emailTextField becomeFirstResponder];
        
    }
    else
    {
        [self.view endEditing:YES];
        checkLoginCredentials = YES;
        [self sendServiceForLogin];
        
    }
    
}

- (void)sendServiceForLogin
{
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Signing in...",@"Signing in...")];
    
    NSString * pushToken;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken])
    {
        pushToken =[[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken];
    }
    else
    {
        pushToken =@"dgfhfghr765998ghghj";
    }
    
    NSString * fcmToken;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"FcmPushToken"])
    {
        fcmToken =[[NSUserDefaults standardUserDefaults]objectForKey:@"FcmPushToken"];
    }
    else
    {
        fcmToken =@"dgfhfghr765998ghghj";
    }

    
     NSDictionary *queryParams;
    queryParams =@{
                   kSMPLoginEmail       :emailTextField.text,
                   kSMPLoginPassword    :passwordTextField.text,
                   kSMPLoginDevideId    :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                   kSMPLoginDeviceType  :@"1",
                   kSMPSignUpLattitude  :[NSNumber numberWithDouble:_currentLatitude],
                   kSMPSignUpLongitude  :[NSNumber numberWithDouble:_currentLongitude],
                   kSMPCommonUpDateTime :[Helper getCurrentDateTime],
                   @"ent_push_token"    :fcmToken,
                   @"ent_app_version"   :[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                   @"ent_manf":@"APPLE",
                   @"ent_dev_model":_platform,
                   @"ent_dev_os":[[UIDevice currentDevice] systemVersion],
                   };
    
    NSLog(@" signin Parameters %@",queryParams);
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:MethodPatientLogin
                              paramas:queryParams
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (succeeded) { //handle success response
                                 
                                 [self loginResponse:response];
                             }
                             else{
                                 NSLog(@"Error");
                             }
                         }];
}

- (void)loginResponse:(NSDictionary *)response{
    
    //  NSLog(@"response:%@",response);
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    if (!response)
    {
        
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
        [alertView show];
        
    }else if (response[@"error"]){
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"error",@"error") Message:[response objectForKey:@"error"]];
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"error",@"error") Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:response[@"token"] forKey:KDAcheckUserSessionToken];
            [ud setObject:response[@"chn"] forKey:kNSURoadyoPubNubChannelkey];
            [ud setObject:response[@"subsChn"] forKey:kNSUDoctorSubscribeChanelKey];
            [ud setObject:response[@"email"] forKey:kNSUDoctorEmailAddressKey];
            [ud setObject:response[@"profilePic"] forKey:kNSUDoctorProfilePicKey];
            [ud setObject:response[@"fName"] forKey: @"dDoctorName"];
            [ud setObject:response[@"typeId"] forKey:@"doctorType"];
            [ud setObject:response[@"phone"] forKey:@"doctorPhone"];
            [ud setObject:response[@"serverChn"] forKey:kNSUDoctorServerChanelKey];
            [ud setObject:response[@"proid"] forKey:@"ProviderId"];
            [ud setObject:response[@"fees_group"] forKey:@"category"];
            [ud setObject:@"0" forKey:@"signing"];
            [ud setObject:response[@"proid"] forKey:@"proid"];
            [ud setBool:YES forKey:@"LoggedIn"];
            [ud setObject:response[@"email"] forKey:@"UserEmailID"];
            [ud setObject:self.passwordTextField.text forKey:@"UserPassword"];
            [ud setValue:@"4" forKey:@"IserveStatus"];
            [ud removeObjectForKey:@"selectedAddress"];
            [ud removeObjectForKey:@"AddressId"];
            [ud synchronize];
            
            if (self.loginBlock) {
                
                [self dismissViewControllerAnimated:YES completion:^{
                    self.loginBlock(YES);
                }];
            }
            else {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                            @"Main" bundle:[NSBundle mainBundle]];
                HomeTabBarController  *viewcontroller = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontroller, nil];
            }
        }
        else
        {
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:response[@"errMsg"]];
        }
    }
}

#pragma mark - TextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == emailTextField)
    {
        [passwordTextField becomeFirstResponder];
    }
    else {
        [passwordTextField resignFirstResponder];
        [self signInButtonClicked:nil];
    }
    return YES;
}

- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select the option to Retrieve Password"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Mobile Number", @"Email ID", nil];
    [actionSheet showInView:self.view];
    [self.view endEditing:YES];
    
}

#pragma mark - UIImagePickerDelegate -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"getMobileOtp"];
            [self forgotPassword];
            //from mobile
            break;
        }
        case 1:
        {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"getMobileOtp"];
            [self forgotPassword];
            // from email
            break;
        }
        default:
            break;
    }
}

-(void)forgotPassword
{
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    ForgotPassword  *forgotPassword = [mainstoryboard instantiateViewControllerWithIdentifier:@"password"];
    UINavigationController *navBar1=[[UINavigationController alloc]initWithRootViewController:forgotPassword];
    [self presentViewController:navBar1 animated:YES completion:nil];
}

- (IBAction)signInButtonClicked:(id)sender
{
    [self signInUser];
}

- (void) forgotPaswordAlertviewTextField
{
    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
                                        initWithTitle:NSLocalizedString(@"Invalid Email ID",@"Invalid Email ID")
                                        message:NSLocalizedString(@"Reenter your email ID ",@"Reenter your email ID ")
                                        delegate:self
                                        cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")
                                        otherButtonTitles:NSLocalizedString(@"Submit",@"Submit"), nil];
    forgotPasswordAlert.tag = 1;
    UITextField *emailForgot = [[UITextField alloc]initWithFrame:CGRectMake(30, 100, 225, 30)];
    
    
    [forgotPasswordAlert addSubview:emailForgot];
    
    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [forgotPasswordAlert show];
}

#pragma mark UIAlertView Delegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 1)
    {
        UITextField *forgotEmailtext = [alertView textFieldAtIndex:0];
        NSLog(@"Email Name: %@", forgotEmailtext.text);
        
        if (((unsigned long)forgotEmailtext.text.length ==0) || [Helper emailValidationCheck:forgotEmailtext.text] == 0)
        {
            [self forgotPaswordAlertviewTextField];
        }
        else
        {
            [self retrievePassword:forgotEmailtext.text];
        }
    }
    else
    {
        NSLog(@"cancel");
        passwordTextField.text = @"";
    }
}

- (void)retrievePassword:(NSString *)text
{
[[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...",@"Loading...")];
    
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    
    NSString *parameters = [NSString stringWithFormat:@"ent_email=%@&ent_user_type=%@",text,@"1"];
    
    NSString *removeSpaceFromParameter=[Helper removeWhiteSpaceFromURL:parameters];
    NSLog(@"request provider around you :%@",parameters);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@forgotPassword",BASE_URL]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:[[removeSpaceFromParameter stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]
                             dataUsingEncoding:NSUTF8StringEncoding
                             allowLossyConversion:YES]];
    
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(retrievePasswordResponse:)];
}


-(void)retrievePasswordResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error",@"Error") Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[dictResponse objectForKey:@"errMsg"]];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
}

- (BOOL) emailValidationCheck: (NSString *) emailToValidate{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    
    [_locationManager stopUpdatingLocation];
    _currentLatitude = newLocation.coordinate.latitude;
    _currentLongitude = newLocation.coordinate.longitude;
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
}

/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_off"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_on"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backToController
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)rememberAction:(id)sender {
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    if (!_rememberMe.selected) {
        _rememberMe.selected = YES;
        [ud setBool:YES forKey:@"isRemember"];
        _isRemembered = YES;
        if (_isRemembered && emailTextField.text.length >0 && passwordTextField.text.length > 0) {
            [ud setObject:emailTextField.text forKey:@"emailORphone"];
            [ud setObject:passwordTextField.text forKey:@"password"];
            [ud synchronize];
        }
    }else{
        [ud setBool:NO forKey:@"isRemember"];
        [ud removeObjectForKey:@"emailORphone"];
        [ud removeObjectForKey:@"password"];
        _rememberMe.selected = NO;
        _isRemembered = NO;
    }
}

@end
