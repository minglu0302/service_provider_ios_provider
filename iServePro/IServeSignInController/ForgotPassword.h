//
//  ForgotPassword.h
//  iServePro
//
//  Created by -Tony Lu on 01/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassword : UIViewController
- (IBAction)submitPassword:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *forgotPassword;

@property (strong, nonatomic) IBOutlet UILabel *countryCode;
- (IBAction)getCountryCode:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *forgotpasswordDescrip;

@end
