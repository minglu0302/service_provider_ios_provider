    //
//  InviteViewController.h
//  iServePro
//
//  Created by -Tony Lu on 02/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteViewController : UIViewController
- (IBAction)buttonFB:(id)sender;
- (IBAction)buttonTweet:(id)sender;
- (IBAction)buttonMsg:(id)sender;
- (IBAction)buttonEmail:(id)sender;

@end
