
//  HomeTabBarController.m
//  iServePro
//
//  Created by -Tony Lu on 12/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.


#import "HomeTabBarController.h"
#import "ChatSocketIOClient.h"
#import "iServeSplashController.h"
#import "LocationTracker.h"

@interface HomeTabBarController ()
{
    ChatSocketIOClient *socket;

}
@property (strong, nonatomic) UIButton *onTheJobOffTheJobButton;
@property (strong, nonatomic) NSString *status;
@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;
@end

@implementation HomeTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!socket) {
        socket  =[ChatSocketIOClient sharedInstance];
    }
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self tabbarImages];
}

-(void)tabbarImages
{
    NSString *homeunselect;
    NSString *homeselect;
    NSString *historyunselect;
    NSString *historyselect;
    NSString *scheduleunselect;
    NSString *scheduleselect;
    NSString *earnunselect;
    NSString *earnselect;
    NSString *proilfeunselect;
    NSString *profileselect;
    
    
    UITabBar *tabBar = self.tabBar;
    
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
    
    if ([UIScreen mainScreen].bounds.size.height <= 568) {
        homeunselect = @"provider_popup_home_btn";
        homeselect = @"provider_popup_home_btn_selector";
        
        historyunselect = @"provider_popup_history_btn";
        historyselect = @"provider_popup_history_btn_selector";
        
        scheduleunselect = @"provider_popup_schedule_btn";
        scheduleselect = @"provider_popup_selector_btn_selector";
        
        earnunselect = @"provider_popup_earnings_btn";
        earnselect = @"provider_popup_earnings_btn_selector";
        
        proilfeunselect = @"provider_popup_profile_btn";
        profileselect = @"provider_popup_profile_btn_selector";
    }else if ([UIScreen mainScreen].bounds.size.height == 667){
        homeunselect = @"6provider_popup_home_btn";
        homeselect = @"6provider_popup_home_btn_selector";
        
        historyunselect = @"6provider_popup_history_btn";
        historyselect = @"6provider_popup_history_btn_selector";
        
        scheduleunselect = @"6provider_popup_schedule_btn";
        scheduleselect = @"6provider_popup_selector_btn_selector";
        
        earnunselect = @"6provider_popup_earnings_btn";
        earnselect = @"6provider_popup_earnings_btn_selector";
        
        proilfeunselect = @"6provider_popup_profile_btn";
        profileselect = @"6provider_popup_profile_btn_selector";
    }else if ([UIScreen mainScreen].bounds.size.height >= 736){
        homeunselect = @"6pprovider_popup_home_btn";
        homeselect = @"6pprovider_popup_home_btn_selector";
        
        historyunselect = @"6pprovider_popup_history_btn";
        historyselect = @"6pprovider_popup_history_btn_selector";
        
        scheduleunselect = @"6pprovider_popup_schedule_btn";
        scheduleselect = @"6pprovider_popup_selector_btn_selector";
        
        earnunselect = @"6pprovider_popup_earnings_btn";
        earnselect = @"6pprovider_popup_earnings_btn_selector";
        
        proilfeunselect = @"6pprovider_popup_profile_btn";
        profileselect = @"6pprovider_popup_profile_btn_selector";
    }
    
     if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
         
         CGFloat tabFrame = self.tabBar.frame.size.width / 5;
         //NSLog(@"tabbar item width: %.2f", tabFrame);
         [tabBar setItemWidth:tabFrame];
         
         homeunselect = @"6provider_popup_home_btn";
         homeselect = @"6provider_popup_home_btn_selector";
         
         historyunselect = @"6provider_popup_history_btn";
         historyselect = @"6provider_popup_history_btn_selector";
         
         scheduleunselect = @"6provider_popup_schedule_btn";
         scheduleselect = @"6provider_popup_selector_btn_selector";
         
         earnunselect = @"6provider_popup_earnings_btn";
         earnselect = @"6provider_popup_earnings_btn_selector";
         
         proilfeunselect = @"6provider_popup_profile_btn";
         profileselect = @"6provider_popup_profile_btn_selector";
     }
//    [tabBar setItemSpacing:0];
//
//    CGRect tabFrame = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
//    tabFrame.size.height = 80;
//    tabFrame.origin.y = self.view.frame.size.height - 80;
//
//    [tabBar setFrame:tabFrame];
    
    tabBarItem1.selectedImage = [[UIImage imageNamed:homeselect] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem1.image = [[UIImage imageNamed:homeunselect] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem2.selectedImage = [[UIImage imageNamed:historyselect]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem2.image = [[UIImage imageNamed:historyunselect]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem3.selectedImage = [[UIImage imageNamed:scheduleselect]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem3.image = [[UIImage imageNamed:scheduleunselect]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem4.selectedImage = [[UIImage imageNamed:earnselect]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem4.image = [[UIImage imageNamed:earnunselect]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem5.selectedImage = [[UIImage imageNamed:profileselect]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem5.image = [[UIImage imageNamed:proilfeunselect]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"OnChatController"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    self.tabBarController.tabBar.hidden = NO;
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}
-(void)viewDidDisappear:(BOOL)animated{
}


@end
