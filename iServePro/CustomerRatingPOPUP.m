//
//  CustomerRatingPOPUP.m
//  iServePro
//
//  Created by -Tony Lu on 03/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "CustomerRatingPOPUP.h"
#import "CancelReasonTableViewCell.h"

@implementation CustomerRatingPOPUP

-(id)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"CustomerRating"
                                          owner:self
                                        options:nil] firstObject];
    [self.cancelTableView registerNib:[UINib nibWithNibName:@"CancelReason" bundle:nil] forCellReuseIdentifier:@"cancel"];
    return self;
}
-(void) onWindow:(UIWindow *)onWindow
{
    _selectedIndex = -1;
      _isSelected=NO;
    _cancelReasons = [[NSMutableArray alloc]init];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dimissView)];
    tapGesture.delegate = self;
    [self addGestureRecognizer:tapGesture];
    self.frame = onWindow.frame;
    [onWindow addSubview:self];
    
    _cancelTableView.allowsMultipleSelectionDuringEditing = YES;

    self.contentView.alpha = 1;
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.01, 0.01);
    
    [UIView animateWithDuration:0.4
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                     }
                     completion:^(BOOL finished) {
                     [self reasonForCancelBookingApi];
                     }];

}

-(void)reasonForCancelBookingApi{
   
    [[ProgressIndicator sharedInstance] showPIOnView:self withMessage:@"Loading.."];
    NSDictionary *dict = @{
                           @"ent_lan"      :@"0",
                           @"ent_user_type":@"1"
                           };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"getCancellationReson" paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 _cancelReasons = [response[@"data"] mutableCopy];
                                 
                                 [self.cancelTableView reloadData];
                                           [self.cancelTableView setEditing:YES animated:YES];
                                 self.heightOfContent.constant = self.cancelTableView.contentSize.height;
                                 [self layoutIfNeeded];
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 _cancelTableView.tintColor = UIColorFromRGB(0X2598ED);
                             }
                         }];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_contentView]) {
       // [_cancelTableView setEditing:YES animated:YES];
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}


-(void)dimissView{
    self.contentView.alpha = 1;
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    
    [UIView animateWithDuration:0.4
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
    
}

- (IBAction)submitTheRating:(id)sender{
    if (_selectedIndex == -1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"Please Select Any Reason",@"Please Select Any Reason")];
    }
    else{
        
        self.contentView.alpha = 1;
        self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
        
        [UIView animateWithDuration:0.4
                              delay:0.2
                            options: UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                         }
                         completion:^(BOOL finished) {
                             [_delegate popUpRatingOFfDismiss:_cancelReasons[_selectedIndex]];
                             [self removeFromSuperview];
                         }];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedIndex = -1;
    [[self.cancelTableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cancelReasons.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for ( NSIndexPath* selectedIndexPath in tableView.indexPathsForSelectedRows ) {
        if (selectedIndexPath.row == indexPath.row) {
        }else
            [tableView deselectRowAtIndexPath:selectedIndexPath animated:NO];
    }
    if (_selectedIndex == indexPath.row) {
        _selectedIndex = -1;
    }else{
        _selectedIndex = indexPath.row;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *kCellID = @"cancel";
    CancelReasonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    for(UIView * cellSubviews in cell.subviews)
    {
        cellSubviews.userInteractionEnabled = NO;
    }
    cell.reasonLabel.text = _cancelReasons[indexPath.row];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_cancelReasons.count == 3) {
        return 62;
    }else{
    return 46;
    }
}


@end
