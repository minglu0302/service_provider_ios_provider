//
//  IServeHomeViewController.h
//  iServePro
//
//  Created by -Tony Lu on 25/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeCell.h"

@interface IServeHomeViewController : UIViewController<HomeCellDelegate,UITableViewDelegate,UITableViewDataSource>
{
    
}
@property(nonatomic,assign)DriverStatus driverStatus;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(assign,nonatomic)int counter;
@property (strong, nonatomic) NSDictionary *dictAppointments;
@property (strong, nonatomic) NSString *status;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightOfContent;
@property (strong, nonatomic) IBOutlet UIButton *onTheJobOffTheJobButton;

- (IBAction)onOffJobButton:(id)sender;

- (IBAction)liveChat:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *liveChat;
@end
