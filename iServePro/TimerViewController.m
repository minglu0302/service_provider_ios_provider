//
//  TimerViewController.m
//  iServePro
//
//  Created by -Tony Lu on 06/07/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "TimerViewController.h"
#import "CustomSliderView.h"
#import "InvoiceViewController.h"
#import "ChatSIOClient.h"
#import "AppointmentDetailController.h"
#import "BookingViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AmazonTransfer.h"
#import "TimerVCCollectionViewCell.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "ImageViewCollection.h"
#import "Services1TableViewCell.h"
#import "iServeSplashController.h"
#import "ChatViewController.h"

@interface TimerViewController ()<CustomSliderViewDelegate,MFMessageComposeViewControllerDelegate>
{
    int timeSec, timeMin, timeHr;
    NSTimer *timerJobStarted;
    ImageViewCollection *collectionImages;
    CGRect screenSize;
}
@property (strong, nonatomic) CustomSliderView *customSliderView;
@property(nonatomic,assign) NSInteger bookingStatu;
@property(nonatomic,assign) NSInteger hours;
@property(nonatomic,assign) NSInteger minutes;

@end

@implementation TimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    screenSize = [[UIScreen mainScreen]bounds];
    [self.noJobPhotos setHidden:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    NSString *title =[NSString stringWithFormat:@"JOB ID:%@",_dictBookingDetails[@"bid"]];
    self.title =title;
    _pauseTimerLabel.layer.borderColor=UIColorFromRGB(0X2598ED).CGColor;
    _bookingStatu =[_dictBookingDetails[@"status"] integerValue];
    [self addCustomSlider];
    [self createNavLeftButton];
    [self updateCustomerinfo];
    
    NSDateFormatter *df = [self DateFormatterProper];
    
    NSDate *bookingDate = [df dateFromString:_dictBookingDetails[@"job_timer"]];
    
    NSTimeInterval timeDiff = [[NSDate date] timeIntervalSinceDate:bookingDate];
    
    
    if ([_dictBookingDetails[@"timer_status"]integerValue]==1) {
        
        timeHr=((int)timeDiff+[_dictBookingDetails[@"timer"] intValue])/3600;
        
        timeMin = fmod(((int)timeDiff+[_dictBookingDetails[@"timer"] intValue]), 3600)/60;
        timeSec = ((int)timeDiff+[_dictBookingDetails[@"timer"] intValue]) - ((timeHr*3600)+(timeMin*60));
        [self pauseTimer:nil];
    }else if ([_dictBookingDetails[@"timer_status"] integerValue]==2){
        
        timeHr=([_dictBookingDetails[@"timer"] intValue])/3600;
        timeMin = fmod(([_dictBookingDetails[@"timer"] intValue]), 3600)/60;
        timeSec = ([_dictBookingDetails[@"timer"] intValue]) - ((timeHr*3600)+(timeMin*60));
        
    }
    NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d:%02d",timeHr, timeMin,timeSec];
    _timerLabel.text = timeNow;
    
    if ( [_dictBookingDetails[@"services"] count] == 0) {
        self.heightOfServiceTableView.constant = 100;
        UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(self.serviceTableView.bounds.size.width/2,self.serviceTableView.bounds.size.height/2 , 120, 21)];
      [Helper setToLabel:messageLbl Text:@"No fixed price services were chosen by this customer.." WithFont:@"opensans" FSize:12 Color:[UIColor darkGrayColor]];
        messageLbl.textAlignment = NSTextAlignmentCenter;
        self.serviceTableView.backgroundView = messageLbl;
    }else{
        [self.serviceTableView reloadData];
        self.heightOfServiceTableView.constant = self.serviceTableView.contentSize.height;
    }
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }

}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (NSDateFormatter *)DateFormatterProper {
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return formatter;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidDisappear:(BOOL)animated{
    [timerJobStarted invalidate];
}

/**
 *  Adding the custom slider with slider text
 */
-(void)addCustomSlider {
    _customSliderView = [[[NSBundle mainBundle] loadNibNamed:@"NewSliderView" owner:self options:nil] lastObject];
    _customSliderView.delegate = self;
    _customSliderView.sliderTitle.text = NSLocalizedString(@"JOB STARTED",@"JOB STARTED");
    CGRect frame = _customSliderView.frame;
    frame.size.width = _sliderView.frame.size.width;
    frame.size.height = _sliderView.frame.size.height;
    _customSliderView.frame = frame;
    [_sliderView addSubview:_customSliderView];
    
}
/**
 *  changes the slider text 
 * according to the status of provider
 */

-(void)updateCustomerinfo{
    _custName.text=_dictBookingDetails[@"fname"];
    if ([_dictBookingDetails[@"customer_notes"]  isEqual:@""]) {
        _jobDetails.text=NSLocalizedString(@"No Notes Provided By The Customer",@"No Notes Provided By The Customer");
    }else{
        _jobDetails.text=_dictBookingDetails[@"customer_notes"];
    }
    
      _heightOfnotes.constant = [self measurenotes:_jobDetails];
    switch (_bookingStatu) {
        case 5:
            _customSliderView.sliderTitle.text  = NSLocalizedString(@"JOB STARTED",@"JOB STARTED");
            break;
        case 21:
            _customSliderView.sliderTitle.text  = NSLocalizedString(@"JOB STARTED",@"JOB STARTED");
            break;
        case 6:
            _customSliderView.sliderTitle.text  = NSLocalizedString(@"JOB COMPLETED",@"JOB COMPLETED");
        default:
            break;
    }
}
/**
 *  back arrow buttons
 */
-(void)createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/**
 *  moves to the home controller
 */
-(void)backButtonPressed {
    
    NSArray *vcs = self.navigationController.viewControllers;
    
    NSInteger count = [vcs count];
    
    if ([[vcs objectAtIndex:count-2] isKindOfClass:[BookingViewController class]]) {
        [self.navigationController popToViewController:[vcs objectAtIndex:0] animated:YES];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - Custom Slider Delegate

-(void)sliderAction
{
    [self onTHEWAYButtonTapped];
}

/**
 *  status updated to the server once provider
 *  has started job or completed job
 */
-(void)onTHEWAYButtonTapped
{
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..",@"Loading..")];
    
    switch (_bookingStatu) {
        case 2:
        case 5:
        case 21:{
            _bookingStatu = 6;
            
            break;
        }
        case 6:{
            _bookingStatu = 22;
            break;
        }
        default:
            break;
    }
    NSDictionary *parameters;
    if (_bookingStatu==6) {
        parameters = @{
                       kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                       kSMPCommonDevideId       :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                       @"ent_appnt_dt"          :self.dictBookingDetails[@"apntDt"],
                       KSMPPatientEmail         :self.dictBookingDetails[@"email"],
                       kSMPRespondResponse      :[NSString stringWithFormat:@"%ld",(long)_bookingStatu],
                       @"ent_date_time"         :[Helper getCurrentDateTime],
                       @"ent_bid"                :self.dictBookingDetails[@"bid"],
                       };
        
    }else{
        parameters = @{
                       kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                       kSMPCommonDevideId       :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                       @"ent_appnt_dt"          :self.dictBookingDetails[@"apntDt"],
                       KSMPPatientEmail         :self.dictBookingDetails[@"email"],
                       kSMPRespondResponse      :[NSString stringWithFormat:@"%ld",(long)_bookingStatu],
                       @"ent_date_time"         :[Helper getCurrentDateTime],
                       @"ent_bid"                :self.dictBookingDetails[@"bid"],
                       @"ent_timer":[NSString stringWithFormat:@"%d",(timeHr*3600)+(timeMin*60)+timeSec],
                       };
    }
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:MethodupdateApptStatus
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             [_customSliderView sliderImageOrigin];
                             if (succeeded) {
                                 if ([response[@"errFlag"] isEqualToString:@"1"]) {
                                     [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                     if([[response objectForKey:@"errNum"] intValue] == 41){
                                         [Helper showAlertWithTitle:NSLocalizedString(@"Error",@"Error") Message:[response objectForKey:@"errMsg"]];
                                         [self backButtonPressed];
                                     }else if ([[response objectForKey:@"errNum"] intValue] == 83){
                                         [self userSessionTokenExpire];
                                     }
                                 }else{
                                     
                                     [self emitTheBookingACk:_bookingStatu];
                                     [self updateCustomerinfo];
                                     if (_bookingStatu==22) {
                                         [self performSegueWithIdentifier:@"toInvoiceSegue" sender:_dictBookingDetails];
                                     }else if (_bookingStatu==6){
                                         [self pauseTimer:nil];
                                     }
                                 }
                             }
                             else
                             {
                                 NSLog(@"Error");
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             }
                         }];
}

/**
 *  if session expires this delegate method will called
 */
-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

/**
 *
 *  @param segue  to invoice controller
 *  @param sender dictionary data and minutes
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([[segue identifier] isEqualToString:@"toInvoiceSegue"])
    {
        InvoiceViewController *details =[segue destinationViewController];
        details.dictBookingDetails=sender;
        details.minutesCount=[NSString stringWithFormat:@"%d",(timeHr*3600)+(timeMin*60)+timeSec];
    }
    return;
}
/**
 *  emiting to the socket when provider updates the status
 *
 *  @param bstatus 6= job started, 22 = job completed and invoice raised
 */
-(void)emitTheBookingACk:(NSInteger)bstatus
{
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_dictBookingDetails[@"bid"],
                            @"bstatus":[NSNumber numberWithInteger:bstatus],
                            @"cid":_dictBookingDetails[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime]
                            };
    [socket publishToChannel:@"LiveBookingAck" message:message];
}

/**
 *  start or pause time action
 *
 *  @param sender once job started the action wills starts automatically
 */
- (IBAction)pauseTimer:(id)sender {
    if (_bookingStatu == 5 ||_bookingStatu==21||_bookingStatu==2) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please Start The Job",@"Please Start The Job")];
    }else{
        
        if(_pauseTimerLabel.selected)
        {
            _pauseTimerLabel.selected=NO;
            _timerTag=@"2";
            [self startTimer];
            
            
        }else{
            _pauseTimerLabel.selected=YES;
            _timerTag=@"1";
            [self startTimer];
            [self getTimerAPI];
        }
    }
    
}

/**
 *  message action
 *
 *  @param sender sending message to customer
 */
- (IBAction)messageAction:(id)sender {
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    ChatViewController  *chatVc = [mainstoryboard instantiateViewControllerWithIdentifier:@"chatVC"];
     chatVc.bid = _dictBookingDetails[@"bid"];
    chatVc.custPic = _dictBookingDetails[@"pPic"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:chatVc];
    
    [self presentViewController:navBar animated:YES completion:nil];

//    if ([MFMessageComposeViewController canSendText]) {
//        MFMessageComposeViewController *message = [[MFMessageComposeViewController alloc] init];
//        message.messageComposeDelegate = self;
//        
//        message.recipients=@[_dictBookingDetails[@"phone"]];
//        [[message navigationBar] setTintColor:[UIColor blackColor]];
//        message.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
//        
//        [message setBody:@""];
//        [self presentViewController:message animated:YES completion:nil];
//    }
}

/**
 *  call action
 *
 *  @param sender customer call
 */
- (IBAction)callAction:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Call", @"Call") message:[NSString stringWithFormat:@"%@ ",_dictBookingDetails[@"phone"]] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Call", @"Call"), nil];
    [alert show];
}

/**
 *  for phone call
 *
 *  @param alertVie  customer phone number
 *  @param buttonIndex 1 for call, 0 for cancel
 */
-(void)alertView:(UIAlertView *)alertVie didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1)
    {
        NSString *phoneNumber = _dictBookingDetails[@"phone"];
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}
/**
 *  timer start and pause
 */
-(void)startTimer {
    
    if (_pauseTimerLabel.selected) {
        timerJobStarted = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    }else{
        NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d:%02d",timeHr, timeMin,timeSec];
        _timerLabel.text = timeNow;
        [timerJobStarted invalidate];
          [self getTimerAPI];
    }
}

/**
 *  timer start from zero or from paused time
 *
 *  @param timer this is for provider that 
 * how much time he spent in work and calculation will be done according to this
 */
- (void)timerTick:(NSTimer *)timer
{
    
    timeSec++;
    if (timeSec == 60)
    {
        timeSec = 0;
        timeMin++;
    }
    if (timeMin == 60) {
        timeMin = 0;
        timeHr++;
    }
    
    NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d:%02d",timeHr, timeMin,timeSec];
    _timerLabel.text = timeNow;
  
}


-(void)getTimerAPI{
    
    NSDictionary *dict =@{
                          @"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id":[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                          @"ent_bid":flStrForObj(_dictBookingDetails[@"bid"]),
                          @"ent_timer":[NSString stringWithFormat:@"%d",(timeHr*3600)+(timeMin*60)+timeSec],
                          @"ent_date_time":[Helper getCurrentDateTime],
                          @"ent_timer_status":flStrForObj(_timerTag)
                          };
    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"UpdateApptTimer"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 //[Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 NSLog(@"Timer updated:%@",response);
                                 
                                 if ([_timerTag isEqual:@"1"]) {
                                     [self updateToServe:@"15"];
                                 }else if ([_timerTag isEqual:@"2"]){
                                     [self updateToServe:@"16"];
                                 }
                             }else{
                                 
                             }
                         }];
}


-(void)updateToServe:(NSString *)status{
    
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_dictBookingDetails[@"bid"],
                            @"bstatus":status,
                            @"cid":_dictBookingDetails[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime]
                            };
    [socket publishToChannel:@"LiveBookingAck" message:message];
}
#pragma uicollectionView delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([_dictBookingDetails[@"job_imgs"] integerValue]==0) {
        [self.noJobPhotos setHidden:NO];
    }
    return [_dictBookingDetails[@"job_imgs"] integerValue];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier =@"customerImages";
    TimerVCCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *strImageUrl = [NSString stringWithFormat:@"https://%@.s3.amazonaws.com/JobImages/%@_%ld.png",Bucket,_dictBookingDetails[@"bid"],(long)indexPath.row];
    
    [cell.activityIndicator startAnimating];
    
    
    [cell.custImages sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                             placeholderImage:[UIImage imageNamed:@"user_image_default"]
                                      options:SDWebImageCacheMemoryOnly
                                    completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                        [cell.activityIndicator stopAnimating];
                                    }];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    collectionImages= [ImageViewCollection sharedInstance];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [[NSUserDefaults standardUserDefaults] setObject:_dictBookingDetails[@"bid"] forKey:@"BID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSInteger profileTag =0;
    [collectionImages showPopUpWithDictionary:window jobImages:[_dictBookingDetails[@"job_imgs"] integerValue] index:indexPath tag:profileTag jobImages:[NSMutableArray new]];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(66, 66);
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"You cancelled sending message");
            break;
        case MessageComposeResultFailed:
            NSLog(@"Message failed");
            break;
        case MessageComposeResultSent:
            NSLog(@"Message sent");
            break;
            
        default:
            NSLog(@"An error occurred while composing this message");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark UITableview DataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return  [_dictBookingDetails[@"services"] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"services";
    Services1TableViewCell *cell;
    if (!cell) {
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    cell.serviceName.text = _dictBookingDetails[@"services"][indexPath.row][@"sname"];
    
    NSNumber *ServicePrice =[NSNumber numberWithInteger:[ _dictBookingDetails[@"services"][indexPath.row][@"sprice"] integerValue]];
    NSString *price = [formatter stringFromNumber:ServicePrice];
    cell.serviceValue.text = price;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView * labelview  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 25)];
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 6, 320, 22)];
    
    [labelview addSubview:labelHeader];
    
    if (section == 0) {
        [Helper setToLabel:labelHeader Text:@"SELECTED SERVICES" WithFont:@"opensans" FSize:11 Color:UIColorFromRGB(0xaaaaaa)];
        labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
    }
    return labelview;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view;
    
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,5)];
    view.backgroundColor = UIColorFromRGB(0xFFFFFF);
    
    return view;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"services";
    
    Services1TableViewCell *cell = (Services1TableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell =[[Services1TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.serviceName.text = [NSString stringWithFormat:@"%@", _dictBookingDetails[@"services"][indexPath.row][@"sname"]];
    
    float height =  [self measureHeiLabel:cell.serviceName];
    
    return 20+height;
}

#pragma mark - Custom Methods -

- (CGFloat)measureHeiLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-142 , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:flStrForObj(label.text)attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return  requiredHeight.size.height;
}


#pragma mark - Custom Methods -

- (CGFloat)measurenotes: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-60 , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:flStrForObj(label.text)attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return  requiredHeight.size.height + 20;
}


@end
