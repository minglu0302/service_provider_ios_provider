//
//  CitySelectTableViewCell.h
//  iServePro
//
//  Created by -Tony Lu on 26/04/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitySelectTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *selectedCity;
@property (strong, nonatomic) NSString *selectedCityID;

@end
