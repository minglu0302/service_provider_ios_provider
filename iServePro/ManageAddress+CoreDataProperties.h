//
//  ManageAddress+CoreDataProperties.h
//  Onthewaypro
//
//  Created by -Tony Lu on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ManageAddress.h"

NS_ASSUME_NONNULL_BEGIN

@interface ManageAddress (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *address;
@property (nullable, nonatomic, retain) NSString *flatno;
@property (nullable, nonatomic, retain) NSString *typeaddress;
@property (nullable, nonatomic, retain) NSString *latit;
@property (nullable, nonatomic, retain) NSString *longit;

@end

NS_ASSUME_NONNULL_END
