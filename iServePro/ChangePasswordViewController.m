//
//  ChangePasswordViewController.m
//  iServeProvider
//
//  Created by -Tony Lu on 21/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ChangePasswordViewController.h"
#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"  //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"  //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"  //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"  //Should contains one or more symbol
@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissView)];
    [self.view addGestureRecognizer:tapGesture];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
    
    [super viewDidLoad];
    _submit.layer.borderWidth = 2;
    _submit.layer.borderColor = UIColorFromRGB(0X2598ED).CGColor;
    self.title = @"Change Password";
    [self createNavLeftButton];
    
        
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}


-(void)viewDidAppear:(BOOL)animated{
       [_oldpasswordTF becomeFirstResponder];
}
-(void)dismissView{
    [self.view endEditing:YES];
}

/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_off"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_on"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backToController
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];

//    [self.navigationController popViewControllerAnimated:YES];
//    
//    [self.view endEditing:YES];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UITextFeildDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == _reenterPassword)
    {
        int strength = [self checkPasswordStrength:_newpasswordTF.text];
        if(strength == 0)
        {
            [_newpasswordTF becomeFirstResponder];
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _oldpasswordTF) {
        if (textField.text.length > 0) {
            
            if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"UserPassword"] isEqualToString:textField.text]) {
                [Helper showAlertWithTitle:@"Message" Message:@"Old password isn't correct"];
                [_oldpasswordTF becomeFirstResponder];
            }
        }
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _oldpasswordTF) {
        [_newpasswordTF becomeFirstResponder];
    }
    else if (textField == _newpasswordTF) {
        [_reenterPassword becomeFirstResponder];
    }
    else if(textField == _reenterPassword)
    {
        if ([_reenterPassword.text isEqualToString:_newpasswordTF.text]) {
            [self.view endEditing:YES];
        }else{
            [Helper showAlertWithTitle:@"Message" Message:@"password Mismatched"];
            [_reenterPassword becomeFirstResponder];
        }
    }
    return YES;
}
#pragma mark - Password Checking

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    //will contains password strength
    int strength = 0;
    
    if (len == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Please enter password first",@"Please enter password first") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        
        return 0;//PasswordStrengthTypeWeak;
    } else if (len <= 5) {
        strength++;
    } else if (len <= 10) {
        strength += 2;
    } else{
        strength += 3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Please enter atleast one Uppercase alphabet",@"Please enter atleast one Uppercase alphabet") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Please enter atleast one Lowercase alphabet",@"Please enter atleast one Lowercase alphabet") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Please enter atleast  one Number",@"Please enter atleast  one Number") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        
        return 0;
    }
    return 1;
}

- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    //NSLog(@"test range %ld",textRange);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = 0;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    
    return didValidate;
}


- (IBAction)submit:(id)sender {
    [self.view endEditing:YES];
    
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"UserPassword"] isEqualToString:_oldpasswordTF.text] && _oldpasswordTF.text.length>2)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Old password isn't correct"];
        [_reenterPassword becomeFirstResponder];
    }else if (_newpasswordTF.text.length > 0) {
        if ([_reenterPassword.text isEqualToString:_newpasswordTF.text]) {
            [self changeThePasswordWebservcie];
        }else{
            
            [Helper showAlertWithTitle:@"Message" Message:@"password Mismatched"];
            [_reenterPassword becomeFirstResponder];
            
        }
    }else{
        
        [Helper showAlertWithTitle:@"Message" Message:@"Enter the Password"];
        [_newpasswordTF becomeFirstResponder];
    }

}

-(void)changeThePasswordWebservcie{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"updating password.."];
    
    
    NSDictionary *dict = @{
                           @"ent_mobile" : [ud objectForKey:@"mobileNO"],
                           @"ent_pass":_newpasswordTF.text,
                           @"ent_user_type" : @"1"
                           };
    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"updatePasswordForUser"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded)
                             {
                                 if ([response[@"errFlag"] integerValue]==0) {
                                     
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                     [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                     [[NSUserDefaults standardUserDefaults] setObject:_newpasswordTF.text forKey:@"UserPassword"];
                                     [self backToController];
                                 }else{
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 }
                             }
                         }];
}

@end
