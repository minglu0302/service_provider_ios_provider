//
//  MobileVerficationViewController.m
//  Onthewaypro
//
//  Created by -Tony Lu on 01/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "MobileVerficationViewController.h"

@interface MobileVerficationViewController ()

@end

@implementation MobileVerficationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavLeftButton];
    self.title = @"CONFIRM OTP";
    [_verifyTextfield1 becomeFirstResponder];
    _verifyTextfield1.layer.borderWidth = 1.5;
    _verifyTextfield1.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
    _verifyTextfield2.layer.borderWidth = 1.5;
    _verifyTextfield2.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
    _verifyTextfield3.layer.borderWidth = 1.5;
    _verifyTextfield3.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
    _verifyTextfield4.layer.borderWidth = 1.5;
    _verifyTextfield4.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
    // Do any additional setup after loading the view.
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
    
    [self.view endEditing:YES];
    
}

/*---------------------------------*/
#pragma mark - Textfield Delegates
/*---------------------------------*/
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // Hide Keyboard when you enter last digit
    if ([textField isEqual:_verifyTextfield4] && ![string isEqualToString:@""]) {
        textField.text = string;
        [textField resignFirstResponder];
       [self checkVerificationCode];
    }
    
    // This allows numeric text only, but also backspace for deletes
    if (string.length > 0 && ![[NSScanner scannerWithString:string] scanInt:NULL])
        return NO;
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    // This 'tabs' to next field when entering digits
    if (newLength == 1) {
        if (textField == _verifyTextfield1)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_verifyTextfield2 afterDelay:0.1];
        }
        else if (textField == _verifyTextfield2)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_verifyTextfield3 afterDelay:0.1];
        }
        else if (textField == _verifyTextfield3)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_verifyTextfield4 afterDelay:0.1];
        }
          }
    //this goes to previous field as you backspace through them, so you don't have to tap into them individually
    else if (oldLength > 0 && newLength == 0) {
        if (textField == _verifyTextfield4)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_verifyTextfield3 afterDelay:0.1];
        }
        else if (textField == _verifyTextfield3)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_verifyTextfield2 afterDelay:0.1];
        }
        else if (textField == _verifyTextfield2)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_verifyTextfield1 afterDelay:0.1];
        }

    }
    return newLength <= 1;
}
- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)checkVerificationCode{
//   
//    NSString *mobileOtp = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"OTP"]];
//    
//    
//    if ([mobileOtp isEqualToString:otp]) {
//        [self performSegueWithIdentifier:@"toChangePassword" sender:nil];
//    }else{
//        [Helper showAlertWithTitle:@"Message" Message:@"Otp Entered is Incorrect"];
//    }
    [self verifytheMobileNumber];
    
}


-(void)verifytheMobileNumber{
    NSString *otp = [NSString stringWithFormat:@"%@%@%@%@",_verifyTextfield1.text,_verifyTextfield2.text,_verifyTextfield3.text,_verifyTextfield4.text];
    NSDictionary *dict =@{
                          @"ent_phone":[[NSUserDefaults standardUserDefaults] objectForKey:@"mobileForOtp"],
                          @"ent_code":otp,
                          @"ent_service_type":@"2",
                          @"ent_user_type":@"1"
                          };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"verifyPhone"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 if ([response[@"errFlag"] integerValue]==0) {
                                     [self performSegueWithIdentifier:@"toChangePassword" sender:nil];
                                 }else{
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 }
                             }
                         }];
}


- (IBAction)didntGetOTP:(id)sender {
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..",@"Loading..")];
    NSDictionary *dict =@{
                          @"ent_mobile":[[NSUserDefaults standardUserDefaults] objectForKey:@"mobileForOtp"],
                          @"ent_user_type":@"1",
                          @"ent_date_time":[Helper getCurrentDateTime]
                          };
    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"ForgotPasswordWithOtp"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                            
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 [Helper showAlertWithTitle:@"Message" Message:@"Otp sent to Registered Mobile number"];
                                 [_verifyTextfield1 becomeFirstResponder];
                                 _verifyTextfield1.text=@"";
                                  _verifyTextfield2.text=@"";
                                  _verifyTextfield3.text=@"";
                                  _verifyTextfield4.text=@"";
                             }
                         }];

}
@end
