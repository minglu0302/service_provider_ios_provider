//
//  profileTableViewCell.h
//  iServePro
//
//  Created by -Tony Lu on 29/04/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface profileTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *arrowImages;


@property (strong, nonatomic) IBOutlet UITextView *ContentTextView;
@end
