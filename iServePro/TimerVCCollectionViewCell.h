//
//  TimerVCCollectionViewCell.h
//  iServePro
//
//  Created by -Tony Lu on 12/07/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerVCCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *custImages;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
