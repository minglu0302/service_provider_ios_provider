//
//  CategoryDetailsUpdate.h
//  iServeProvider
//
//  Created by -Tony Lu on 18/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryDetailsUpdate : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (strong, nonatomic) IBOutlet UITextField *categoryNameTF;
@property (strong, nonatomic) IBOutlet UILabel *currencyLabel;
@property (strong, nonatomic) IBOutlet UITextField *curencyTF;
@property (strong, nonatomic) IBOutlet UITextField *priceMinTF;

@property (strong, nonatomic) IBOutlet UILabel *pricePerMinLabel;
@property (strong, nonatomic) IBOutlet UITextField *visitFeeTF;
@property (strong, nonatomic) IBOutlet UILabel *radiusLabel;
@property (strong, nonatomic) IBOutlet UITextField *radiusTF;
@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *radius;
@property (strong, nonatomic) NSDictionary *dictCategory;
@end
