//
//  PaymentTableViewCell.h
//  iServePro
//
//  Created by -Tony Lu on 14/07/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *avgRatings;
@property (strong, nonatomic) IBOutlet UILabel *acceptRating;

@property (strong, nonatomic) IBOutlet UILabel *noOfbookings;
@property (strong, nonatomic) IBOutlet UILabel *earinings;
@property (strong, nonatomic) IBOutlet UILabel *openbal;
@property (strong, nonatomic) IBOutlet UILabel *closedbal;
@property (strong, nonatomic) IBOutlet UILabel *startTime;
@property (strong, nonatomic) IBOutlet UILabel *entTime;
@property (strong, nonatomic) IBOutlet UILabel *acceptedBooking;

@property (strong, nonatomic) IBOutlet UILabel *rejectedbooking;
@property (strong, nonatomic) IBOutlet UILabel *ignoreBooking;
@property (strong, nonatomic) IBOutlet UILabel *totAcceptBookings;
@property (strong, nonatomic) IBOutlet UILabel *cancelledBookings;

@end
