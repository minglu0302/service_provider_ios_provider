//
//  ViewController.h
//
//  Created by Apple on 23/12/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Inputbar.h"
#import "Chat.h"

@interface ChatViewController : UIViewController

//@property (weak, nonatomic) IBOutlet Inputbar *inputbar;

@property (weak, nonatomic) IBOutlet UIButton *navigationBackButton;
@property (weak, nonatomic) IBOutlet UITableView *chattingTableView;
@property (weak, nonatomic) IBOutlet UIImageView *providerImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;


@property (strong, nonatomic) Chat *chat;
@property (strong, nonatomic) Inputbar *inputbar;
@property (strong, nonatomic) NSDictionary *providerDetails;
@property NSInteger messageId;
-(void)receivedMsgFromPsuh:(NSDictionary *)dict;


- (IBAction)userDidTapScreen:(id)sender;
- (IBAction)navigationBackButtonAction:(id)sender;

@property (strong,  nonatomic) NSString *bid;
@property (strong,  nonatomic) NSString *custPic;

@end

