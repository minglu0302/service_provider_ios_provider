//
//  Message.m
//  Whatsapp
//
//  Created by Rafael Castro on 6/16/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "Message.h"

@implementation Message

-(id)init
{
    self = [super init];
    if (self)
    {
//        self.sender = MessageSenderMyself;
//        self.status = MessageStatusSending;
//        self.text = @"";
        self.heigh = 44;
//        self.date = [NSDate dateWithTimeIntervalSinceNow:-60*60*24];
//        self.identifier = @"";
    }
    return self;
}

+(Message *)messageFromDictionary:(NSDictionary *)dictionary andIdentifier:(NSString *)bid
{
    Message *message = [[Message alloc] init];
    message.text = dictionary[@"payload"];
    message.identifier = dictionary[@"msgid"];
    
    if([dictionary[@"usertype"] integerValue] != 2)
    {
        message.sender = MessageSenderMyself;
        message.status = MessageStatusSent;
    }
    else
    {
        message.sender = MessageSenderSomeone;
        message.status = MessageStatusReceived;
    }
    
    message.chat_id = bid;
    message.identifier = @"";

    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:dictionary[@"dt"]];

    message.date = date;
    
    return message;
}
@end
