//
//  ViewController.m
//  WhatsappChat
//
//  Created by Apple on 23/12/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "ChatViewController.h"
#import "MessageCell.h"
#import "TableArray.h"
#import "MessageGateway.h"
#import "Inputbar.h"
#import "DAKeyboardControl.h"
#import "LocalStorage.h"
#import "ChatSocketIOClient.h"
#import "UINavigationItem+VNHNavigationItem.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface ChatViewController ()<InputbarDelegate,MessageGatewayDelegate,
UITableViewDataSource,UITableViewDelegate,ChatSocketIODelegetes>
{
    ChatSocketIOClient *socketWrapper;
    Contact *contact;
    ChatSIOClient *socket;
    BOOL isKeyboardOpened;
    NSInteger keyboardHeight;
}

@property (strong, nonatomic) TableArray *tableArray;
@property (strong, nonatomic) MessageGateway *gateway;

@end

@implementation ChatViewController

#pragma mark - Initial Methods -

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self setInitialProperties];
    if (!socket || !socketWrapper) {
        socket = [ChatSIOClient sharedInstance];
        socketWrapper = [ChatSocketIOClient sharedInstance];
    }
  
    socketWrapper.chatDelegate = self;
    //    [self setInputbar];
    [self.navigationItem addLeftSpace];
    self.title = [NSString stringWithFormat:@"JOB ID:%@",_bid];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"OnChatController"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
        if(!self.inputbar)
        {
            self.inputbar = [[Inputbar alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height - 84,  self.view.bounds.size.width, 44)];
            [self setInputbar];
            [self.view addSubview:self.inputbar];
        }
    }else{
        if(!self.inputbar)
        {
            self.inputbar = [[Inputbar alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height - 44,  self.view.bounds.size.width, 44)];
            [self setInputbar];
            [self.view addSubview:self.inputbar];
        }
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    // Do any additional setup after loading the view, typically from a nib.
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    
    //your other code here..........
}

-(void)setInitialProperties
{
    [self setInitialDetails];
    [self setTableView];
    [self setGateway];
    [self getAllMessagesAccordingToBid];
}
-(void)viewWillAppear:(BOOL)animated{
    [socketWrapper connectSocket];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    __weak Inputbar *inputbar = _inputbar;
    __weak UITableView *tableView = self.chattingTableView;
    __weak ChatViewController *controller = self;
    __weak UIView *view = self.view;
    
    self.view.keyboardTriggerOffset = inputbar.frame.size.height;
    [self.view addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
        /*
         Try not to call "self" inside this block (retain cycle).
         But if you do, make sure to remove DAKeyboardControl
         when you are done with the view controller by calling:
         [self.view removeKeyboardControl];
         */
        if(opening)
        {
            CGRect toolBarFrame = inputbar.frame;
            toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
            inputbar.frame = toolBarFrame;
            
            isKeyboardOpened = YES;
            CGRect inputViewFrameInView = [view convertRect:keyboardFrameInView fromView:nil];
            CGRect intersection = CGRectIntersection(tableView.frame, inputViewFrameInView);
            UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height+44, 0.0);
            tableView.scrollIndicatorInsets = ei;
            tableView.contentInset = ei;
            [controller tableViewScrollToBottomAnimated:YES];
            
        }
        else if (closing)
        {
            isKeyboardOpened = NO;
            inputbar.frame = CGRectMake(0, view.bounds.size.height - 44,  view.bounds.size.width, 44);
            UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
            [UIView animateWithDuration:0.2 animations:^{
                tableView.scrollIndicatorInsets = ei;
                tableView.contentInset = ei;
            }];
            
            
            [controller tableViewScrollToBottomAnimated:YES];
            
        }
        
        
    }];
    
}

-(void)getAllMessagesAccordingToBid{
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:NSLocalizedString(@"Loading..",@"Loading..")];
    NSDictionary *dict = @{
                           kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                           kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                           @"ent_bid":_bid,
                           @"ent_user_type":@"1"
                           };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"getallMessages"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [self parseOldChatMessages:response[@"messages"]];
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 
                             }
                         }];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"OnChatController"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self.view endEditing:YES];
    [self.view removeKeyboardControl];
    [self.gateway dismiss];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[LocalStorage sharedInstance]removeObjectOfClass];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Methods -

-(void)setInputbar
{
    self.inputbar.placeholder = @"Type a message";
    self.inputbar.delegatee = self;
    self.inputbar.leftButtonImage = [UIImage imageNamed:@"share"];
    self.inputbar.rightButtonText = @"Send";
    self.inputbar.rightButtonTextColor = [UIColor colorWithRed:0 green:124/255.0 blue:1 alpha:1];
}

-(void)setTableView
{
    self.tableArray = [[TableArray alloc] init];
    
    self.chattingTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,self.view.frame.size.width, 10.0f)];
    
    [self.chattingTableView registerClass:[MessageCell class] forCellReuseIdentifier: @"MessageCell"];
}

-(void)setGateway
{
    self.gateway = [MessageGateway sharedInstance];
    self.gateway.delegate = self;
    self.gateway.chat = self.chat;
    [self.gateway loadOldMessages];
}

-(void)setChat:(Chat *)chat
{
    _chat = chat;
    //    self.title = chat.contact.name;
}

-(void)setInitialDetails
{
    contact = [[Contact alloc] init];
    //    self.title = self.providerDetails[@"name"];
    contact.name = self.providerDetails[@"name"];
    contact.identifier = _bid;//@"12345";//self.providerDetails[@"pid"];
    
    self.chat = [[Chat alloc] init];
    self.chat.contact = contact;
    
    [self.activityIndicator startAnimating];
    [self.providerImageView sd_setImageWithURL:[NSURL URLWithString:_custPic]
                              placeholderImage:[UIImage imageNamed:@"user_image_default"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                         [self.activityIndicator stopAnimating];
                                     }];
    
    
    
    
}


#pragma mark - UIButton Actions -

- (IBAction)userDidTapScreen:(id)sender
{
    [self.inputbar resignFirstResponder];
}

- (IBAction)navigationBackButtonAction:(id)sender
{
    [self.inputbar resignFirstResponder];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - UITableView DataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.tableArray numberOfSections];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableArray numberOfMessagesInSection:section];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MessageCell";
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.message = [self.tableArray objectAtIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Message *message = [self.tableArray objectAtIndexPath:indexPath];
    return message.heigh;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.tableArray titleForSection:section];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 40);
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor clearColor];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:Helvitica_Medium size:20.0];
    [label sizeToFit];
    label.center = view.center;
    label.font = [UIFont fontWithName:Helvitica_Medium size:13.0];
    label.backgroundColor = [UIColor colorWithRed:207/255.0 green:220/255.0 blue:252.0/255.0 alpha:1];
    label.layer.cornerRadius = 10;
    label.layer.masksToBounds = YES;
    label.autoresizingMask = UIViewAutoresizingNone;
    [view addSubview:label];
    
    return view;
}
- (void)tableViewScrollToBottomAnimated:(BOOL)animated
{
    
    NSInteger numberOfSections = [self.tableArray numberOfSections];
    NSInteger numberOfRows = [self.tableArray numberOfMessagesInSection:numberOfSections-1];
    if (numberOfRows)
    {
        //   dispatch_async(dispatch_get_main_queue(),^{
        [self.chattingTableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                      atScrollPosition:UITableViewScrollPositionBottom animated:animated];
        //   });
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Inputbar Delegate -

-(void)inputbarDidPressRightButton:(Inputbar *)inputbar
{
    
    //Insert Message in UI
    
    Message *message = [[Message alloc] init];
    message.text = inputbar.text;
    message.date = [NSDate date];
    message.chat_id = _chat.identifier;
    
    //Store Message in memory
    //    [self.tableArray addObject:message];
    
    //Insert Message in UI
    
    // dispatch_async(dispatch_get_main_queue(),^{
    //    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
    //    [self.chattingTableView beginUpdates];
    //
    //    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
    //        [self.chattingTableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
    //                              withRowAnimation:UITableViewRowAnimationNone];
    //
    //    [self.chattingTableView insertRowsAtIndexPaths:@[indexPath]
    //                                  withRowAnimation:UITableViewRowAnimationBottom];
    //
    //
    //    [self.chattingTableView endUpdates];
    //    [self tableViewScrollToBottomAnimated:YES];
    //   });
    
    //    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [appD playNotificationSound];
    
    
    
    [self updateTableWhenRecieveMessageFromProvider:message];
    
    //Send message to server
    [self.gateway sendMessage:message];
    
    self.messageId = self.messageId + 1;
    
    NSDictionary *params = @{
                             @"msgtype":@"0",
                             @"bid":_bid,
                             @"payload":message.text,
                             @"msgid":[NSNumber numberWithInteger:self.messageId],
                             @"usertype":[NSNumber numberWithInteger:1],
                             @"currdt":[Helper getCurrentGMTDateTime]
                             };
    
    NSLog(@"Published Message:%@",params);
    [socket publishToChannel:@"Message" message:params ];
    
    
    
    
    //Send message to server
    
}

- (NSString*)encodeStringTo64:(NSString*)fromString
{
    // Create NSData object
    NSData *nsdata = [fromString
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    // Print the Base64 encoded string
    NSLog(@"Encoded: %@", base64Encoded);
    return base64Encoded;
}

-(void)inputbarDidPressLeftButton:(Inputbar *)inputbar
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Left Button Pressed"
                                                        message:nil
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}
-(void)inputbarDidChangeHeight:(CGFloat)new_height
{
    //Update DAKeyboardControl
    self.view.keyboardTriggerOffset = new_height;
}

#pragma mark - MessageGateway Delegate -

-(void)gatewayDidUpdateStatusForMessage:(Message *)message
{
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
    MessageCell *cell = (MessageCell *)[self.chattingTableView cellForRowAtIndexPath:indexPath];
    [cell updateMessageStatus];
}
-(void)gatewayDidReceiveMessages:(NSArray *)array
{
    self.tableArray = [[TableArray alloc] init];
    [self.tableArray addObjectsFromArray:array];
    dispatch_async(dispatch_get_main_queue(),^{
        [self.chattingTableView reloadData];
        // [self tableViewScrollToBottomAnimated:NO];
        [self.chattingTableView setContentOffset:CGPointMake(0, self.chattingTableView.contentSize.height - self.chattingTableView.bounds.size.height) animated:NO];
    });
}

-(void)updateTableWhenRecieveMessageFromProvider:(Message *)message
{
    [self.tableArray addObject:message];
    dispatch_async(dispatch_get_main_queue(),^{
        
        [self.chattingTableView reloadData];
        if (!isKeyboardOpened) {
            [self.chattingTableView setContentOffset:CGPointMake(0, self.chattingTableView.contentSize.height - self.chattingTableView.bounds.size.height) animated:NO];
        }else{
            [self.chattingTableView setContentOffset:CGPointMake(0, self.chattingTableView.contentSize.height+keyboardHeight+44 - self.chattingTableView.bounds.size.height) animated:NO];
        }
        
    });
    
}

#pragma mark - Message from Push
-(void)receivedMsgFromPsuh:(NSDictionary *)dict{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"chatBid"]) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"chatBid"] integerValue]==[dict[@"bid"] integerValue]) {
        }else{
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"timeStamp"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:dict[@"bid"] forKey:@"chatBid"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"timeStamp"]) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"timeStamp"] integerValue]==[dict[@"timestamp"] integerValue]) {
            NSLog(@"Already Message Exists");
        }else{
            Message *msg = [Message messageFromDictionary:dict andIdentifier:self.bid];
            
            [[LocalStorage sharedInstance] storeMessage:msg];
            //  [self.gateway loadOldMessages];
            
            [self updateTableWhenRecieveMessageFromProvider:msg];
            [[NSUserDefaults standardUserDefaults] setObject:dict[@"timestamp"] forKey:@"timeStamp"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }else{
        Message *msg = [Message messageFromDictionary:dict andIdentifier:self.bid];
        
        [[LocalStorage sharedInstance] storeMessage:msg];
        //  [self.gateway loadOldMessages];
        
        [self updateTableWhenRecieveMessageFromProvider:msg];
        [[NSUserDefaults standardUserDefaults] setObject:dict[@"timestamp"] forKey:@"timeStamp"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
}
#pragma mark - Socket Response -


-(void)respondToChatMessage:(NSDictionary *)message{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"chatBid"]) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"chatBid"] integerValue]==[message[@"bid"] integerValue]) {
        }else{
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"timeStamp"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:message[@"bid"] forKey:@"chatBid"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"timeStamp"]) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"timeStamp"] integerValue]==[message[@"timestamp"] integerValue]) {
            NSLog(@"Already Message Exists");
        }else{
            Message *msg = [Message messageFromDictionary:message andIdentifier:self.bid];
            
            [[LocalStorage sharedInstance] storeMessage:msg];
            //  [self.gateway loadOldMessages];
            
            [self updateTableWhenRecieveMessageFromProvider:msg];
            [[NSUserDefaults standardUserDefaults] setObject:message[@"timestamp"] forKey:@"timeStamp"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }else{
        Message *msg = [Message messageFromDictionary:message andIdentifier:self.bid];
        
        [[LocalStorage sharedInstance] storeMessage:msg];
        //  [self.gateway loadOldMessages];
        
        [self updateTableWhenRecieveMessageFromProvider:msg];
        [[NSUserDefaults standardUserDefaults] setObject:message[@"timestamp"] forKey:@"timeStamp"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
}

-(void)parseOldChatMessages:(NSArray *)arrayOfOldMessages
{
    for(NSDictionary *dict in arrayOfOldMessages)
    {
        [[LocalStorage sharedInstance] storeMessage:[Message messageFromDictionary:dict andIdentifier:self.bid]];
    }
    
    [self.gateway loadOldMessages];
    
}

@end
