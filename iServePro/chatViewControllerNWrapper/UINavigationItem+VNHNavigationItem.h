//
//  UINavigationItem+VNHNavigationItem.h
//  Trustpals
//
//  Created by imma web on 09/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol VNHNavigationItemDelegate <NSObject>
//
//@optional
//- (void)didRightBarButtonCliecked:(id)sender;
//
//@end

@interface UINavigationItem (VNHNavigationItem)

//@property (weak, nonatomic) id<VNHNavigationItemDelegate>delegate;
//
//- (void)createRightBarButtonWithTitle:(NSString *)buttonTitle;
- (void)addLeftSpace;
- (void)addRightSpace;

@end
