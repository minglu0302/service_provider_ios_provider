//
//  UINavigationItem+VNHNavigationItem.m
//  Trustpals
//
//  Created by imma web on 09/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#define kNavigationItemSpace -10

#import "UINavigationItem+VNHNavigationItem.h"

@implementation UINavigationItem (VNHNavigationItem)

//- (void)createRightBarButtonWithTitle:(NSString *)buttonTitle {
//    
//    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
//    [rightButton setTitle:buttonTitle
//                 forState:UIControlStateNormal];
//    
//    [rightButton setTitleColor:[UIColor whiteColor]
//                      forState:UIControlStateNormal];
//    [rightButton setTitleColor:[UIColor grayColor]
//                      forState:UIControlStateHighlighted];
//    
//    [rightButton.titleLabel setFont:[UIFont fontWithName:HelveticaNeueCondensedBold size:15]];
//    [rightButton.titleLabel setNumberOfLines:2];
//    
//    [rightButton addTarget:self
//                    action:@selector(rightBarButtonAction:)
//          forControlEvents:UIControlEventTouchUpInside];
//    
//    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
//    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
//        // Add a negative spacer on iOS >= 7.0
//        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
//                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
//                                           target:nil action:nil];
//        negativeSpacer.width = kNavigationItemSpace;
//        [self setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,button, nil]];
//    } else {
//        // Just set the UIBarButtonItem as you would normally
//        [self setRightBarButtonItem:button];
//    }
//}
//- (void)setDelegate:(id<VNHNavigationItemDelegate>)delegate {
//    self.delegate = delegate;
//}
//- (void)rightBarButtonAction:(id)sender {
//    if (self.delegate && [self.delegate respondsToSelector:@selector(didRightBarButtonCliecked:)]) {
//        [self.delegate didRightBarButtonCliecked:sender];
//    }
//}


- (void)addLeftSpace
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        // Add a negative spacer on iOS >= 7.0
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                        target:nil action:nil];
        negativeSpacer.width = kNavigationItemSpace;
        [self setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, self.leftBarButtonItem, nil]];
    } else {
        // Just set the UIBarButtonItem as you would normally
        [self setLeftBarButtonItem:self.leftBarButtonItem];
    }
}

- (void)addRightSpace
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        // Add a negative spacer on iOS >= 7.0
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = kNavigationItemSpace;
        [self setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, self.rightBarButtonItem, nil]];
    } else {
        // Just set the UIBarButtonItem as you would normally
        [self setRightBarButtonItem:self.rightBarButtonItem];
    }
}
@end
