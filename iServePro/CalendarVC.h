//
//  CalendarVC.h
//  iServePro
//
//  Created by -Tony Lu on 28/03/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JTCalendar/JTCalendar.h>


@interface CalendarVC : UIViewController<JTCalendarDelegate,UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) JTCalendarManager *calendarManager;
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;
@property (strong, nonatomic) IBOutlet UIView *calendarView;

@property (strong, nonatomic) IBOutlet UILabel *year;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)previousCal:(id)sender;
- (IBAction)nextCalendar:(id)sender;
- (IBAction)liveChat:(id)sender;
@end
