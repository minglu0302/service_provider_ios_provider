//
//  webViewController.h
//  iServePro
//
//  Created by -Tony Lu on 19/03/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>
@property(nonatomic,strong) NSString *weburl;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property(strong,nonatomic) UIButton *backButton;
@end
