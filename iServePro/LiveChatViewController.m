//
//  LiveChatViewController.m
//  Tease
//
//  Created by Rahulsharma on 24/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "LiveChatViewController.h"

@interface LiveChatViewController ()

@property (nonatomic, copy) NSString *chatUrl;
@end

@implementation LiveChatViewController

- (void)viewDidLoad {
      [super viewDidLoad];

    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self setTitle:NSLocalizedString(@"Support", @"Support") ];
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    self.indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    [self.indicator setColor:[UIColor blackColor]];
    [self.indicator startAnimating];
    [self.view addSubview:self.indicator];
    [self requestUrls];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
    
    [self createNavLeftButton];
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_off"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_on"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backToController
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    void (^showChatView)(void) = ^(void) {
        [self.chatView setAlpha:1.0];
        [self.indicator setAlpha:0.0];
    };
    
    void (^stopIndicator)(BOOL) = ^(BOOL finished) {
        [self.indicator stopAnimating];
    };
    
    [UIView animateWithDuration:1.0
                          delay:1.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:showChatView
                     completion:stopIndicator];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//set the defines and call this method to get the live chat url and give the url to webview controller
- (void)requestUrls
{
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURL *url = [NSURL URLWithString:@LC_URL];
    NSURL *url = [NSURL URLWithString:@LC_CHAT_URL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [self.chatView loadRequest:request];
//    [[session dataTaskWithURL:url
//            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                if (!error) {
//                    NSError *jsonError;
//                    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data
//                                                                         options:NSJSONReadingAllowFragments
//                                                                           error:&jsonError];
//                    
//                    if ([JSON isKindOfClass:[NSDictionary class]] && [JSON valueForKey:@"chat_url"] != nil) {
//                        NSURL *url = [NSURL URLWithString: [self prepareUrl:JSON[@"chat_url"]]];
//                        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
//                        [self.chatView loadRequest:request];
//                        
//                    } else if (jsonError) {
//                        NSLog(@"%@", jsonError);
//                    }
//                } else {
//                    NSLog(@"%@", error);
//                }
//            }] resume];
}
//
- (NSString *)prepareUrl:(NSString *)url
{
    NSMutableString *string = [NSMutableString stringWithFormat:@"http://%@", url];

    [string replaceOccurrencesOfString:@"{%license%}"
                            withString:@LC_LICENSE
                               options:NSLiteralSearch
                                 range:NSMakeRange(0, [string length])];
    
    [string replaceOccurrencesOfString:@"{%group%}"
                            withString:@"Goclean Service"
                               options:NSLiteralSearch
                                 range:NSMakeRange(0, [string length])];
    
    return string;
}


@end
