//
//  ScheduleViewController.m
//  Onthewaypro
//
//  Created by -Tony Lu on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ScheduleViewController.h"
#import "ScheduleTableCell.h"
#import "User.h"
#import "LocationTracker.h"
#import "iServeSplashController.h"
#import "SlotDetailsVC.h"
#import "LiveChatViewController.h"


@interface ScheduleViewController ()<JTCalendarDelegate,UserDelegate>
{
    NSMutableDictionary *_eventsByDate;
    
    NSDate *_todayDate;
    NSDate *_minDate;
    NSDate *_maxDate;
    NSDate *_dateSelected;
}
@property (strong, nonatomic) NSMutableArray *dict;
@end


@implementation ScheduleViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(!self){
        return nil;
    }
    
    
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dict =[[NSMutableArray alloc]init];
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
   
    // Generate random events sort by date using a dateformatter for the demonstration
    // [self createRandomEvents];
    
    // Create a min and max date for limit the calendar, optional
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x2598ED)};
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    _yearLabel.text = [formatter stringFromDate:[NSDate date]];
    [self createMinAndMaxDate];
    [_calendarManager setMenuView:_headerView];
    [_calendarManager setContentView:_mainCalendarView];
    [_calendarManager setDate:_todayDate];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewDidAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;

    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"secDate"]) {
        [self getALLSlots:[ud objectForKey:@"secDate"]];
    }else{
        NSString *month = [self getMonths];
        [ud setObject:month forKey:@"secDate"];
        [ud synchronize];
        [self getALLSlots:month];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userSessionTokenExpire) name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    
}

-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)getALLSlots :(NSString *)month
{
//     [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:NSLocalizedString(@"Loading...",@"Loading...")];

    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = @{
                           @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                           @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                           @"ent_pro_id":[ud objectForKey:@"ProviderId"],
                           @"ent_month":month
                           };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"GetAllSlot"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) { //handle success response
                                 [self createRandomEvents:[response mutableCopy]];
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             }
                             else{
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 
                                 NSLog(@"Error");
                                 
                             }
                             
                         }];
}

/***************************************/
-(NSString *)getMonths
{
    NSDate *date = [NSDate date];
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    // NSInteger Day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    //
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month]];
    
    NSString *retMonth = [NSString stringWithFormat:@"%ld-%@",(long)year,monthString];
    return retMonth;
}

/***************************************/

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = UIColorFromRGB(0X808080);
        dayView.dotView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_mainCalendarView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}


- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                       [self.mainTableView reloadData];
                    } completion:nil];
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_mainCalendarView.date isTheSameMonthThan:dayView.date]){
        if([_mainCalendarView.date compare:dayView.date] == NSOrderedAscending){
            [_mainCalendarView loadNextPageWithAnimation];
        }
        else{
            [_mainCalendarView loadPreviousPageWithAnimation];
        }
    }
}


#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [_calendarManager.dateHelper date:date isEqualOrAfter:_minDate andEqualOrBefore:_maxDate];
}

- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    _yearLabel.text = [formatter stringFromDate:calendar.date];
    [self getSlotDetailsAccordingToMonths:calendar.date];
    //    NSLog(@"Next page loaded");
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    _yearLabel.text = [formatter stringFromDate:calendar.date];
    [self getSlotDetailsAccordingToMonths:calendar.date];
    //    NSLog(@"Previous page loaded");
}

-(void)getSlotDetailsAccordingToMonths:(NSDate *)dateString{
    NSDate *date = dateString;
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month]];
    
    NSString *retMonth = [NSString stringWithFormat:@"%ld-%@",(long)year,monthString];
     [[NSUserDefaults standardUserDefaults] setObject:retMonth forKey:@"secDate"];
     [[NSUserDefaults standardUserDefaults] synchronize];
    [self getALLSlots:retMonth];

}
#pragma mark - Fake data

- (void)createMinAndMaxDate
{
    _todayDate = [NSDate date];
    
    // Min date will be 2 month before today
    _minDate = [_calendarManager.dateHelper addToDate:_todayDate months:-12];
    
    // Max date will be 2 month after today
    _maxDate = [_calendarManager.dateHelper addToDate:_todayDate months:9];
}

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"yyyy-MM-dd";//dd-MM-yyyy
    }
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    return NO;
    
}

- (void)createRandomEvents:(NSMutableDictionary *)response
{
    if ([response[@"errFlag"] integerValue]==1) {
        if ([response[@"errNum"] integerValue] == 7) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else if ([response[@"errNum"] integerValue] == 96 || [response[@"errNum"] integerValue] == 94) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
        
    }else{
        
        _eventsByDate = [NSMutableDictionary new];
        NSMutableDictionary *apArr = [[NSMutableDictionary alloc]init];
        
        NSMutableArray *appointmentsArr = [response objectForKey:@"slots"];
        for(int i = 0; i < appointmentsArr.count; ++i){
            
            apArr = [appointmentsArr objectAtIndex:i];
            
            NSArray *appDetailArr = [apArr objectForKey:@"slot"];
            
            NSString *key = [apArr objectForKey:@"date"];
            
            if(!_eventsByDate[key]){
                _eventsByDate[key] =appDetailArr;
            }
        }
        [_calendarManager reload];
        [self.mainTableView reloadData];
    }
}

/*-------------------------*/
#pragma mark - UserDelegate
/*-------------------------*/
-(void)userDidLogoutSucessfully:(BOOL)sucess {
    if (sucess){
        // Logged it out Successfully
        NSLog(@"Logged it out Successfully");
    }
    else{
        // Session is Expired
        NSLog(@"Session is Expired");
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)userDidFailedToLogout:(NSError *)error
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}



#pragma mark - UITableView Datasource and delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 90;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];//2012-11-22
    NSString *date;
    
    if (!_dateSelected) {
        date = [formatter stringFromDate:_todayDate];
    }else{
        date = [formatter stringFromDate:_dateSelected];
    }
    _dict=[_eventsByDate[date]mutableCopy];
    return [_eventsByDate[date] count];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ScheduleCell";
    ScheduleTableCell *cell;
    
    if (!cell) {
        cell  = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    cell.startTime.text =[self convertTo24hrsto12Format:_dict[indexPath.row][@"start_dt"]];
    cell.endTime.text = [self convertTo24hrsto12Format:_dict[indexPath.row][@"end_dt"]];
    cell.layer.cornerRadius = 5;
    cell.layer.masksToBounds = YES;
    
  

    
    if ([_dict[indexPath.row][@"booked"]integerValue]==1) {
        cell.slotHeaderView.backgroundColor = UIColorFromRGB(0X10CFBD);
        cell.status.text = @"FREE";
    }else if ([_dict[indexPath.row][@"booked"]integerValue]==3){
        cell.status.text = @"BOOKED";
        cell.slotHeaderView.backgroundColor = UIColorFromRGB(0X6D5CAE);
    }else {
        cell.status.text = @"COMPLETED";
        cell.slotHeaderView.backgroundColor = UIColorFromRGB(0XF55753);
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(NSString *)convertTo24hrsto12Format:(NSString*)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    NSDate *date1 = [dateFormatter dateFromString:date];
    
    dateFormatter.dateFormat = @"hh:mm a";
    NSString *pmamDateString = [dateFormatter stringFromDate:date1];
    return pmamDateString;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIStoryboard *mainstoryboard;
    if ([_dict[indexPath.row][@"booked"]integerValue] == 1 ||[_dict[indexPath.row][@"booked"]integerValue] == 3) {
        if (!mainstoryboard) {
            mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        }
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];//2012-11-22
        NSString *date;
        
        if (!_dateSelected) {
            date = [formatter stringFromDate:_todayDate];
        }else{
            date = [formatter stringFromDate:_dateSelected];
        }
 
        SlotDetailsVC  *slotDetails = [mainstoryboard instantiateViewControllerWithIdentifier:@"slotDetailsVC"];
        UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:slotDetails];
        slotDetails.dict = _dict[indexPath.row];
        slotDetails.slotSelectedDate = date;
        [self presentViewController:navBar animated:YES completion:nil];
    }else {
        [Helper showAlertWithTitle:@"Booking Completed" Message:@"Check for Invoice in History..!!"];
    }
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)preCalendar:(id)sender {
       [_mainCalendarView loadPreviousPageWithAnimation];
}


- (IBAction)postCalendar:(id)sender {
      [_mainCalendarView loadNextPageWithAnimation];

}
- (IBAction)addSlot:(id)sender {
    self.tabBarController.tabBar.hidden = YES;

    [self performSegueWithIdentifier:@"AddSlotSegue" sender:nil];
}

- (IBAction)liveChat:(id)sender {
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    LiveChatViewController  *chatVc = [mainstoryboard instantiateViewControllerWithIdentifier:@"liveChatVC"];
    
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:chatVc];
    navBar.navigationBar.translucent = NO;
    [self presentViewController:navBar animated:YES completion:nil];
}
@end
