//
//  Services1TableViewCell.h
//  Onthewaypro
//
//  Created by -Tony Lu on 27/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Services1TableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *serviceValue;

@property (strong, nonatomic) IBOutlet UILabel *serviceName;
@end
