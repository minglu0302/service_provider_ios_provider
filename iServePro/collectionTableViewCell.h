//
//  collectionTableViewCell.h
//  iServePro
//
//  Created by -Tony Lu on 22/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface collectionTableViewCell : UITableViewCell
@property (nonatomic, strong)NSMutableArray *images;
-(void)reloadCollectionView:(NSMutableArray*)jobImages;
-(void)reloadCollection:(NSMutableArray *)jobImages;
-(void)addImagesSelected;
@property (strong, nonatomic) IBOutlet UICollectionView *jobCollection;
@property NSInteger count;
@property NSInteger count1;

@property (nonatomic, strong)NSMutableArray *UpdatedImages;

@property BOOL isDeleteActive;
@property NSInteger deletedIndexpath;
@property NSInteger c;
@property (nonatomic, strong)UIButton *buttonDelete;

@property BOOL isAddOrDelete;
@property (nonatomic, strong)NSMutableArray *cacheImages;
@property (strong, nonatomic) UIImageView *cacheImage;

@end
