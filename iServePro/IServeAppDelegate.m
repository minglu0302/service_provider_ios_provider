
//
//  AppDelegate.m
//  iServePro
//
//  Created by -Tony Lu on 24/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "IServeAppDelegate.h"
#import "iServeHelpController.h"
#import "iServeSplashController.h"
#import "PMDReachabilityWrapper.h"
#import "IServeHomeViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "NewBookingController.h"
#import "ChatSIOClient.h"
#import "ChatSocketIOClient.h"
#import "HomeTabBarController.h"
#import "AmazonTransfer.h"
#import "NewBookingController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "ATAppUpdater.h"
#import "PresentingAnimator.h"
#import "DismissingAnimator.h"
#import <Firebase.h>
#import "ChatViewController.h"
#import "TWMessageBarManager.h"
#import "AddRemindersAndEvents.h"



@interface IServeAppDelegate ()<UIViewControllerTransitioningDelegate,ChatSocketIODelegetes>
{
     NSString *alertMsg;
    ChatSocketIOClient *client;
     UIBackgroundTaskIdentifier newTask;
     UIBackgroundTaskIdentifier oldTask;
    NSDictionary *bookingDict;
    NSDictionary *infoDict;
}
@property (nonatomic,strong) iServeHelpController *helpViewController;
@property (nonatomic,strong) iServeSplashController *splashViewController;
//@property (nonatomic,strong) MapViewController *mapViewContoller;

@property (nonatomic, strong) Reachability *hostReach;
@property (nonatomic, strong) Reachability *internetReach;
@property (nonatomic, strong) Reachability *wifiReach;
@property (nonatomic, strong) NSDictionary *pushInfo;
@property (strong, nonatomic) UIStoryboard *mainstoryboard;
@property (nonatomic) NSTimer* locationUpdateTimer;

@property (nonatomic, strong) NSDictionary *chatData;


@end

@implementation IServeAppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize hostReach;
@synthesize internetReach;
@synthesize wifiReach;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [self registerForNotifications:application];
    
    [FIRApp configure];
    
    [[TELogger getInstance] initiateFileLogging];
    
    
    [GMSServices provideAPIKey:kPMDGoogleMapsAPIKey]; // configuring the google map key
    [AmazonTransfer setConfigurationWithRegion:AWSRegionUSWest1 accessKey:AmazonAccessKey secretKey:AmazonSecretKey];  /// Configuring the amazon keys and region
    //  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent]; // for status bar text in white color
    
    [self handlePushNotificationWithLaunchOption:launchOptions]; // handles the push when app got killded
    [self setupAppearance];
    [Fabric with:@[[Crashlytics class]]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    return YES;
}

///*****************Registering the new feature to handle notification when the app new background*******//
-(void)registerForNotifications:(UIApplication *)application
{
    //Configuring for Interactive Notifications
    UIMutableUserNotificationAction *accept = [[UIMutableUserNotificationAction alloc] init];
    accept.identifier =NSLocalizedString(@"Accept",@"Accept");
    accept.title = NSLocalizedString(@"Accept",@"Accept");
    accept.activationMode = UIUserNotificationActivationModeForeground;
    accept.destructive = NO;
    accept.authenticationRequired = YES;
    
    UIMutableUserNotificationAction *reject = [[UIMutableUserNotificationAction alloc] init];
    reject.identifier = NSLocalizedString(@"Reject",@"Reject");
    reject.title = NSLocalizedString(@"Reject",@"Reject");
    reject.activationMode = UIUserNotificationActivationModeBackground;
    reject.destructive = NO;
    reject.authenticationRequired = YES;
    
    UIMutableUserNotificationCategory *notificationCategory = [[UIMutableUserNotificationCategory alloc] init];
    notificationCategory.identifier = NSLocalizedString(@"JobRequest",@"JobRequest");
    [notificationCategory setActions:@[accept,reject] forContext:UIUserNotificationActionContextDefault];
    
    NSSet *categories = [NSSet setWithObjects:notificationCategory, nil];
    
    //Registering for Local Notification
   // [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound categories:categories]];
    
    //Registering for Remote Notification
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert ) categories:categories]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
}


///***************Handles the local push notifcation data**********************//
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    // from  socket

    NSDictionary *dict = notification.userInfo;
    NSDictionary *bookingArray = dict[@"aps"];
    NSInteger btype = [bookingArray[@"btype"]integerValue];
    if (btype == 1 || btype == 3) {
        _isNotificationClicked = YES;
        [self openNewBookingVCWithInfo:bookingArray];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNewbooking"];
    }else if(btype == 2){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNewbooking"];
        _isNotificationClicked = YES;

        [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
        UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
        
        TELogInfo(@"currentViewControll : %@", naviVC.viewControllers);
        
        if ([[naviVC.viewControllers lastObject] isKindOfClass:[IServeHomeViewController class]]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
        }
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@"Congrats you got scheduling booking from%@ ", bookingArray[@"cname"]]];
    }
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet:
                    [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"push token distribution: %@", dt);
    
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:KDAgetPushToken];
    }
    else {
        [[NSUserDefaults standardUserDefaults]setObject:dt forKey:KDAgetPushToken];
    }
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken
                                        type:FIRInstanceIDAPNSTokenTypeSandbox];  ///Develop mode
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken
                                        type:FIRInstanceIDAPNSTokenTypeProd];   /// Distribution Mode
    
}

///************** Generating and saving the fcm token ****************//
- (void)tokenRefreshNotification:(NSNotification *)notification
{
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    if (refreshedToken) {
         [[NSUserDefaults standardUserDefaults] setObject:refreshedToken forKey:@"FcmPushToken"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
   
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    // TODO: If necessary send token to application server.
}

///****************Connecting to fcm if no fcm token**********//
- (void)connectToFcm
{
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    [[FIRMessaging messaging] disconnect];
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
            [[FIRMessaging messaging] subscribeToTopic:@"/topics/ios"];
            [[FIRMessaging messaging] subscribeToTopic:@"/topics/all"];
        }
    }];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"])
    {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:KDAgetPushToken];
    }
}

/**
 *  Check for Version Change
 */
- (void)checkForVersionUpdate {
    
    BOOL showCutomAlert = NO;
    
    if (showCutomAlert) {
        // Custom alert
        ATAppUpdater *updater = [ATAppUpdater sharedUpdater];
        
        [updater setAlertTitle:NSLocalizedString(@"New Version", @"New Version")];
        [updater setAlertMessage:NSLocalizedString(@"Version %@ is available on the AppStore.", @"Version %@ is available on the AppStore.")];
        [updater setAlertUpdateButtonTitle:NSLocalizedString(@"Update", @"Update")];
        [updater setAlertCancelButtonTitle:NSLocalizedString(@"Not Now", @"Not Now")];
        [updater showUpdateWithConfirmation];
    }
    else {
        //[[ATAppUpdater sharedUpdater] showUpdateWithConfirmation]; // reminder for update
          [[ATAppUpdater sharedUpdater] showUpdateWithForce];  // Forceful update
    }
}



///**************Handles the booking local notifcation when the app in background************//Above iOS 10.0
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void(^)())completionHandler {
    _isNotificationClicked =YES;
    NSDictionary *dict = [notification.userInfo mutableCopy];
    NewBookingController *new =[[NewBookingController alloc]init];
      [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNewbooking"];
    if([identifier isEqualToString:@"Accept"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"bookingStatusResponse"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [new receiveNotification:dict];
    }
    else if([identifier isEqualToString:@"Reject"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"3" forKey:@"bookingStatusResponse"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [new receiveNotification:dict];
    }
    if(completionHandler != nil)    //Finally call completion handler if its not nil
        completionHandler();
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    if (!client) {
        client =[ChatSocketIOClient sharedInstance];
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"LoggedIn"])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [client heartBeat:@"0"];
            NSLog(@"Become in Background=Called");
        });
    }
    NSLog(@"Become in Background");
}


- (void)applicationWillEnterForeground:(UIApplication *)application {

    if([[NSUserDefaults standardUserDefaults] boolForKey:@"LoggedIn"])
    {
        if (!client) {
               client = [ChatSocketIOClient sharedInstance];
        }
        [client connectSocket];
        [client heartBeat:@"1"];
    }
    NSLog(@"Become in Foreground");
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    //Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
      [self checkForVersionUpdate];
    application.applicationIconBadgeNumber = 1;
    application.applicationIconBadgeNumber = 0;
    if (_isNotificationClicked == NO && [[NSUserDefaults standardUserDefaults] boolForKey:@"isNewbooking"]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNewbooking"];
        NSInteger btype = [bookingDict[@"btype"]integerValue];
        if (btype == 1 || btype == 3) {
            [self openNewBookingVCWithInfo:bookingDict];
        }else if(btype == 2){
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNewbooking"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
            UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
            
            TELogInfo(@"currentViewControll : %@", naviVC.viewControllers);
            
            if ([[naviVC.viewControllers lastObject] isKindOfClass:[IServeHomeViewController class]]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
            }
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@"Congrats you got scheduling booking from%@ ", bookingDict[@"cname"]]];
        }
    }

    PMDReachabilityWrapper *networkHandler = [PMDReachabilityWrapper sharedInstance];
    [networkHandler monitorReachability];
    if([application backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied)
    {
        NSLog(@"Background Denied");
    }
    else if([application backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted)
    {
        NSLog(@"Background Restricted");
    }
    else
    {
        NSLog(@"Background Enable");
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"LoggedIn"])
    {
    client =[ChatSocketIOClient sharedInstance];
    }

}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

///*******************Handles the push data when clicking on push notification*********//
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"user Info %@",userInfo);
    _isNotificationClicked = YES;
    if (userInfo[@"st"]) {
        [self handleNotificationForUserInfo:userInfo];
    }else if(userInfo[@"nt"]){
      
    }else{
        NSError *jsonError;
        NSData *objectData = [userInfo[@"payload"] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
        NSString *currentDateNTime =[Helper getCurrentDateTime];
        [ud setObject:currentDateNTime forKey:@"currentbookingDateNtime"];
        [ud synchronize];
       [self handleNotificationForUserInfo:json];
    }
}

#pragma mark -
#pragma mark Core Data stack
// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"AddressManage" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"AddressManage.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}



- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    
    return YES;
}


void uncaughtExceptionHandler(NSException *exception)
{
    NSLog(@"The app has encountered an unhandled exception: %@", [exception debugDescription]);
    NSLog(@"Stack trace: %@", [exception callStackSymbols]);
    NSLog(@"desc: %@", [exception description]);
    NSLog(@"name: %@", [exception name]);
    NSLog(@"user info: %@", [exception userInfo]);
}


#pragma mark - HandlePushNotification
/**
 *  handle push if app is opened by clicking push
 *
 *  @param launchOptions pushpayload dictionary
 */
-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions
{
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload)
    {
        //check if user is logged in
        if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
        {
            NSLog(@"user Info %@",remoteNotificationPayload);
            if (remoteNotificationPayload[@"st"]) {
                [self performSelector:@selector(handleNotificationForUserInfo:) withObject:remoteNotificationPayload afterDelay:3];
            }else if (remoteNotificationPayload[@"nt"]){
                
            }
            else{
                _isNotificationClicked = YES;
                NSError *jsonError;
                NSData *objectData = [remoteNotificationPayload[@"payload"] dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&jsonError];
                NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
                NSString *currentDateNTime =[Helper getCurrentDateTime];
                [ud setObject:currentDateNTime forKey:@"currentbookingDateNtime"];
                [ud synchronize];
                [self performSelector:@selector(handleNotificationForUserInfo:) withObject:json afterDelay:3];
            }
        }
    }
}



#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma Reachability


///************Monitoring the Network*************///
- (void)monitorReachability {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    self.hostReach = [Reachability reachabilityWithHostName:@"http://www.goclean-service.com"];
    [self.hostReach startNotifier];
    
    self.internetReach = [Reachability reachabilityForInternetConnection];
    [self.internetReach startNotifier];
    
    self.wifiReach = [Reachability reachabilityForLocalWiFi];
    [self.wifiReach startNotifier];
}
- (BOOL)isNetworkAvailable {
    
    return self.networkStatus != NotReachable;
}

// Called by Reachability whenever status changes.
- (void)reachabilityChanged:(NSNotification* )note {
    
    Reachability *curReach = (Reachability *)[note object];
    
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
    _networkStatus = [curReach currentReachabilityStatus];
    
    if (_networkStatus == NotReachable) {
        NSLog(@"Network not reachable.");
    }
    
}

- (void)saveContext
{
    
}

/*----------------------------------------*/
#pragma mark - Set Up NAvigation Appearance
/*----------------------------------------*/

- (void)setupAppearance
{
    [[UINavigationBar appearance] setBackgroundColor:UIColorFromRGB(0x041420)];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"login_navigation_bar.png"] forBarMetrics:UIBarMetricsDefault];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"opensans" size:17], NSFontAttributeName,UIColorFromRGB(0x2598ED), NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:textAttributes];
}

///**********Update the driver status********************//
-(void)checkDriverStatus{

        ChatSIOClient *socket =[ChatSIOClient sharedInstance];
    
        NSString *driverEmail = [[NSUserDefaults standardUserDefaults] objectForKey:@"dEmail"];
        NSString *deviceUDID = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
        NSDictionary *message = @{@"a":[NSNumber numberWithInt:2],
                                  @"e_id": driverEmail,
                                  @"d_id":deviceUDID,
                                  };
            client.chatDelegate = self;
        [socket publishToChannel:kISPSocketChannel message:message];
}

///****shows the socket push when app is not in chat controller*************//
- (void)successButtonPressed:(NSDictionary *)sender
{
    _chatData =  sender[@"aps"][0];
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Message"
                                                   description:sender[@"aps"][0][@"payload"]
                                                          type:TWMessageBarMessageTypeSuccess
                                                statusBarStyle:UIStatusBarStyleDefault
                                                      callback:nil];
}

///****present chat controller by clicking on socket custom push*************//

- (void)OpenChatViewVC{
    UIStoryboard *mainstoryboard;
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    ChatViewController  *chatVc = [mainstoryboard instantiateViewControllerWithIdentifier:@"chatVC"];
    chatVc.bid =[NSString stringWithFormat:@"%@", _chatData[@"bid"]];
    chatVc.custPic = _chatData[@"pic"];
    IServeAppDelegate *tmpDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:chatVc];
    UIViewController *vc = ((UINavigationController*)tmpDelegate.window.rootViewController).visibleViewController;
    [vc presentViewController:navBar animated:YES completion:nil];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 920) {
        if (buttonIndex == 0) {
            UIStoryboard *mainstoryboard;
            if (!mainstoryboard) {
                mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            }
            ChatViewController  *chatVc = [mainstoryboard instantiateViewControllerWithIdentifier:@"chatVC"];
            chatVc.bid = infoDict[@"bid"];
            chatVc.custPic = infoDict[@"pic"];
            IServeAppDelegate *tmpDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
            UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:chatVc];
            UIViewController *vc = ((UINavigationController*)tmpDelegate.window.rootViewController).visibleViewController;
            [vc presentViewController:navBar animated:YES completion:nil];
        }
    }
}

///************************Handling push Notification***********************//
- (void)handleNotificationForUserInfo:(NSDictionary*)userInfo{
    
    if (userInfo[@"st"]) {
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"OnChatController"]) {
            NSString *string1 = [NSString stringWithFormat:@"BID:%@",userInfo[@"bid"]];
            alertMsg =[NSString stringWithFormat:@"A Message from %@, Do you want to Check??",userInfo[@"name"]];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:string1 message:alertMsg delegate:self cancelButtonTitle:NSLocalizedString(@"Yes",@"Yes") otherButtonTitles:NSLocalizedString(@"No",@"No"), nil];
            
            alert.tag = 920;
            [alert show];
            infoDict = userInfo;
            
        }else{
            UIStoryboard *mainstoryboard;
            if (!mainstoryboard) {
                mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            }
            ChatViewController  *chatVc = [mainstoryboard instantiateViewControllerWithIdentifier:@"chatVC"];
            chatVc.bid = userInfo[@"bid"];
            chatVc.custPic = userInfo[@"pic"];
            [chatVc receivedMsgFromPsuh:userInfo];
        }
    }else{
    NSLog(@"handle push %@",userInfo);
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef = CFBundleCopyResourceURL(mainBundle, CFSTR("sms-received"), CFSTR("wav"), NULL);
    SystemSoundID soundId;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundId);
    AudioServicesPlaySystemSound(soundId);
    CFRelease(soundFileURLRef);
    
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isBooked"];
    
    self.pushInfo = userInfo;
    
    int type = [userInfo[@"btype"] intValue];
    switch (type) {
        case 1:
        case 3://live Booking
        {
            [self openNewBookingVCWithInfo:self.pushInfo];
        }
            break;
        case 2: // later booking
        {
            [[NSUserDefaults standardUserDefaults] setObject:userInfo[@"bid"] forKey:@"latBid"];
            [[NSUserDefaults standardUserDefaults] setObject:userInfo[@"cname"] forKey:@"customerName"];
            [[NSUserDefaults standardUserDefaults] setObject:userInfo[@"add"] forKey:@"custAddress"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
            UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
            
            TELogInfo(@"currentViewControll : %@", naviVC.viewControllers);
            
            if ([[naviVC.viewControllers lastObject] isKindOfClass:[IServeHomeViewController class]]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
            }
            [self remainderAdded:userInfo[@"stime"] endTime:userInfo[@"etime"]];
        }
            break;
        case 4:// cancel booking
        {
            alertMsg =[NSString stringWithFormat:@"%@", userInfo[@"alert"]];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentCancel" object:nil userInfo:nil];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Booking Cancelled",@"Booking Cancelled") message:alertMsg delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
            [alert show];
            
        }break;
            
        case 5: // reject from admin
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:alertMsg delegate:self cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles: nil];
            
            UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
            if ([[[[[[naviVC.viewControllers objectAtIndex:0] childViewControllers] objectAtIndex:1]  childViewControllers] lastObject] isKindOfClass:[IServeHomeViewController class]]) {
                alert.tag = 400;
                [alert show];
            }
        }
            break;
        case 19:
        {
            alertMsg = userInfo[@"alert"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HandleAcceptAndRejectFromAdmin" object:nil userInfo:nil];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:alertMsg delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
            [alert show];
        }
            break;
            
        default:
            break;
    }
    }
}

///************************Added the reminder for lateral booking***********************//
-(void)remainderAdded:(NSString *)startTime endTime:(NSString *)endTime{
    
    NSTimeInterval start = [startTime doubleValue];
    NSDate * startData = [NSDate dateWithTimeIntervalSince1970:start];
    NSString *stringStart = [self getDateStringFromDate:startData];
    
    NSTimeInterval end = [endTime doubleValue];
    NSDate * endDate = [NSDate dateWithTimeIntervalSince1970:end];
    NSString *stringEnd = [self getDateStringFromDate:endDate];
    
    NSDate *FromDate = [self convertGMTtoLocalTimeConversion:stringStart];
    NSDate *toDate = [self convertGMTtoLocalTimeConversion:stringEnd];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:startData forKey:@"startBookingDate"];
    AddRemindersAndEvents *eventObj = [AddRemindersAndEvents instance];
    [eventObj eventStore];
    [eventObj setStartingDate:FromDate];
    [eventObj setEndingDate:toDate];
    [eventObj updateAuthorizationStatusToAccessEventStore];
}

///************************convert date in string format date***********************//

-(NSString *)getDateStringFromDate :(NSDate *)date {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}
///************************convert gmt to local time ***********************//
-(NSDate *)convertGMTtoLocalTimeConversion:(NSString *)gmtDateStr
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *local = [NSTimeZone systemTimeZone];
    [formatter setTimeZone:local];
    NSDate *localDate = [formatter dateFromString:gmtDateStr]; // get the date
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    NSTimeInterval localTimeInterval = [localDate timeIntervalSinceReferenceDate] - timeZoneOffset;
    NSDate *localCurrentDate = [NSDate dateWithTimeIntervalSinceReferenceDate:localTimeInterval];
    return localCurrentDate;
}

/*************************Socket Delegate Method***************************/
-(void)responseFromChannels:(NSDictionary *)responseDictionary{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    NSInteger btype = [responseDictionary[@"btype"]integerValue];
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    
    NSString *currentDateNTime =[Helper getCurrentDateTime];
    [ud setObject:currentDateNTime forKey:@"currentbookingDateNtime"];
    [ud synchronize];
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        switch (btype) {
            case 1:
            case 3:
            {
                NSMutableDictionary *notificationDict = [[NSMutableDictionary alloc] init];
                [notificationDict setObject:responseDictionary forKey:@"aps"];
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.category  = NSLocalizedString(@"JobRequest",@"JobRequest");
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertBody =NSLocalizedString(@"Congratulations you got a new booking request", @"Congratulations you got a new booking request") ;
                localNotification.userInfo = notificationDict;
                localNotification.soundName = @"sound.mp3";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                bookingDict = responseDictionary;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isNewbooking"];
            }
                break;
            case 2:{
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"bid"] forKey:@"latBid"];
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"cname"] forKey:@"customerName"];
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"add"] forKey:@"custAddress"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSMutableDictionary *notificationDict = [[NSMutableDictionary alloc] init];
                [notificationDict setObject:responseDictionary forKey:@"aps"];
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.category  = NSLocalizedString(@"JobRequest",@"JobRequest");
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertBody = NSLocalizedString(@"Congratulations you got a new Scheduled booking", @"Congratulations you got a new Scheduled booking");
                localNotification.userInfo = notificationDict;
                localNotification.soundName = @"sound.mp3";
                bookingDict = responseDictionary;

                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"cname"] forKey:@"customerName"];
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"add"] forKey:@"custAddress"];
                [[NSUserDefaults standardUserDefaults] synchronize];
               [self remainderAdded:responseDictionary[@"stime"] endTime:responseDictionary[@"etime"]];
            }
                break;
            case 4:
            {
                NSMutableDictionary *notificationDict = [[NSMutableDictionary alloc] init];
                [notificationDict setObject:responseDictionary forKey:@"aps"];
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.category  = NSLocalizedString(@"JobRequest",@"JobRequest");
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertBody = NSLocalizedString(@"You booking has been cancelled", @"You booking has been cancelled") ;
                localNotification.soundName = @"sound.mp3";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                
                NSString *string = [NSString stringWithFormat:@"Reason:%@",responseDictionary[@"can_reason"]];
                NSString *string1 = [NSString stringWithFormat:@"Booking Cancelled BID:%@",responseDictionary[@"bid"]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:string1 message:string delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
                [alert show];
                
            }
                break;
            default:
                break;
        }
    }else{
        switch (btype) {
            case 1:
            case 3:
            {
                [self openNewBookingVCWithInfo:responseDictionary];
            }
                break;
            case 2:
            {
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"bid"] forKey:@"latBid"];
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"cname"] forKey:@"customerName"];
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"add"] forKey:@"custAddress"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
                UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
                
                if ([[naviVC.viewControllers lastObject] isKindOfClass:[IServeHomeViewController class]]) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
                }
                [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@"Congrats you got scheduling booking from %@", responseDictionary[@"cname"]]];
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"cname"] forKey:@"customerName"];
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"add"] forKey:@"custAddress"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self remainderAdded:responseDictionary[@"stime"] endTime:responseDictionary[@"etime"]];
                
            }
                break;
            case 4:
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                NSString *string = [NSString stringWithFormat:@"Reason:%@",responseDictionary[@"can_reason"]];
                NSString *string1 = [NSString stringWithFormat:@"Booking Cancelled BID:%@",responseDictionary[@"bid"]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:string1 message:string delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
                [alert show];
                });
            }
                break;
            default:
                break;
        }
    }
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source
{
    return [PresentingAnimator new];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return [DismissingAnimator new];
}


///************************Booking pop Functionality done here***********************//
-(void)openNewBookingVCWithInfo:(NSDictionary *)info
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"lastBid"]) {
        if (([[ud objectForKey:@"lastBid"] integerValue]==[info[@"bid"]integerValue])) {
            return;
        }else{
            [ud setObject:info[@"bid"] forKey:@"lastBid"];
            [ud synchronize];
        }
    }else{
        [ud setObject:info[@"bid"] forKey:@"lastBid"];
        [ud synchronize];
    }
    NSDateFormatter *df = [self DateFormatterProper];
    
    NSDate *bookingDate = [df dateFromString:[[NSUserDefaults standardUserDefaults] objectForKey:@"currentbookingDateNtime"]];
    
    NSTimeInterval timeDiff = [[NSDate date] timeIntervalSinceDate:bookingDate];
    
    _isNotificationClicked =NO;
     [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNewbooking"];
    IServeAppDelegate *tmpDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    UIViewController *vc = ((UINavigationController*)tmpDelegate.window.rootViewController).visibleViewController;
    
    if (!_mainstoryboard) {
        _mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    NewBookingController *newBookingVC = [_mainstoryboard instantiateViewControllerWithIdentifier:@"newBookingVC"];
    newBookingVC.detailsDict=info;
    
    newBookingVC.timeRemaining =30- timeDiff;
    
    newBookingVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [newBookingVC setModalPresentationStyle:UIModalPresentationOverFullScreen];
    
    newBookingVC.transitioningDelegate = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [vc presentViewController:newBookingVC
                         animated:YES
                       completion:NULL];
    });
;

}

// Used to get required date format //
- (NSDateFormatter *)DateFormatterProper {
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return formatter;
}


@end
