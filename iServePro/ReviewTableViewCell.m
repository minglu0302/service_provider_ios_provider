//
//  ReviewTableViewCell.m
//  iServePro
//
//  Created by -Tony Lu on 22/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ReviewTableViewCell.h"

@implementation ReviewTableViewCell

- (void)awakeFromNib {
     [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
