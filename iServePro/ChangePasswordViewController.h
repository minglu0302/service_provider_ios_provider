//
//  ChangePasswordViewController.h
//  iServeProvider
//
//  Created by -Tony Lu on 21/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *newpasswordTF;
@property (strong, nonatomic) IBOutlet UITextField *oldpasswordTF;
@property (strong, nonatomic) IBOutlet UITextField *reenterPassword;
@property (strong, nonatomic) IBOutlet UIButton *submit;
- (IBAction)submit:(id)sender;
@end
