//
//  AddRemindersAndEvents.h
//  iAssistPro
//
//  Created by -Tony Lu on 29/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "AddRemindersAndEvents.h"
#import "PMDReachabilityWrapper.h"

static AddRemindersAndEvents * eventsObj = nil;


@interface AddRemindersAndEvents()

// The database with calendar events and reminders
@property (strong, nonatomic) EKEventStore *eventStore;

// Indicates whether app has access to event store.
@property (nonatomic) BOOL isAccessToEventStoreGranted;

// The data source for the table view
@property (strong, nonatomic) NSMutableArray *todoItems;

//
@property (strong,nonatomic) EKAlarm *alarm;

@property (strong, nonatomic) EKCalendar *calendar;

@end
@implementation AddRemindersAndEvents

+(instancetype)instance
{
    if(!eventsObj)
    {
        eventsObj = [[AddRemindersAndEvents alloc] init];
    }
    return eventsObj;
}

-(EKEventStore *)eventStore
{
    if(!_eventStore)
    {
        _eventStore = [[EKEventStore alloc] init];
    }
    return _eventStore;
}

-(void)createEvent
{
      NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    EKEvent *event = [EKEvent eventWithEventStore:_eventStore];
    event.title  = NSLocalizedString(@"Booking reminder on Goclean Service provider",@"Booking reminder on Goclean Service provider");
    
     event.notes = [NSString stringWithFormat:@"You have a confirmed booking with %@ on Goclean Service Provider .Location %@ at %@",[ud objectForKey:@"customerName"],[ud objectForKey:@"custAddress"],[ud objectForKey:@"startBookingDate"]];
    event.startDate = _startingDate;
    event.endDate = _endingDate;
    
    NSArray *alarms = @[[EKAlarm alarmWithRelativeOffset:- 60.0f * 60.0f*1]];
    event.alarms = alarms;
    
    [event setCalendar:[_eventStore defaultCalendarForNewEvents]];
    NSError *err;
    
 
    BOOL success = [_eventStore saveEvent:event span:EKSpanThisEvent error:&err];
    NSString *eventID = [NSString stringWithFormat:@"%@", event.eventIdentifier];
    NSLog(@"Event Id:%@",eventID);
    [self sendEventIdToServer:[ud objectForKey:@"latBid"] andEventID:eventID];
    
    if(err)
                NSLog(@"unable to save event to the calendar!: Error= %@", err);
        //add error for deleting all event calenders.
        if (success) {
            NSLog(@"sucessfully created");
        }
}


#pragma mark - Web Service Call -

-(void)sendEventIdToServer:(NSString *)bid andEventID:(NSString *)eventId
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if([[PMDReachabilityWrapper sharedInstance] isNetworkAvailable])
    {
        if ([ud objectForKey:@"savBid"]) {
            if ([[ud objectForKey:@"savBid"] isEqualToString:bid]) {
                return;
            }
        }else{
            [ud setObject:bid forKey:@"savBid"];
        }
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey]),
                                 @"ent_bid":bid,
                                 @"ent_event_id":eventId,
                                 @"ent_user_type":@"1"
                                 };
        NetworkHandler *handler = [NetworkHandler sharedInstance];
        [handler composeRequestWithMethod:@"UpdateBookingReminder"
                                  paramas:params
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
                                 if (succeeded) {
                                     NSLog(@"Remainder updated to server succesfully");
                                 }
                             }];
        
    }
}



-(void)removeReminder:(NSString *)eventId
{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent
                          completion:^(BOOL granted, NSError *error)
     {
         if (!granted)
         {
             return;
         }
         EKEvent *event = [store eventWithIdentifier:eventId];
         
         if(event)
         {
             NSError *err = nil;
             [store removeEvent:event span:EKSpanThisEvent commit:YES error:&err];
         }
     }];
    
}

-(void)createReminder
{
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    EKReminder *reminder = [EKReminder reminderWithEventStore:self.eventStore];
    reminder.title = NSLocalizedString(@"Goclean Service Provider provider",@"Goclean Service Provider provider");
    [reminder setCalendar:[self calendar]];
    reminder.notes = [NSString stringWithFormat:@"You have a confirmed booking with %@ on Goclean Service Provider .Location %@ at %@",[ud objectForKey:@"customerName"],[ud objectForKey:@"custAddress"],[ud objectForKey:@"startBookingDate"]];
  //  reminder.dueDateComponents = [self dateComponentsForDefaultDueDate];
    reminder.completionDate = _endingDate;
    reminder.priority = 1;
    
    NSArray *alarms = @[[EKAlarm alarmWithRelativeOffset:- 60.0f * 60.0f*1]];
    reminder.alarms = alarms;

    
    // 3
    NSError *error = nil;
    BOOL success = [self.eventStore saveReminder:reminder commit:YES error:&error];
    if (!success) {
        // Handle error.
        //add error for deleting all event calenders.
    }
    
    // 4
    NSString *message = (success) ? @"Reminder was successfully added!" : @"Failed to add reminder!";
    NSLog(@"message = %@",message);
}

- (NSDateComponents *)dateComponentsForDefaultDueDate {
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay|NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond
                                                        fromDate:_startingDate];
    return components;
}

#pragma mark - Reminders

- (void)updateAuthorizationStatusToAccessEventStore {
    
    EKAuthorizationStatus authorizationStatus = [EKEventStore authorizationStatusForEntityType:EKEntityTypeReminder&EKEntityTypeEvent];
    
    switch (authorizationStatus) {
            // 3
        case EKAuthorizationStatusDenied:
        case EKAuthorizationStatusRestricted: {
            self.isAccessToEventStoreGranted = NO;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Access Denied",@"Access Denied")
                                                                message:NSLocalizedString(@"This app doesn't have access to your Reminders.",@"This app doesn't have access to your Reminders.") delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"Dismiss",@"Dismiss") otherButtonTitles:nil];
            [alertView show];
            break;
        }
            
            // 4
        case EKAuthorizationStatusAuthorized:
            self.isAccessToEventStoreGranted = YES;
            [self createEvent];
           // [self createReminder];
            break;
            
            // 5
        case EKAuthorizationStatusNotDetermined: {
            __weak AddRemindersAndEvents *weakself = self;
            [_eventStore requestAccessToEntityType:EKEntityTypeReminder&EKEntityTypeEvent
                                        completion:^(BOOL granted, NSError *error) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                weakself.isAccessToEventStoreGranted = granted;
                                                if(granted)
                                                {
                                                    [self createEvent];
                                                 //   [self createReminder];
                                                }
                                            });
                                        }];
            break;
        }
    }
}

- (EKCalendar *)calendar {
    if (!_calendar) {
        
        // 1
        NSArray *calendars = [self.eventStore calendarsForEntityType:EKEntityTypeReminder];
        
        // 2
        NSString *calendarTitle = NSLocalizedString(@"Booking reminder on Goclean Service provider",@"Booking reminder on Goclean Service provider");;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title matches %@", calendarTitle];
        NSArray *filtered = [calendars filteredArrayUsingPredicate:predicate];
        
        if ([filtered count]) {
            _calendar = [filtered firstObject];
        } else {
            
            // 3
            _calendar = [EKCalendar calendarForEntityType:EKEntityTypeReminder eventStore:self.eventStore];
            _calendar.title = NSLocalizedString(@"Booking reminder on Goclean Service provider",@"Booking reminder on Goclean Service provider");;
            _calendar.source = self.eventStore.defaultCalendarForNewReminders.source;
            
            // 4
            NSError *calendarErr = nil;
            BOOL calendarSuccess = [self.eventStore saveCalendar:_calendar commit:YES error:&calendarErr];
            if (!calendarSuccess) {
                // Handle error
            }
        }
    }
    return _calendar;
}

@end
