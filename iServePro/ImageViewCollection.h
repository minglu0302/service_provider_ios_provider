//
//  ImageViewCollection.h
//  YaaroDriver
//
//  Created by -Tony Lu on 05/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewCollection : UIView

+(id)sharedInstance;
- (void)showPopUpWithDictionary:(UIWindow *)window jobImages:(NSInteger)images index:(NSIndexPath*)indexVal tag:(NSInteger)tag jobImages:(NSMutableArray *)jobImages;
@property (strong, nonatomic) IBOutlet UICollectionView *imageCollection;
@property (assign, nonatomic)NSInteger jobImages;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (assign, nonatomic)NSInteger tagValue;
- (IBAction)closePopup:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (strong, nonatomic)NSMutableArray *ProfileJobImages;
@end
