//
//  PickAddressFromMapViewController.m
//  iServe_AutoLayout
//
//  Created by Apple on 14/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "PickAddressFromMapViewController.h"
#import "SaveSelectedAddressViewController.h"
#import "GetCurrentLocation.h"
#import "PickUpViewController.h"

@interface PickAddressFromMapViewController ()<GMSMapViewDelegate,GetCurrentLocationDelegate,PickUpAddressDelegate>
{
    double currentLatitude;
    double currentLongitude;
    GetCurrentLocation *getCurrentLocation;
     BOOL isAddressManuallyPicked;
}

@end

@implementation PickAddressFromMapViewController

#pragma mark - Initial Methods -

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
   
    self.messageLabel.text = @"Place the pin on exact location\nor Allow location services";

    [self.navigationController setNavigationBarHidden:NO animated:YES];

    
    //Set Properties Of MapView
    currentLatitude = [[[NSUserDefaults standardUserDefaults]objectForKey:@"lats"]floatValue];
    currentLongitude = [[[NSUserDefaults standardUserDefaults]objectForKey:@"longs"]floatValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLatitude longitude:currentLongitude zoom:16];
    
    self.selectedAddressLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentAddress"];
    
    self.mapView.camera = camera;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.delegate = self;
    self.mapView.myLocationEnabled = YES;
    [self createNavLeftButton];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
//    getCurrentLocation = [GetCurrentLocation sharedInstance];
//    getCurrentLocation.delegate = self;
//    [getCurrentLocation getLocation];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)searchAddressButtonAction:(id)sender
{
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    PickUpViewController *pickController =[mainstoryboard instantiateViewControllerWithIdentifier:@"pickUpLocationVC"];
    
    pickController.latitude =  [NSString stringWithFormat:@"%f",currentLatitude];
    pickController.longitude = [NSString stringWithFormat:@"%f",currentLongitude];
    pickController.pickUpAddressDelegate = self;
    
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
      [self presentViewController:navBar animated:YES completion:nil];
}

#pragma mark - GetAddress From Pick UP VC Delegate Method -

-(void)getAddressFromPickUpAddressVC:(NSDictionary *)addressDetails
{
    isAddressManuallyPicked = YES;
    NSString *addressText = flStrForStr(addressDetails[@"address1"]);
    if ([addressDetails[@"address2"] rangeOfString:addressText].location == NSNotFound) {
        
        addressText = [addressText stringByAppendingString:@","];
        addressText = [addressText stringByAppendingString:flStrForStr(addressDetails[@"address2"])];
        
    } else {
        
        if([addressDetails[@"address2"] length])
        {
            addressText = flStrForStr(addressDetails[@"address2"]);
        }
        
    }
    
    currentLatitude = [addressDetails[@"lat"] doubleValue];
    currentLongitude = [addressDetails[@"long"] doubleValue];
    self.selectedAddressLabel.text = addressText;
    
    GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(currentLatitude, currentLongitude) zoom:16];
    [self updatedLocation:currentLatitude and:currentLongitude];
    [self.mapView animateWithCameraUpdate:zoomCamera];
    
}


/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backButtonPressed
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - UIButton Actions -

- (IBAction)confirmLocationButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"toEditAddress" sender:sender];
}


#pragma mark - GMSMapview Delegate -

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    if (isAddressManuallyPicked) {
        //Checking Address Picked Manually (Get Address From Search Address Controller)
        
        isAddressManuallyPicked = NO;
        return;
    }
    
    else
    {
    CGPoint point1 = self.mapView.center;
    CLLocationCoordinate2D coor = [self.mapView.projection coordinateForPoint:point1];
    currentLatitude = coor.latitude;
    currentLongitude = coor.longitude;
    
    CLLocation *location = [[CLLocation alloc]initWithLatitude:currentLatitude longitude:currentLongitude];
    
    [self getAddress:location];
    }
}

#pragma mark - Get Current Location Delegates -

- (void)updatedLocation:(double)latitude and:(double)longitude
{
    //change map camera postion to current location
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:16];
    [self.mapView setCamera:camera];
    
    //save current location to plot direciton on map
    currentLatitude = latitude;
    currentLongitude =  longitude;
    
}

-(void)updatedAddress:(NSString *)currentAddress
{
    self.selectedAddressLabel.text = currentAddress;
    NSLog(@"Current Address In PickUp Address Class:%@",self.selectedAddressLabel.text);
}


#pragma mark - Get Current Location Methods -

- (void)getAddress:(CLLocation *)coordinate
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation:coordinate
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!error)
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             
             self.selectedAddressLabel.text = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             NSLog(@"Selected Address in PickUp Address Class:%@",self.selectedAddressLabel.text);
             
         }
         else
         {
             NSLog(@"Failed to update location : %@",error);
         }
     }];
    
}

#pragma mark - Prepare Segue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toEditAddress"])
    {
        SaveSelectedAddressViewController *saveAddressVC = [segue destinationViewController];
        saveAddressVC.isFromProviderBookingVC = self.isFromProviderBookingVC;
        saveAddressVC.selectedAddressDetails = @{
                                                @"address":flStrForStr(self.selectedAddressLabel.text),
                                                @"lat":[NSNumber numberWithDouble:currentLatitude],
                                                @"log":[NSNumber numberWithDouble:currentLongitude],
                                               };

    }
}

@end
