//
//  Addresscell.h
//  iServePassenger
//
//  Created by -Tony Lu on 15/04/15.
//  Copyright (c) 2015 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Addresscell : UITableViewCell


/**
 *  used to store address
 */
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;


/**
 *  used to remove the address from Database
 */
@property (strong, nonatomic) IBOutlet UIButton *removeAddressButton;

@property (weak, nonatomic) IBOutlet UIImageView *tagImageView;

@property (weak, nonatomic) IBOutlet UILabel *tagLabel;


@end
