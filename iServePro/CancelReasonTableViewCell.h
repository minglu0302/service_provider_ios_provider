//
//  CancelReasonTableViewCell.h
//  iServePro
//
//  Created by -Tony Lu on 25/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelReasonTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *reasonLabel;

@end
