//
//  BackDetailsTableViewCell.h
//  ServeusPro
//
//  Created by -Tony Lu on 17/03/17.
//  Copyright © 2017 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackDetailsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *accountHolderName;
@property (strong, nonatomic) IBOutlet UIButton *selectedButton;
@property (strong, nonatomic) IBOutlet UIView *topView;

@property (strong, nonatomic) IBOutlet UILabel *accountNumber;
@end
