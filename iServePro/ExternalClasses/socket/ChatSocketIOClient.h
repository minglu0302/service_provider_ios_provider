//  ChatSocketIOClient.h
//  Sup
//
//  Created by Tony Lu on 1/8/16.
//  Copyright © 2016 Tony Lu. All rights reserved.
//
#import<Foundation/Foundation.h>
#import "ChatSIOConfiguration.h"
#import "ChatSIOClient.h"
#import "Helper.h"


@protocol ChatSocketIODelegetes<NSObject>

//-(void)responseFromChannels1:(NSDictionary *)responseDictionary;

//-(void)didConnect;
//-(void)didDisconnect;
//-(void)messageSentSuccessfullyForMessageID:(NSMutableDictionary *)msgInfo;
//-(void)didSubscribeToHistory;
//-(void)didSubscribeToGetMessageAcks;
//-(void)didSubscribetoOnlineStatus;

-(void)responseFromChannels:(NSDictionary *)responseDictionary;

-(void)respondToChatMessage:(NSDictionary *)message;


@end

typedef NS_ENUM(NSUInteger, SocketMessageType) {
    SocketMessageTypeText,
    SocketMessageTypePhoto,
    SocketMessageTypeVideo,
    SocketMessageTypeLocation,
    SocketMessageTypeContact,
};

@interface ChatSocketIOClient : NSObject


+ (instancetype) sharedInstance;


-(void)socketIOSetup;
//- (void) syncContacts:(NSArray *)contacts;
-(void)connectSocket;
-(void)disconnectSocket;


@property (nonatomic, weak) id <ChatSocketIODelegetes> chatDelegate;
@property (nonatomic, strong) NSMutableArray *channelsName;
//****iserve*******//
-(void)heartBeat:(NSString *)status;

@end


