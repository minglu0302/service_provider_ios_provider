//
//  ChatSIOConfiguration.m
//  Sup
//
//  Created by Tony Lu on 1/8/16.
//  Copyright © 2016 Tony Lu. All rights reserved.
//

#import "ChatSIOConfiguration.h"


#define kkHostURL        @"http://www.goclean-service.com"
#define kkPortNumber     @"9999"

@interface ChatSIOConfiguration()

@property (nonatomic, strong) NSString *hostURL;
@property (nonatomic, strong) NSString *portNumber;


@end




@implementation ChatSIOConfiguration

+(instancetype)defaultConfiguration{
    
    ChatSIOConfiguration *config  = [[ChatSIOConfiguration alloc]init];
    config.hostURL = kkHostURL;
    config.portNumber = kkPortNumber;
    return config;
    
}

-(instancetype) initWithHostURL:(NSString *)hostURL portNumber:(NSString *)portNumber{
    
    self = [super init];
    if (self) {
        self.hostURL = hostURL;
        self.portNumber = portNumber;
    }
    return self;
    
}
@end

@implementation ChatSIOConfiguration (AccessHelper)

- (NSString*) getHostString {
    return [NSString stringWithFormat:@"%@:%@", _hostURL, _portNumber];
}
@end
