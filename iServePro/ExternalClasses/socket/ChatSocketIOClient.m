//
//  ChatSocketIOClient.m
//  Sup
//
//  Created by Tony Lu on 1/8/16.
//  Copyright © 2016 Tony Lu. All rights reserved.
//
#import "ChatSocketIOClient.h"
#import "ChatSIOClient.h"
#import "zlib.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "IServeAppDelegate.h"
#import "iServeSplashController.h"
#import "HomeTabBarController.h"
#import "NewBookingController.h"
#import "IServeHomeViewController.h"
#import "AddRemindersAndEvents.h"


static ChatSocketIOClient *supSocketIOClient =  nil;

@interface ChatSocketIOClient() <ChatSIOClientDelegate,CLLocationManagerDelegate>
@property ChatSIOClient *soketIOClient;
@property (nonatomic,strong) NSArray *contactArray ;
@property int num;
@property int currentNum;
@property BOOL isNewBooking;
@property (strong, nonatomic) UIWindow *window;




@end


@implementation ChatSocketIOClient


+ (instancetype) sharedInstance
{
    if (!supSocketIOClient) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            supSocketIOClient = [[self alloc] init];
            
        });
    }
    
    
    return supSocketIOClient;
}



-(void)connectSocket {
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [supSocketIOClient socketIOSetup];
    }];
    
}

-(void)disconnectSocket {
    
    [_soketIOClient disconnect];
}

-(void)socketIOSetup
{
    ChatSIOConfiguration *config = [ChatSIOConfiguration defaultConfiguration];
    self.soketIOClient = [ChatSIOClient sharedInstance];
    [self.soketIOClient setConfiguration:config];
    [self.soketIOClient setDelegate:self];
    [self.soketIOClient connect];
    self.channelsName = [[NSMutableArray alloc] init];
}


#pragma mark - SIOClientDelegate

- (void) sioClient:(ChatSIOClient *)client didConnectToHost:(NSString*)host {
    NSLog(@"ChatSIO connect to %@", host);
    
    
    if (![self.channelsName containsObject:@"LiveBooking"]) {
        [self.channelsName addObject:@"LiveBooking"];
        [self.soketIOClient subscribeToChannels:@[@"LiveBooking"]];
    }
    if (![self.channelsName containsObject:@"Message"]) {
        [self.channelsName addObject:@"Message"];
        [self.soketIOClient subscribeToChannels:@[@"Message"]];
    }

    [self heartBeat:@"1"];
}

-(void)heartBeat:(NSString *)status{
    NSString *proID;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kNSUDoctorEmailAddressKey]) {
        proID=[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDoctorEmailAddressKey];
    }else{
        proID=@"Simulator";
    }
    
    NSDictionary *message=@{
                            @"status":@"1",
                            @"email":proID,
                            @"pid":flStrForObj([[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"])
                            };
    [self.soketIOClient publishToChannel:@"ProviderStatus" message:message];
}


- (void) sioClient:(ChatSIOClient *)client didSubscribeToChannel:(NSString*)channel {
    NSLog(@"ChatSIO subscribe to %@", channel);
}

- (void) sioClient:(ChatSIOClient *)client didSendMessageToChannel:(NSString *)channel {
    NSLog(@"ChatSIO message sent to %@", channel);
    
}


- (NSString *)stringByRemovingControlCharacters: (NSString *)inputString
{
    NSCharacterSet *controlChars = [NSCharacterSet controlCharacterSet];
    NSRange range = [inputString rangeOfCharacterFromSet:controlChars];
    if (range.location != NSNotFound) {
        NSMutableString *mutable = [NSMutableString stringWithString:inputString];
        while (range.location != NSNotFound) {
            [mutable deleteCharactersInRange:range];
            range = [mutable rangeOfCharacterFromSet:controlChars];
        }
        return mutable;
    }
    return inputString;
}



- (void) sioClient:(ChatSIOClient *)client didRecieveMessage:(NSArray*)message onChannel:(NSString *)channel {
    NSLog(@"ChatSIO message recieved %@ on %@", message, channel);
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:NO];
    IServeAppDelegate *tmpDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if ([channel isEqualToString:@"Message"]) {
        NSLog(@"message received %@",message);
        
        if(message[0][@"payload"])
        {
            if (_chatDelegate && [_chatDelegate respondsToSelector:@selector(respondToChatMessage:)]) {
                [self.chatDelegate respondToChatMessage:message[0]];
            }
        }
        
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"OnChatController"] && state == UIApplicationStateActive) {
            NSMutableDictionary *notificationDict = [[NSMutableDictionary alloc] init];
            [notificationDict setObject:message forKey:@"aps"];
            [tmpDelegate successButtonPressed:notificationDict];
        }
    }
    else if ([channel isEqualToString:@"LiveBooking"]) {
        if (_chatDelegate && [_chatDelegate respondsToSelector:@selector(responseFromChannels:)]) {
            [self.chatDelegate responseFromChannels:message[0]];
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentCancel" object:nil];
}

-(void)remainderAdded:(NSString *)startTime endTime:(NSString *)endTime{
    
    
    
    
    NSTimeInterval start = [startTime doubleValue];
    NSDate * startData = [NSDate dateWithTimeIntervalSince1970:start];
    NSString *stringStart = [self getDateStringFromDate:startData];
    
    NSTimeInterval end = [endTime doubleValue];
    NSDate * endDate = [NSDate dateWithTimeIntervalSince1970:end];
    NSString *stringEnd = [self getDateStringFromDate:endDate];
    
    NSDate *FromDate = [self convertGMTtoLocalTimeConversion:stringStart];
    NSDate *toDate = [self convertGMTtoLocalTimeConversion:stringEnd];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:startData forKey:@"startBookingDate"];
    AddRemindersAndEvents *eventObj = [AddRemindersAndEvents instance];
    [eventObj eventStore];
    [eventObj setStartingDate:FromDate];
    [eventObj setEndingDate:toDate];
    [eventObj updateAuthorizationStatusToAccessEventStore];
}

-(NSString *)getDateStringFromDate :(NSDate *)date {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

-(NSDate *)convertGMTtoLocalTimeConversion:(NSString *)gmtDateStr
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *local = [NSTimeZone systemTimeZone];
    [formatter setTimeZone:local];
    NSDate *localDate = [formatter dateFromString:gmtDateStr]; // get the date
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    NSTimeInterval localTimeInterval = [localDate timeIntervalSinceReferenceDate] - timeZoneOffset;
    NSDate *localCurrentDate = [NSDate dateWithTimeIntervalSinceReferenceDate:localTimeInterval];
    return localCurrentDate;
}



- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void(^)())completionHandler {
    
    NSDictionary *dict = [notification.userInfo mutableCopy];
    
    if([identifier isEqualToString:@"Accept"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"bookingStatusResponse"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
          [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptBooking" object:dict];
        
    }
    else if([identifier isEqualToString:@"Reject"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"3" forKey:@"bookingStatusResponse"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"rejectBooking" object:nil userInfo:(NSDictionary *)dict];
    }
    if(completionHandler != nil)    //Finally call completion handler if its not nil
        completionHandler();
}




- (void) sioClient:(ChatSIOClient *)client didDisconnectFromHost:(NSString*)host {
    
    NSLog(@"ChatSIO disconnect from %@", host);
//    if (_chatDelegate && [_chatDelegate respondsToSelector:@selector(didDisconnect)]) {
//        [_chatDelegate didDisconnect];
//    }
}

- (void) sioClient:(ChatSIOClient *)client gotError:(NSDictionary *)errorInfo {
    NSLog(@"SIO Error : %@", errorInfo);
}

@end
