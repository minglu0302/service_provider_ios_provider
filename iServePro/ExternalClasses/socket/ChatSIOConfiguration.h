
//
//  ChatSIOConfiguration.h
//  Sup
//
//  Created by Tony Lu on 1/8/16.
//  Copyright © 2016 Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatSIOConfiguration : NSObject


@property (nonatomic, readonly) NSString *hostURL;



@property (nonatomic, readonly) NSString *portNumber;

+ (instancetype) defaultConfiguration;

- (instancetype) initWithHostURL:(NSString*)hostURL portNumber:(NSString*)portNumber;

@end

@interface ChatSIOConfiguration (AccessHelper)

- (NSString*) getHostString;

@end
