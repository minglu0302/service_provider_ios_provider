//
//  UILabel+LabelHeight.h
//  iServePro
//
//  Created by -Tony Lu on 10/03/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (LabelHeight)

+ (float)getLabelHeight:(UILabel *)label;

@end
