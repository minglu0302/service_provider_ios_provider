//
//  AmazonTransfer.h
//  
//
//  Created by Tony Lu on 04/09/14.
//  Copyright (c) 2014 Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWSCore.h"
#import "AWSS3.h"
#import "AWSS3TransferManager.h"

typedef void(^AmazonTransferCompletionBlock)(BOOL success, id result , NSError *error);
typedef void(^AmazonTransferProgressBlock)(int64_t progressSize, int64_t expectedSize);

#define AmazonAccessKey           @"AKIAI5CRBK3VAFK7HUPA"
#define AmazonSecretKey           @"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"
#define Bucket                    @"goclean-service"


@interface AmazonTransfer : NSObject

+ (void) setConfigurationWithRegion:(AWSRegionType)regionType
                         accessKey:(NSString*)accessKey
                         secretKey:(NSString*)secretKey;

+ (void) upload:(NSString*)localFilePath
         fileKey:(NSString*)fileKey
        toBucket:(NSString*)bucket
       mimeType:(NSString *) mimeType
   progressBlock:(AmazonTransferProgressBlock) progressBlock
 completionBlock:(AmazonTransferCompletionBlock) completionBlock;

+ (void) download:(NSString*)fileKey
       fromBucket:(NSString*)bucket
           toFile:(NSString*)localFilePath
   progressBlock:(AmazonTransferProgressBlock) progressBlock
 completionBlock:(AmazonTransferCompletionBlock) completionBlock;

+ (void) upload:(NSString *) localFilePath
        fileKey:(NSString *) fileKey
       toBucket:(NSString *) bucket
       mimeType:(NSString *) mimeType
completionBlock:(AWSS3TransferUtilityUploadCompletionHandlerBlock) completionBlock;

@end


