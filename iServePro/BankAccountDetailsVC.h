//
//  BankAccountDetailsVC.h
//  ServeusPro
//
//  Created by -Tony Lu on 17/03/17.
//  Copyright © 2017 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankAccountDetailsVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *bankDetailsTableView;
- (IBAction)addBackDetails:(id)sender;
- (IBAction)backButton:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *labelText;
@end
