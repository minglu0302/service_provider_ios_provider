//
//  CalendarVC.m
//  iServePro
//
//  Created by -Tony Lu on 28/03/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "CalendarVC.h"
#import "HistoryCalendarCell.h"
#import "InvoicePopup.h"
#import "User.h"
#import "iServeSplashController.h"
#import "LocationTracker.h"
#import "LiveChatViewController.h"


@interface CalendarVC ()<UserDelegate>{
    NSMutableDictionary *_eventsByDate;
    
    NSDate *_todayDate;
    NSDate *_minDate;
    NSDate *_maxDate;
    
    NSDate *_dateSelected;
}
@property (strong, nonatomic) IBOutlet UILabel *noJobHistoryLabel;
@property (strong, nonatomic) NSMutableArray *dict;
@end


@implementation CalendarVC
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(!self){
        return nil;
    }
    
    
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dict =[[NSMutableArray alloc]init];
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    _year.text = [formatter stringFromDate:[NSDate date]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x2598ED)};
    // Generate random events sort by date using a dateformatter for the demonstration
    // [self createRandomEvents];
    
    // Create a min and max date for limit the calendar, optional
    [self createMinAndMaxDate];
    [_calendarManager setMenuView:_calendarMenuView];
    
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:_todayDate];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }

}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewDidAppear:(BOOL)animated{
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"calDate"]) {
         [self sendServicegetPatientAppointment:[ud objectForKey:@"calDate"]];
    }else{
    NSString *month = [self getMonths];
        [ud setObject:month forKey:@"calDate"];
    [self sendServicegetPatientAppointment:month];
    }
}

/***************************************/
-(NSString *)getMonths
{
    NSDate *date = [NSDate date];
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    // NSInteger Day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    //
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month]];
    
    NSString *retMonth = [NSString stringWithFormat:@"%ld-%@",(long)year,monthString];
    return retMonth;
}
-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userSessionTokenExpire) name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    
}

-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)sendServicegetPatientAppointment :(NSString *)month
{
    
//    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:NSLocalizedString(@"Loading...",@"Loading...")];

    NSDictionary *parameters = @{kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_appnt_dt":month,
                                 @"ent_date_time":[Helper getCurrentDateTime]};
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:@"getMasterAppointmentsHistory"
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (succeeded) { //handle success response
                                 [self createRandomEvents:[response mutableCopy]];
                                 // [self getPatientAppointmentResponse:response];
                             }
                             else{
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 
                                 NSLog(@"Error");
                                 
                             }
                         }];
}
/***************************************/

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = UIColorFromRGB(0X808080);
        dayView.dotView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}


- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                        [self.tableView reloadData];
                    } completion:nil];
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
}


#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [_calendarManager.dateHelper date:date isEqualOrAfter:_minDate andEqualOrBefore:_maxDate];
}

- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    _year.text = [formatter stringFromDate:calendar.date];
    [self getSlotDetailsAccordingToMonths:calendar.date];
    //    NSLog(@"Next page loaded");
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    _year.text = [formatter stringFromDate:calendar.date];
    [self getSlotDetailsAccordingToMonths:calendar.date];
    //    NSLog(@"Previous page loaded");
}
-(void)getSlotDetailsAccordingToMonths:(NSDate *)dateString{
    NSDate *date = dateString;
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month]];
    
    NSString *retMonth = [NSString stringWithFormat:@"%ld-%@",(long)year,monthString];
    [[NSUserDefaults standardUserDefaults] setObject:retMonth forKey:@"calDate"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self sendServicegetPatientAppointment:retMonth];
}

#pragma mark - Fake data

- (void)createMinAndMaxDate
{
    _todayDate = [NSDate date];
    
    // Min date will be 2 month before today
    _minDate = [_calendarManager.dateHelper addToDate:_todayDate months:-9];
    
    // Max date will be 2 month after today
    _maxDate = [_calendarManager.dateHelper addToDate:_todayDate months:9];
}

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"yyyy-MM-dd";//dd-MM-yyyy
    }
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    return NO;
    
}

- (void)createRandomEvents:(NSMutableDictionary *)response
{
    if ([response[@"errFlag"] integerValue]==1) {
        if ([response[@"errNum"] integerValue] == 7) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else if ([response[@"errNum"] integerValue] == 96 || [response[@"errNum"] integerValue] == 94||[response[@"errNum"] integerValue] == 83) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
        
    }else{
        
        _eventsByDate = [NSMutableDictionary new];
        NSMutableDictionary *apArr = [[NSMutableDictionary alloc]init];
        
        NSMutableArray *appointmentsArr = [response objectForKey:@"Bookings"];
        for(int i = 0; i < appointmentsArr.count; ++i){
            
            apArr = [appointmentsArr objectAtIndex:i];
            
            NSArray *appDetailArr = [apArr objectForKey:@"appt"];
            
            NSString *key = [apArr objectForKey:@"date"];
            
            if(!_eventsByDate[key]){
                _eventsByDate[key] =appDetailArr;
                
            }
        }
        [_calendarManager reload];
        [self.tableView reloadData];
    }
}

/*-------------------------*/
#pragma mark - UserDelegate
/*-------------------------*/
-(void)userDidLogoutSucessfully:(BOOL)sucess {
    if (sucess){
        // Logged it out Successfully
        NSLog(@"Logged it out Successfully");
    }
    else{
        // Session is Expired
        NSLog(@"Session is Expired");
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)userDidFailedToLogout:(NSError *)error
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}



#pragma mark - UITableView Datasource and delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 105;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];//2012-11-22
    NSString *date;
    if (!_dateSelected) {
        date = [formatter stringFromDate:_todayDate];
    }else{
        date = [formatter stringFromDate:_dateSelected];
    }
    _dict=[_eventsByDate[date]mutableCopy];
    
    if (_dict.count) {
        _noJobHistoryLabel.hidden = true;
    }else{
        _noJobHistoryLabel.hidden = false;
    }
    return [_eventsByDate[date] count];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"calendarCell";
    HistoryCalendarCell *cell;
    
    if (!cell) {
        cell  = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    NSInteger serviceAmt = 0;
    cell.custAddress.text=flStrForObj(_dict[indexPath.row][@"addrLine1"]);
    cell.custName.text=flStrForObj(_dict[indexPath.row][@"fname"]);
    cell.bid.text=[NSString stringWithFormat:@"Bid: %@",_dict[indexPath.row][@"bid"]];
    cell.requestType.text =[NSString stringWithFormat:@"%@ - %@ Request",_dict[indexPath.row][@"apntTime"],_dict[indexPath.row][@"cat_name"]];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    for (int i = 0; i<[_dict[indexPath.row][@"services"] count]; i++) {
        serviceAmt = serviceAmt + [_dict[indexPath.row][@"services"][i][@"sprice"]integerValue];
    }
    
    NSNumber *TOt =[NSNumber numberWithDouble:[_dict[indexPath.row][@"visit_amount"] doubleValue]+[_dict[indexPath.row][@"mat_fees"] doubleValue]+[_dict[indexPath.row][@"misc_fees"] doubleValue]+([_dict[indexPath.row][@"appt_duration"] doubleValue]*[_dict[indexPath.row][@"price_per_min"] doubleValue])-[_dict[indexPath.row][@"coupon_discount"] doubleValue]-[_dict[indexPath.row][@"pro_disc"] doubleValue]+serviceAmt];
    NSString *totalMat = [formatter stringFromNumber:TOt];
    cell.amountCharged.text=totalMat;
    
    if ([_dict[indexPath.row][@"status"]integerValue]==4||[_dict[indexPath.row][@"status"]integerValue]==9) {
        NSNumber *cancelAmt =[NSNumber numberWithInteger:[_dict[indexPath.row][@"cancelAmt"]integerValue]];
        NSString *cancelFee = [formatter stringFromNumber:cancelAmt];
        cell.bookingStatus.text=flStrForObj(_dict[indexPath.row][@"statusMsg"]);
        cell.amountCharged.text=cancelFee;
    }else if ([_dict[indexPath.row][@"status"]integerValue]==3||[_dict[indexPath.row][@"status"]integerValue]==10)
    {
        NSNumber *cancelAmt =[NSNumber numberWithInteger:0];
        NSString *cancelFee = [formatter stringFromNumber:cancelAmt];
        cell.bookingStatus.text=flStrForObj(_dict[indexPath.row][@"statusMsg"]);
        cell.amountCharged.text=cancelFee;
    }else{
        cell.bookingStatus.text=flStrForObj(_dict[indexPath.row][@"statusMsg"]);
        cell.amountCharged.text=totalMat;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_dict[indexPath.row][@"status"]integerValue]==4||[_dict[indexPath.row][@"status"]integerValue]==9||[_dict[indexPath.row][@"status"]integerValue]==3||[_dict[indexPath.row][@"status"]integerValue]==10) {
        [Helper showAlertWithTitle:@"Message" Message:@"This Booking Has been Cancelled"];
    }else{
        InvoicePopup *invoice = [InvoicePopup sharedInstance];
        // Set frame of PopUp
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [invoice showPopUpWithDictionary:_dict[indexPath.row]
                                onWindow:window];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



- (IBAction)previousCal:(id)sender {
    [_calendarContentView loadPreviousPageWithAnimation];
}

- (IBAction)nextCalendar:(id)sender {
    [_calendarContentView loadNextPageWithAnimation];
}

- (IBAction)liveChat:(id)sender {
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    LiveChatViewController  *chatVc = [mainstoryboard instantiateViewControllerWithIdentifier:@"liveChatVC"];
    
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:chatVc];
    navBar.navigationBar.translucent = NO;
    [self presentViewController:navBar animated:YES completion:nil];
}

@end
