//
//  HMADatabase.h
//  Homappy
//
//  Created by -Tony Lu on 03/08/15.
//  Copyright (c) 2015 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TSPLDatabase : NSObject

// Generic Method for Deleting Row and Table

/**
 *  Delete row from table
 *
 *  @param tableName EntityName
 *  @param keys      Condition statement
 *
 *  @return returns YES if success, NO on Failure
 */
+(BOOL) deleteRowFromTable:(NSString*)tableName andKeys:(NSString *)keys;

/**
 *  Delete al, the rows from table
 *
 *  @param tableName EntityName
 */
+(void) deleteAllRowsFromTable: (NSString *)tableName;
/**
 *  Fetch the data from table with conditions
 *
 *  @param table     Entity name
 *  @param condition Condition statemen
 *  @param column    Column name
 *  @param asc       Order
 *
 *  @return ArrayOfResult
 */
+(NSArray *) dataFromTable:(NSString *)table
                 condition:(NSString *)condition
                   orderBy:(NSString *)column
                 ascending:(BOOL)asc;

/**
 *  Fetch all the data from table
 *
 *  @param tableName EntityName
 *
 *  @return ArrayOfResult
 */
+(NSArray *)getAllDataFromTable:(NSString *)tableName;

/**
 *  Fetch the data from table with conditions
 *
 *  @param table     Entity name
 *  @param condition Condition statemen
 *
 *  @return ArrayOfResult
 */
+ (NSArray *)getAllDataFromTable:(NSString *)table
                    andCondition:(NSString *)condition;

/**
 *  more than one condition
 *
 *  @param BOOL condtion if success Returns
 *
 *  @return returns according the condition
 */
+ (NSArray *)getAllDataFromTable:(NSString *)table
                      condition1:(NSString *)condition1
                      condition2:(NSString *)condition2;

/*--------------------------------*/
#pragma mark - New Data base method
/*--------------------------------*/



/**
 *  Address Table
 *
 *  @param dictionary Parameter to be stored
 *
 *  @return Bool value YES on Success or NO on Failure
 */
- (BOOL)makeDataEntryForAddressTable:(NSDictionary *)dictionary;







+ (BOOL)deleteArowFromTable:(NSString *)tableName andIndex:(NSInteger)index;

@end
