//
//  NewBookingController.m
//  iServePro
//
//  Created by -Tony Lu on 03/03/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "NewBookingController.h"
#import <AVFoundation/AVFoundation.h>
#import "ProgressIndicator.h"
#import "PICircularProgressView.h"
#import "ChatSIOClient.h"
#import "ChatSocketIOClient.h"
#import "IServeHomeViewController.h"

@interface NewBookingController ()
{
    ChatSocketIOClient *socket1;
    ChatSIOClient *soketIOClient;
}
@property (strong, nonatomic) IBOutlet GMSMapView *mapView_;

@property (strong, nonatomic)  AVAudioPlayer *audioPlayer;
@property (assign, nonatomic) float expireTime;
@property (strong, nonatomic) NSTimer *timerProgress;
@property (strong, nonatomic) NSMutableArray *array;
@property (nonatomic) NSTimer* UpdateTimer;
@property int bookingStat;

@property float countdown;

@end

@implementation NewBookingController
- (void)viewDidLoad {
    
    [super viewDidLoad];
    if(!socket1 || !soketIOClient){
        socket1  =[ChatSocketIOClient sharedInstance];
        soketIOClient =[ChatSIOClient sharedInstance];
    }
    [socket1 connectSocket];
    if (_detailsDict.count) {
        _array=[_detailsDict mutableCopy];
    }
    self.progressView.backgroundColor = [UIColor colorWithWhite:0.333 alpha:0.730];
    self.progressView.layer.cornerRadius = 90;
    self.progressView.layer.masksToBounds = YES;
    
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"alarma"
                            ofType:@"mp3"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    if (!_audioPlayer) {
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:pewPewURL error:nil];
    }
    _audioPlayer.numberOfLoops = 0;
    [_audioPlayer prepareToPlay];
    [_audioPlayer play];
    NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&%@&sensor=true&key=%@",[_detailsDict[@"lat"] doubleValue], [_detailsDict[@"long"] doubleValue],@"zoom=14&size=180x180", kPMDGoogleMapsAPIKey];
    NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    UIImage *image = [UIImage imageWithData: [NSData dataWithContentsOfURL:mapUrl]];
    _mapImage.image=image;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"acceptBooking" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"rejectBooking" object:nil];
    
    [self startCoundownTimer];
    [self UpdateCustomerInfo];
    _isTimeOver=YES;
}
- (void)receiveNotification:(NSDictionary *)dict{
    _detailsDict= dict[@"aps"];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"bookingStatusResponse"] integerValue]==2) {
        [self acceptOrder:nil];
    }else{
        [self rejectOrder:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    [_timerProgress invalidate];
    _timerProgress = nil;
}

-(void)UpdateCustomerInfo
{
    _custLoc1.text =_detailsDict[@"add"];
    _requestType.text=[NSString stringWithFormat:@"New %@ Request",_detailsDict[@"catname"]];
    
}


- (IBAction)acceptOrder:(id)sender {
    _isTimeOver=NO;
    [self respondToNewBookingFor:kNotificationTypeBookingAccept1];
}

- (IBAction)rejectOrder:(id)sender {
    _isTimeOver=NO;
    [self respondToNewBookingFor:kNotificationTypeBookingReject1];
}



-(void)respondToNewBookingFor:(BookingNotificationType)bookingAction{
    [_audioPlayer stop];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...",@"Loading...")];
    NSDictionary *queryParams;
    queryParams =@{
                   KDAcheckUserSessionToken     :[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                   kSMPCommonDevideId           :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                   kSMPRespondPassengerEmail    :flStrForObj(_detailsDict[@"email"]),
                   kSMPRespondBookingDateTime   :flStrForObj(_detailsDict[@"dt"]),
                   kSMPRespondResponse          :[NSString stringWithFormat:@"%d",bookingAction],
                   kSMPRespondBookingType       :constkNotificationTypeBookingType,
                   kSMPCommonUpDateTime         : [Helper getCurrentDateTime],
                   @"ent_bid"                   :flStrForObj(_detailsDict[@"bid"]),
                   @"ent_proid"                 :[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"]
                   };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodRespondToAppointMent
                                    paramas:queryParams
                               onComplition:^(BOOL success, NSDictionary *response){                                                                      ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                   [pi hideProgressIndicator];
                                   
                                   if (success) { //handle success response
                                       
                                       if ([response[@"errFlag"] integerValue] == 0)
                                       {
                                           
                                           if (bookingAction == 2)
                                           {
                                                _bookingStat = bookingAction;
                                               [self emitTheBookingACk];
                                               [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
                                               [self dismissViewControllerAnimated:YES completion:nil];
                                           }
                                           else if (bookingAction == 3)
                                           {
                                               _bookingStat = bookingAction;
                                               [self emitTheBookingACk];
                                              [self dismissViewControllerAnimated:YES completion:nil];
                                               
                                           }
                                       }
                                       else if ([response[@"errFlag"] integerValue] == 1)
                                       {
                                           [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message")Message:response[@"errMsg"]];
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }else
                                       {
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }
                                   }
                                   else{
                                       
                                   }
                               }];
}


-(void)emitTheBookingACk
{

    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_detailsDict[@"bid"],
                            @"bstatus":[NSNumber numberWithInt:_bookingStat],
                            @"cid":_detailsDict[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime]
                            };
    
        [socket publishToChannel:@"LiveBookingAck" message:message];
}


-(void)startCoundownTimer{
//    [UIView animateWithDuration:30.f animations:^{
//        self.pgView.value = 30.f - self.pgView.value;
//    }];

  
    self.progressView.thicknessRatio=0.2;
    self.progressView.progress = 1;
    self.progressView.showText =YES;
    self.progressView.roundedHead = YES;
    [self.progressView setProgressText:[NSString stringWithFormat:@"%.0f",_timeRemaining]];
    _expireTime = _timeRemaining;
    if (![_timerProgress isValid]) {
        _timerProgress = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    }
}


- (void)timerTick
{
    float Newvalue1 = _expireTime/30 - (0.033333333);
    float Newvalue = _expireTime - 1;
    [_progressView setProgressText:[NSString stringWithFormat:@"%.0f", Newvalue]];
    _progressView.progress = Newvalue1;
    _expireTime = Newvalue;
    if (Newvalue <= 0.0) {
        [_audioPlayer stop];
        [_timerProgress invalidate];
        [_progressView removeFromSuperview];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
